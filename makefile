SHELL		= /bin/sh
MAKE		= make

CC		= cc
CFLAGS		=

DESTDIR		=
PREFIX		= /usr/local
BINDIR		= $(PREFIX)/games
DATADIR		= $(PREFIX)/share/games
INCDIR		= $(PREFIX)/inc
LIBDIR		= $(PREFIX)/lib
MANDIR		= $(PREFIX)/man

PRGNAM		= hnefatafl
VERSION		= 140816
MANCAT		= 6

SRC_BUILDDIR	= build
SRC_DATADIR	= data
SRC_MANDIR	= man
SRC_SRCDIR	= src
CORE_DIR	= $(SRC_SRCDIR)/core
AIM_DIR		= $(SRC_SRCDIR)/aim
UICX_DIR	= $(SRC_SRCDIR)/uicx
MDTOROFF	= $(SRC_BUILDDIR)/mdtoroff.sh

BIN		= $(PRGNAM)
MANS		= $(SRC_MANDIR)/$(PRGNAM)_eng.$(MANCAT)\
		$(SRC_MANDIR)/$(PRGNAM)_swe.$(MANCAT)

all: static

static:
	cd $(SRC_SRCDIR)	&& $(MAKE) static\
					CC="$(CC)"\
					CFLAGS="$(CFLAGS)"

dynamic:
	cd $(SRC_SRCDIR)	&& $(MAKE) dynamic\
					CC="$(CC)"\
					CFLAGS="$(CFLAGS)"

roff: $(MANS)

$(SRC_MANDIR)/$(PRGNAM)_eng.$(MANCAT): $(SRC_MANDIR)/$(PRGNAM)_eng.txt
	cat $(SRC_MANDIR)/$(PRGNAM)_eng.txt\
		| $(MDTOROFF)\
			-c "$(MANCAT)"\
			-p "$(PRGNAM)"\
			-v "$(VERSION)"\
		> $(SRC_MANDIR)/$(PRGNAM)_eng.$(MANCAT)

$(SRC_MANDIR)/$(PRGNAM)_swe.$(MANCAT): $(SRC_MANDIR)/$(PRGNAM)_swe.txt
	cat $(SRC_MANDIR)/$(PRGNAM)_swe.txt\
		| $(MDTOROFF)\
			-c "$(MANCAT)"\
			-p "$(PRGNAM)"\
			-v "$(VERSION)"\
		> $(SRC_MANDIR)/$(PRGNAM)_swe.$(MANCAT)

clean:
	cd $(SRC_SRCDIR)	&& $(MAKE) clean

mrproper: clean
	cd $(SRC_SRCDIR)	&& $(MAKE) mrproper

clean-roff:
	rm -f $(MANS)

clean-dist: mrproper clean-roff
	cd $(SRC_SRCDIR)	&& $(MAKE) clean-dist

dist: clean-dist roff
	cd $(SRC_SRCDIR)	&& $(MAKE) dist
	FILES=`echo *`				;\
	mkdir -p	"$(PRGNAM)$(VERSION)"	;\
	cp -r $$FILES	"$(PRGNAM)$(VERSION)/"
	tar -cf		"$(PRGNAM)$(VERSION).tar" "$(PRGNAM)$(VERSION)"
	gzip -9		"$(PRGNAM)$(VERSION).tar"
	rm -rf		"$(PRGNAM)$(VERSION)"

install: install-static

install-dynamic:
	$(MAKE) install-bin
	$(MAKE) install-data
	$(MAKE) install-inc
	$(MAKE) install-lib
	$(MAKE) install-man

install-static:
	$(MAKE) install-bin
	$(MAKE) install-data
	$(MAKE) install-man

install-bin:
	mkdir -p $(DESTDIR)$(BINDIR)
	cp $(UICX_DIR)/$(BIN)			$(DESTDIR)$(BINDIR)/

install-data:
	mkdir -p $(DESTDIR)$(DATADIR)/$(PRGNAM)
	cp -r $(SRC_DATADIR)/*		$(DESTDIR)$(DATADIR)/$(PRGNAM)/

install-inc:
	mkdir -p $(DESTDIR)$(INCDIR)
	cp $(CORE_DIR)/$(PRGNAM).h		$(DESTDIR)$(INCDIR)/
	cp $(AIM_DIR)/$(PRGNAM)_aim.h		$(DESTDIR)$(INCDIR)/

install-lib:
	mkdir -p $(DESTDIR)$(LIBDIR)
	cp $(CORE_DIR)/lib$(PRGNAM).so		$(DESTDIR)$(LIBDIR)/
	cp $(AIM_DIR)/lib$(PRGNAM)_aim.so	$(DESTDIR)$(LIBDIR)/

install-man:
	mkdir -p $(DESTDIR)$(MANDIR)/man$(MANCAT)
	mkdir -p $(DESTDIR)$(MANDIR)/sv/man$(MANCAT)
	cp $(SRC_MANDIR)/$(PRGNAM)_eng.$(MANCAT)\
		$(DESTDIR)$(MANDIR)/man$(MANCAT)/$(PRGNAM).$(MANCAT)
	cp $(SRC_MANDIR)/$(PRGNAM)_swe.$(MANCAT)\
		$(DESTDIR)$(MANDIR)/sv/man$(MANCAT)/$(PRGNAM).$(MANCAT)

uninstall: uninstall-static

uninstall-dynamic:
	$(MAKE) uninstall-bin
	$(MAKE) uninstall-data
	$(MAKE) uninstall-inc
	$(MAKE) uninstall-lib
	$(MAKE) uninstall-man

uninstall-static:
	$(MAKE) uninstall-bin
	$(MAKE) uninstall-data
	$(MAKE) uninstall-man

uninstall-bin:
	rm -f $(DESTDIR)$(BINDIR)/$(BIN)

uninstall-data:
	rm -rf $(DESTDIR)$(DATADIR)/$(PRGNAM)

uninstall-inc:
	rm -f $(DESTDIR)$(INCDIR)/$(PRGNAM).h
	rm -f $(DESTDIR)$(INCDIR)/$(PRGNAM)_aim.h

uninstall-lib:
	rm -f $(DESTDIR)$(LIBDIR)/lib$(PRGNAM).so
	rm -f $(DESTDIR)$(LIBDIR)/lib$(PRGNAM)_aim.so

uninstall-man:
	rm -f $(DESTDIR)$(MANDIR)/man$(MANCAT)/$(PRGNAM).$(MANCAT)
	rm -f $(DESTDIR)$(MANDIR)/sv/man$(MANCAT)/$(PRGNAM).$(MANCAT)

style:
	$(SRC_BUILDDIR)/style.sh\
		`find $(CORE_DIR) -type f -name '*\.[ch]'\
			-o -name 'README' -o -name 'DEPENDS'\
			-o -name 'LICENSE' -o -name '*\.awk'`\
		`find $(AIM_DIR) -type f -name '*\.[ch]'\
			-o -name 'README' -o -name 'DEPENDS'`\
		`find $(UICX_DIR) -type f -name '*\.[ch]'\
			-o -name 'README' -o -name 'DEPENDS'`\
		CHANGES CONTRIBUTING INSTALL LICENSE README TODO

.PHONY: all clean mrproper clean-dist dist\
	dynamic static\
	install install-dynamic install-static\
		install-bin install-data install-inc\
		install-lib install-man\
	uninstall uninstall-dynamic uninstall-static\
		uninstall-bin uninstall-data uninstall-inc\
		uninstall-lib install-man\
	style

