#!/bin/sh

# Version 140729

#
# This script checks for:
#
# *	Overlong lines			(> $COLS)
# *	Trailing spaces or tabs		("[ \t]$")
# *	A white space followed by a tab	(" \t")
#	Note that "\t " does not count!
#

set -e

COLS="${COLS:-72}"

c_expand='expand'
c_grep='grep'

statusall=0
colsexcess="`expr $COLS + 1`"

if	[ "$#" -lt 1 ]
then
	printf '%s FILES/DIRS...\n' "`basename $0`"
	exit 1
fi

check ()
{
	if	[ ! -f "$1" ]
	then
		printf 'FATAL invalid file: %s\n' "$1"
		exit 1
	fi

	statusfile=0

	if	$c_grep -n ' 	' "$1"
	then
		printf 'ERROR mixed white spaces and tabs\n'
		statusfile=1
	fi

	if	$c_grep -n '[ 	]$' "$1"
	then
		printf 'ERROR trailing white space or tab\n'
		statusfile=1
	fi

	if	$c_expand "$1" | $c_grep -n "^.\{$colsexcess,\}$"
	then
		printf 'ERROR overlong line\n'
		statusfile=1
	fi

	return "$statusfile"
}

while	[ "$#" -gt 0 ]
do
	if	check "$1"
	then
		true
	else
		printf 'FAILURE: %s\n' "$1"
		statusall=1
	fi
	shift 1
done

if	[ 0 = "$statusall" ]
then
	printf 'SUCCESS (%s columns)\n' "$COLS"
fi

