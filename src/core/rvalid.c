/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>		/* assert */

#include "board.h"		/* hnef_game_over */
#include "func.h"		/* hnef_rvalid_good */
#include "line.h"		/* hnef_line_empty */
#include "num.h"		/* hnef_single_bit */
#include "playert.h"		/* HNEF_PLAYER_UNINIT */
#include "ruleset.h"		/* hnef_ruleset_valid_size */
#include "rvalid.h"
#include "type_piece.h"		/* hnef_type_piece_get */
#include "type_square.h"	/* hnef_type_square_get */

/*
 * Returns failure if:
 *
 * -	ruleset->id is a 0-length string or NULL.
 * -	ruleset->name is a 0-length string or NULL.
 */
static
enum HNEF_RVALID
hnef_ruleset_valid_gen (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies nothing@*/
{
	assert	(NULL != game);

	/*
	 * Note that `rules->path` can be `NULL`. If so it will be
	 * initialized to an empty line in `game_init_done()`.
	 */

	if	(NULL == game->rules->name
	||	hnef_line_empty(game->rules->name))
	{
		return	HNEF_RVALID_FAIL_NAME;
	}
	else if	(NULL == game->rules->id
	||	hnef_line_empty(game->rules->id))
	{
		return	HNEF_RVALID_FAIL_ID;
	}
	else
	{
		return	HNEF_RVALID_SUCCESS;
	}
}

/*
 * Checks every bit in the bit array, and returns false if any bit:
 *
 * -	Is not 0 or a single bit.
 */
static
HNEF_BOOL
hnef_ruleset_valid_bitarr (
/*@in@*/
/*@notnull@*/
	const HNEF_BIT_U8	* const HNEF_RSTR arr,
	const unsigned short	arrlen
	)
/*@modifies nothing@*/
{
	unsigned short	i;

	assert	(NULL != arr);

	for	(i = 0; i < arrlen; ++i)
	{
		const HNEF_BIT_U8	bit = arr[i];
		if	(HNEF_BIT_U8_EMPTY != bit
		&&	!hnef_single_bit((unsigned int)bit))
		{
			return	HNEF_FALSE;
		}
	}
	return	HNEF_TRUE;
}

/*
 * Returns failure if:
 *
 * -	Any bit in rules->pieces is not among rules->type_pieces
 *	(UNDEF).
 *
 * -	Any type_piece does not appear in rules->pieces (UNUSED).
 *
 * Assumes that all bits in rules->pieces are single bits or
 * HNEF_BIT_U8_EMPTY.
 */
static
enum HNEF_RVALID
hnef_ruleset_valid_bits_def_piece (
/*@in@*/
/*@notnull@*/
	struct hnef_ruleset	* const HNEF_RSTR rules
	)
/*@modifies nothing@*/
{
	unsigned short	i,
			blen;

	assert	(NULL != rules);
	assert	(NULL != rules->pieces);
	assert	(NULL != rules->squares);

	blen	= (unsigned short)(rules->bwidth * rules->bheight);

	for	(i = 0; i < blen; ++i)
	{
		const HNEF_BIT_U8 pbit = rules->pieces[i];
		if	(HNEF_BIT_U8_EMPTY != pbit
		&&	HNEF_BIT_U8_EMPTY == hnef_type_piece_get
						(rules, pbit)->bit)
		{
			return	HNEF_RVALID_FAIL_PIECE_UNDEF;
		}
	}

	for	(i = 0; i < rules->type_piecec; ++i)
	{
		unsigned short j;
		const struct hnef_type_piece * const HNEF_RSTR tp =
			& rules->type_pieces[i];
		for	(j = 0; j < blen; ++j)
		{
			if	(rules->pieces[j] == tp->bit)
			{
				goto	CONTINUE_LOOP;
			}
		}
		return	HNEF_RVALID_FAIL_PIECE_UNUSED;
		CONTINUE_LOOP:;
	}
	return	HNEF_RVALID_SUCCESS;
}

/*
 * Returns failure if:
 *
 * -	Any bit in rules->squares is not among rules->type_squares.
 *
 * -	Any type_square does not appear in rules->squares.
 *
 * Assumes that all bits in rules->squares are single bits or
 * HNEF_BIT_U8_EMPTY.
 */
static
enum HNEF_RVALID
hnef_ruleset_valid_bits_def_square (
/*@in@*/
/*@notnull@*/
	struct hnef_ruleset	* const HNEF_RSTR rules
	)
/*@modifies nothing@*/
{
	unsigned short	i,
			blen;

	assert	(NULL != rules);
	assert	(NULL != rules->pieces);
	assert	(NULL != rules->squares);

	blen	= (unsigned short)(rules->bwidth * rules->bheight);

	for	(i = 0; i < blen; ++i)
	{
		const HNEF_BIT_U8	sqbit = rules->squares[i];
		if	(HNEF_BIT_U8_EMPTY != sqbit
		&&	HNEF_BIT_U8_EMPTY == hnef_type_square_get
						(rules, sqbit)->bit)
		{
			return	HNEF_RVALID_FAIL_SQUARE_UNDEF;
		}
	}

	for	(i = 0; i < rules->type_squarec; ++i)
	{
		unsigned short j;
		const struct hnef_type_square * const HNEF_RSTR ts =
			& rules->type_squares[i];
		for	(j = 0; j < blen; ++j)
		{
			if	(rules->squares[j] == ts->bit)
			{
				goto	CONTINUE_LOOP;
			}
		}
		return	HNEF_RVALID_FAIL_SQUARE_UNUSED;
		CONTINUE_LOOP:;
	}
	return	HNEF_RVALID_SUCCESS;
}

/*
 * Checks if all of the bits in bitmask are among rules->type_pieces. If
 * so, returns true; else (if some is not present) returns false.
 */
static
HNEF_BOOL
hnef_bits_among_tpieces (
/*@in@*/
/*@notnull@*/
	const struct hnef_ruleset	* const HNEF_RSTR rules,
	const HNEF_BIT_U8		bitmask
	)
/*@modifies nothing@*/
{
	unsigned short bit;

	assert	(NULL != rules);

	for	(bit = (unsigned short)1; bit <= (unsigned short)128;
		bit = (unsigned short)(bit * 2))
	{
		unsigned short i;
		if	(((unsigned int)bit & (unsigned int)bitmask)
				!= (unsigned int)bit)
		{
			continue;
		}
		for	(i = 0; i < rules->type_piecec; ++i)
		{
			if	(rules->type_pieces[i].bit
						== (HNEF_BIT_U8)bit)
			{
				goto	CONTINUE_LOOP;
			}
		}
		return	HNEF_FALSE;
		CONTINUE_LOOP:;
	}
	return	HNEF_TRUE;
}

/*
 * Checks if all of the bits in bitmask are among rules->type_squares.
 * If so, returns true; else (if some is not present) returns false.
 */
static
HNEF_BOOL
hnef_bits_among_tsquares (
/*@in@*/
/*@notnull@*/
	const struct hnef_ruleset	* const HNEF_RSTR rules,
	const HNEF_BIT_U8		bitmask
	)
/*@modifies nothing@*/
{
	unsigned short bit;

	assert	(NULL != rules);

	for	(bit = (unsigned short)1; bit <= (unsigned short)128;
		bit = (unsigned short)(bit * 2))
	{
		unsigned short i;
		if	(((unsigned int)bit & (unsigned int)bitmask)
			!= (unsigned int)bit)
		{
			continue;
		}
		for	(i = 0; i < rules->type_squarec; ++i)
		{
			if	(rules->type_squares[i].bit
						== (HNEF_BIT_U8)bit)
			{
				goto	CONTINUE_LOOP;
			}
		}
		return	HNEF_FALSE;
		CONTINUE_LOOP:;
	}
	return	HNEF_TRUE;
}

/*
 * Returns failure if:
 *
 * -	Any piece in tp->captures is owned by tp->owner.
 */
static
enum HNEF_RVALID
hnef_ruleset_valid_tpiece_captures (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	const struct hnef_type_piece	* const HNEF_RSTR tp
	)
/*@modifies nothing@*/
{
	unsigned short bit;

	assert	(NULL != game);
	assert	(NULL != tp);

	for	(bit = (unsigned short)1; bit <= (unsigned short)128;
		bit = (unsigned short)(bit * 2))
	{
		if	(((unsigned int)bit
				& (unsigned int)tp->captures)
				!= (unsigned int)bit)
		{
			continue;
		}
		if	(hnef_type_piece_get(game->rules,
				(HNEF_BIT_U8)bit)->owner == tp->owner)
		{
			return	HNEF_RVALID_FAIL_PIECE_CAPTOWN;
		}
	}
	return	HNEF_RVALID_SUCCESS;
}

/*
 * Returns false if:
 *
 * -	bit is not in arr.
 */
static
HNEF_BOOL
hnef_ruleset_valid_bit_in_arr (
/*@in@*/
/*@notnull@*/
	const HNEF_BIT_U8	* const HNEF_RSTR arr,
	const unsigned short	arrlen,
	const HNEF_BIT_U8	bit
	)
/*@modifies nothing@*/
{
	unsigned short i;

	assert	(NULL != arr);

	for	(i = 0; i < arrlen; ++i)
	{
		if	(bit == arr[i])
		{
			return	HNEF_TRUE;
		}
	}
	return	HNEF_FALSE;
}

/*
 * Returns failure if the player has no pieces on the board.
 */
static
enum HNEF_RVALID
hnef_ruleset_valid_player_pieces (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game,
	unsigned short		p_index
	)
/*@modifies nothing@*/
{
	struct hnef_ruleset	* HNEF_RSTR rules	= NULL;
	unsigned short		pos,
				blen;

	assert	(NULL != game);
	assert	(NULL != game->rules);

	rules	= game->rules;

	assert	(NULL != rules->pieces);
	assert	(NULL != rules->squares);

	blen	= (unsigned short)(rules->bwidth * rules->bheight);

	for	(pos = 0; pos < blen; ++pos)
	{
		const HNEF_BIT_U8	pbit = rules->pieces[pos];
		if (HNEF_BIT_U8_EMPTY != pbit
		&& hnef_type_piece_get(rules, pbit)->owner == p_index)
		{
			return	HNEF_RVALID_SUCCESS;
		}
	}
	return	HNEF_RVALID_FAIL_PIECE_BOARD_NONE;
}

/*
 * Returns false if:
 *
 * -	Some piece owned by p_index has dbl_trap, but no piece owned by
 *	p_index has dbl_trap_capt.
 *
 * -	Some piece owned by p_index has dbl_trap_compl, but no piece
 *	owned by p_index is without dbl_trap_compl.
 */
static
enum HNEF_RVALID
hnef_ruleset_valid_player_dtrap (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game,
	unsigned short		p_index
	)
/*@modifies nothing@*/
{
	unsigned short i;

	assert	(NULL != game);

	for	(i = 0; i < game->rules->type_piecec; ++i)
	{
		const struct hnef_type_piece * const HNEF_RSTR tp =
			& game->rules->type_pieces[i];
		if	(tp->owner == p_index
		&&	tp->dbl_trap)
		{
			unsigned short j = 0;
			for	(; j < game->rules->type_piecec; ++j)
			{
				const struct hnef_type_piece
					* const HNEF_RSTR tp2 =
					& game->rules->type_pieces[j];
				if	(tp2->owner == p_index
				&&	tp2->dbl_trap_capt)
				{
					goto BREAKLOOP;
				}
			}
			/*
			 * A piece has dbl_trap, but no piece has
			 * dbl_trap_capt.
			 */
			return	HNEF_RVALID_FAIL_DTRAP_NOCAPT;
		}
	}
	BREAKLOOP:;

	for	(i = 0; i < game->rules->type_piecec; ++i)
	{
		const struct hnef_type_piece * const HNEF_RSTR tp =
			& game->rules->type_pieces[i];
		if	(tp->owner == p_index
		&&	tp->dbl_trap_compl)
		{
			unsigned short j = 0;
			for	(; j < game->rules->type_piecec; ++j)
			{
				const struct hnef_type_piece
					* const HNEF_RSTR tp2 =
					& game->rules->type_pieces[j];
				if	(tp2->owner == p_index
				&&	!tp2->dbl_trap_compl)
				{
					goto BREAKLOOP2;
				}
			}
			/*
			 * A piece has dbl_trap_compl, but no piece is
			 * without dbl_trap_compl.
			 */
			return	HNEF_RVALID_FAIL_DTRAP_ONLYCOMPL;
		}
	}
	BREAKLOOP2:;

	return	HNEF_RVALID_SUCCESS;
}

static
enum HNEF_RVALID
hnef_ruleset_valid_player (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game,
	unsigned short		p_index
	)
/*@modifies nothing@*/
{
	enum HNEF_RVALID	rv = HNEF_RVALID_SUCCESS;

	assert	(NULL != game);

	if (	!hnef_rvalid_good(rv = hnef_ruleset_valid_player_pieces
						(game, p_index))
	||	!hnef_rvalid_good(rv = hnef_ruleset_valid_player_dtrap
						(game, p_index)))
	{
		return	rv;
	}

	return	HNEF_RVALID_SUCCESS;
}

static
enum HNEF_RVALID
hnef_ruleset_valid_players (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies nothing@*/
{
	unsigned short		i;
	enum HNEF_RVALID	rv = HNEF_RVALID_SUCCESS;

	assert	(NULL != game);

	for	(i = 0; i < game->playerc; ++i)
	{
		if	(!hnef_rvalid_good(rv =
				hnef_ruleset_valid_player(game, i)))
		{
			return	rv;
		}
	}
	return	HNEF_RVALID_SUCCESS;
}

/*
 * Returns false if:
 *
 * -	Any piece bit in game->rules->pieces can not occupy the
 *	corresponding bit in game->rules->squares, id est if a piece
 *	starts on a square that he can not occupy.
 *
 *	This returns false if any piece starts on an off-board (0)
 *	square.
 *
 * -	Any piece bit in game->rules->pieces stands on a square that he
 *	can escape to.
 */
static
enum HNEF_RVALID
hnef_ruleset_valid_tpieces_arr (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies nothing@*/
{
	struct hnef_ruleset	* HNEF_RSTR rules	= NULL;
	unsigned short		i,
				blen;

	assert	(NULL != game);
	assert	(NULL != game->rules);

	rules	= game->rules;

	assert	(NULL != rules->pieces);
	assert	(NULL != rules->squares);

	blen	= (unsigned short)(rules->bwidth * rules->bheight);

	for	(i = 0; i < blen; ++i)
	{
		const HNEF_BIT_U8 pbit	= rules->pieces[i];
		const HNEF_BIT_U8 sqbit	= rules->squares[i];
		const struct hnef_type_piece * HNEF_RSTR tp = NULL;
		if	(HNEF_BIT_U8_EMPTY == pbit)
		{
			continue;
		}
		tp	= hnef_type_piece_get(rules, pbit);

		if	(!hnef_type_piece_can_occupy(tp, sqbit))
		{
			return	HNEF_RVALID_FAIL_PIECE_STARTNOOCCUP;
		}

		if	((((unsigned int)pbit
			& (unsigned int)rules->opt_tp_escape)
				== (unsigned int)pbit)
		&&	(((unsigned int)sqbit
			& (unsigned int)rules->opt_ts_escape)
				== (unsigned int)sqbit))
		{
			return	HNEF_RVALID_FAIL_PIECE_ESCAPESTART;
		}
	}
	return	HNEF_RVALID_SUCCESS;
}

/*
 * Validates a single type_piece.
 *
 * Returns false if:
 *
 * -	Owner index is out of game->playerc bounds (owned does not
 *	exist).
 *
 * -	Any bit in tp->captures is not a defined type_piece.
 *
 * -	Any bit in tp->noreturn is not a defined type_square.
 *
 * -	Any bit in tp->occupies is not a defined type_square.
 *
 * -	Any bit in tp->dbl_trap_squares is not a defined type_square.
 *
 * -	Any bit in tp->noreturn, tp->dbl_trap_squares or tp->occupies
 *	does not exist in game->rules->squares.
 *
 * -	Any bit in tp->noreturn is not occupiable by tp.
 *
 * -	If tp->dbl_trap is set, but tp->dbl_trap_squares does not
 *	contain any square bit.
 */
static
enum HNEF_RVALID
hnef_ruleset_valid_tpiece (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	const struct hnef_type_piece	* const HNEF_RSTR tp
	)
/*@modifies nothing@*/
{
	struct hnef_ruleset	* HNEF_RSTR rules	= NULL;
	unsigned short		bit,
				blen;
	enum HNEF_RVALID	rv		= HNEF_RVALID_SUCCESS;

	assert	(NULL != game);
	assert	(NULL != game->rules);

	rules	= game->rules;

	assert	(NULL != rules->pieces);
	assert	(NULL != rules->squares);
	assert	(NULL != tp);

	blen	= (unsigned short)(rules->bwidth * rules->bheight);

	if	(tp->owner >= game->playerc
	||	tp->owner == HNEF_PLAYER_UNINIT)
	{
		return	HNEF_RVALID_FAIL_PIECE_OWNER;
	}

	if	(!hnef_bits_among_tpieces(rules, tp->captures))
	{
		return	HNEF_RVALID_FAIL_PIECE_UNDEF;
	}

	if	(!hnef_bits_among_tsquares(rules, tp->noreturn)
	||	!hnef_bits_among_tsquares(rules, tp->dbl_trap_squares)
	||	!hnef_bits_among_tsquares(rules, tp->occupies))
	{
		return	HNEF_RVALID_FAIL_SQUARE_UNDEF;
	}

	if	(tp->dbl_trap
	&&	HNEF_BIT_U8_EMPTY == tp->dbl_trap_squares)
	{
		/*
		 * If double trap is set, then it doesn't make sense if
		 * no square can trigger double trap.
		 */
		return	HNEF_RVALID_FAIL_DTRAP_NOSQUARES;
	}

	if	(tp->capt_sides < HNEF_TPIECE_CAPT_SIDES_MIN
	||	tp->capt_sides > HNEF_TPIECE_CAPT_SIDES_MAX)
	{
		return	HNEF_RVALID_FAIL_PIECE_CAPTSIDES;
	}

	if	(!hnef_rvalid_good(rv =
			hnef_ruleset_valid_tpiece_captures(game, tp)))
	{
		return	rv;
	}

	if	(tp->custodial
	&&	tp->capt_sides < (unsigned short)2)
	{
		return	HNEF_RVALID_FAIL_PIECE_CAPTSIDES_CUST;
	}

	if	(tp->capt_edge
	&&	tp->capt_sides < (unsigned short)2)
	{
		return	HNEF_RVALID_FAIL_PIECE_CAPTSIDES_EDGE;
	}

	if	(!hnef_ruleset_valid_bit_in_arr(rules->pieces,
						blen, tp->bit))
	{
		return	HNEF_RVALID_FAIL_PIECE_UNUSED;
	}

	if	((	tp->dbl_trap_capt
		||	tp->dbl_trap_compl
		||	tp->dbl_trap_edge
		||	tp->dbl_trap_encl)
	&&	!tp->dbl_trap)
	{
		return	HNEF_RVALID_FAIL_DTRAP_INCONSISTENT;
	}

	/*
	 * Check that all noreturn, occupies and dbl_trap_squares
	 * squares exist, and that all squares in noreturn and
	 * dbl_trap_squares are occupiable by tp.
	 */
	for	(bit = (unsigned short)1; bit <= (unsigned short)128;
		bit = (unsigned short)(bit * 2))
	{
		if	((((unsigned int)tp->noreturn
			| (unsigned int)tp->dbl_trap_squares
			| (unsigned int)tp->occupies)
			& (unsigned int)bit) == (unsigned int)bit)
		{
			/*
			 * bit is in noreturn, occupies or
			 * dbl_trap_squares.
			 */
			if	(!hnef_ruleset_valid_bit_in_arr
					(rules->squares, blen,
					(HNEF_BIT_U8)bit))
			{
				return	HNEF_RVALID_FAIL_SQUARE_UNUSED;
			}
		}

		if	((((unsigned int)tp->dbl_trap_squares
			| (unsigned int)tp->noreturn)
			& (unsigned int)bit) == (unsigned int)bit)
		{
			/*
			 * bit is in noreturn or dbl_trap_squares.
			 */
			if	(!hnef_type_piece_can_occupy(tp,
						(HNEF_BIT_U8)bit))
			{
				return
				HNEF_RVALID_FAIL_PIECE_REFINOCCUP;
			}
		}
	}

	return	HNEF_RVALID_SUCCESS;
}

/*
 * Validates all type_pieces.
 */
static
enum HNEF_RVALID
hnef_ruleset_valid_tpieces (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies nothing@*/
{
	unsigned short		i;
	enum HNEF_RVALID	rv = HNEF_RVALID_SUCCESS;

	assert	(NULL != game);

	for	(i = 0; i < game->rules->type_piecec; ++i)
	{
		if	(!hnef_rvalid_good
			(rv = hnef_ruleset_valid_tpiece(game,
				& game->rules->type_pieces[i])))
		{
			return	rv;
		}
	}
	return	HNEF_RVALID_SUCCESS;
}

/*
 * Validates a single type_square.
 *
 * Returns false if:
 *
 * -	Any bit in `ts->captures` is not a defined type_piece.
 *
 * -	Any bit in `ts->capt_sides_pieces` is not a defined type_piece.
 *
 * -	`ts->capt_sides` is defined but `ts->capt_sides_pieces` does not
 *	contain any pieces.
 *
 * -	Any piece in `ts->capt_sides_escape` can not occupy `ts`.
 */
static
enum HNEF_RVALID
hnef_ruleset_valid_tsquare (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	const struct hnef_type_square	* const HNEF_RSTR ts
	)
/*@modifies nothing@*/
{
	struct hnef_ruleset	* HNEF_RSTR rules	= NULL;
	unsigned short		blen;

	assert	(NULL != game);
	assert	(NULL != game->rules);

	rules	= game->rules;

	assert	(NULL != rules->pieces);
	assert	(NULL != rules->squares);
	assert	(NULL != ts);

	blen	= (unsigned short)(rules->bwidth * rules->bheight);

	if	(!hnef_bits_among_tpieces(rules, ts->captures)
	||	!hnef_bits_among_tpieces(rules, ts->capt_sides_pieces))
	{
		return	HNEF_RVALID_FAIL_PIECE_UNDEF;
	}

	if	(ts->capt_sides != HNEF_TSQUARE_CAPT_SIDES_NONE)
	{
		if	(ts->capt_sides < HNEF_TPIECE_CAPT_SIDES_MIN
		||	ts->capt_sides > HNEF_TPIECE_CAPT_SIDES_MAX)
		{
			return	HNEF_RVALID_FAIL_SQUARE_CAPTSIDES;
		}
		if	(HNEF_BIT_U8_EMPTY == ts->capt_sides_pieces)
		{
			return
			HNEF_RVALID_FAIL_SQUARE_CAPTSIDES_NOPIECE;
		}
	}

	if	(!hnef_ruleset_valid_bit_in_arr(rules->squares,
						blen, ts->bit))
	{
		return	HNEF_RVALID_FAIL_SQUARE_UNUSED;
	}

	if	(ts->capt_sides != HNEF_TSQUARE_CAPT_SIDES_NONE)
	{
		unsigned short	i;
		for	(i = 0; i < rules->type_piecec; ++i)
		{
			const struct hnef_type_piece
				* const HNEF_RSTR tp =
				& rules->type_pieces[i];
			const HNEF_BIT_U8 pbit = tp->bit;
			if	(((unsigned int)pbit
				& (unsigned int)ts->capt_sides_pieces)
				!= (unsigned int)pbit)
			{
				continue;
			}

			if	(((unsigned int)tp->occupies
				& (unsigned int)ts->bit)
				!= (unsigned int)ts->bit)
			{
				/*
				 * Piece can't occupy a square which
				 * overrides its `capt_sides`.
				 */
				return
			HNEF_RVALID_FAIL_SQUARE_CAPTSIDES_NOOCCUP;
			}
		}
	}

	return	HNEF_RVALID_SUCCESS;
}

/*
 * Validates all type_squares.
 */
static
enum HNEF_RVALID
hnef_ruleset_valid_tsquares (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies nothing@*/
{
	unsigned short		i;
	enum HNEF_RVALID	rv = HNEF_RVALID_SUCCESS;

	assert	(NULL != game);

	for	(i = 0; i < game->rules->type_squarec; ++i)
	{
		if	(!hnef_rvalid_good
				(rv = hnef_ruleset_valid_tsquare(game,
				& game->rules->type_squares[i])))
		{
			return	rv;
		}
	}
	return	HNEF_RVALID_SUCCESS;
}

/*
 * Checks that if a type_piece has escape, then a type_square must have
 * it too, and vice versa.
 *
 * However, it doesn't check that a piece with escape can occupy some
 * square with escape.
 */
static
enum HNEF_RVALID
hnef_ruleset_valid_escape (
/*@in@*/
/*@notnull@*/
	const struct hnef_ruleset	* const HNEF_RSTR rules
	)
/*@modifies nothing@*/
{
	assert	(NULL != rules);

	return	(HNEF_BIT_U8_EMPTY == rules->opt_tp_escape)
	==	(HNEF_BIT_U8_EMPTY == rules->opt_ts_escape)
		? HNEF_RVALID_SUCCESS
		: HNEF_RVALID_FAIL_ESCAPE_NO_PIECE_XOR_SQUARE;
}

/*
 * Checks if `game` is valid.
 *
 * Call this after reading a game object (from a ruleset file) using
 * rread.c.
 *
 * Refer to the individual functions in this file to see exactly what's
 * checked.
 *
 * Although this function modifies `game`, it restores it to its
 * original state before returning.
 *
 * Note that this function does not guarantee that the game rules are
 * valid. It just checks for some obvious errors. There may be other
 * errors that result in an unplayable game. Furthermore it makes no
 * attempt to check if the game rules make sense -- for example, it
 * would be possible to define a ruleset in which no piece can be
 * captured and where the game can only be won by capturing some pieces;
 * and it would be possible to create a board that is cut off in two
 * halves with one player's pieces on the bottom half and the other
 * player's on the top half so that they can't reach each other.
 *
 * May be `NULL` or `!NULL`:
 *
 * -	`game->rules->path`
 *
 * Expected to be `NULL` (will be initialized by `game_init_done()`, so
 * if this is called after `game_init_done()`, it will not be `NULL`):
 *
 * -	`game->board`
 *
 * Others may not be `NULL`.
 */
enum HNEF_RVALID
hnef_game_valid (
	struct hnef_game	* const HNEF_RSTR game
	)
{
	struct hnef_ruleset	* HNEF_RSTR rules = NULL;
	enum HNEF_RVALID	rv		= HNEF_RVALID_SUCCESS;
	unsigned short		blen;

	assert	(NULL != game);

	if	(game->opt_valid_known)
	{
		return	game->opt_valid_code;
	}

	rules	= game->rules;

	if	(!hnef_ruleset_valid_size(rules->bwidth)
	||	!hnef_ruleset_valid_size(rules->bheight))
	{
		rv	= HNEF_RVALID_FAIL_BSIZE;
		goto	RETURN_RV;
	}

	blen	= (unsigned short)(rules->bwidth * rules->bheight);

	if	(NULL == rules->pieces)
	{
		rv	= HNEF_RVALID_FAIL_NULL_PIECES;
		goto	RETURN_RV;
	}
	else if	(NULL == rules->squares)
	{
		rv	= HNEF_RVALID_FAIL_NULL_SQUARES;
		goto	RETURN_RV;
	}
	else if	(!hnef_rvalid_good(rv = hnef_ruleset_valid_gen(game)))
	{
		goto	RETURN_RV;
	}
	else if	(!hnef_ruleset_valid_bitarr(rules->squares, blen)
	||	!hnef_ruleset_valid_bitarr(rules->pieces, blen))
	{
		rv	= HNEF_RVALID_FAIL_BITMASK;
		goto	RETURN_RV;
	}
	else if	(!hnef_rvalid_good(rv =
			hnef_ruleset_valid_bits_def_piece(rules))
	||	!hnef_rvalid_good(rv =
			hnef_ruleset_valid_bits_def_square(rules)))
	{
		goto	RETURN_RV;
	}
	else if	(!hnef_rvalid_good(rv = hnef_ruleset_valid_tpieces_arr
							(game)))
	{
		goto	RETURN_RV;
	}
	else if	(!hnef_rvalid_good(rv = hnef_ruleset_valid_tpieces
							(game))
	||	!hnef_rvalid_good(rv = hnef_ruleset_valid_tsquares
							(game))
	||	!hnef_rvalid_good(rv = hnef_ruleset_valid_escape
							(rules)))
	{
		goto	RETURN_RV;
	}
	else if	(!hnef_rvalid_good(rv = hnef_ruleset_valid_players
							(game)))
	{
		goto	RETURN_RV;
	}
	else
	{
		rv	= HNEF_RVALID_SUCCESS;
		goto	RETURN_RV;
	}

	RETURN_RV:
	game->opt_valid_known	= HNEF_TRUE;
	game->opt_valid_code	= rv;
	return			rv;
}

