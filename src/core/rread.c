/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#ifndef S_SPLINT_S	/* SPLint bug */
#include <ctype.h>	/* isspace */
#endif
#include <limits.h>	/* *_MAX */
#include <string.h>	/* strcmp */

#include "func.h"		/* hnef_fr_* */
#include "game.h"		/* hnef_game_* */
#include "num.h"		/* hnef_textto_* */
#include "rread.h"
#include "rreadt.h"		/* HNEF_RREAD_L_* */
#include "line.h"		/* hnef_line_* */
#include "linet.h"		/* HNEF_PARSE_* */
#include "movet.h"		/* HNEF_BOARDPOS_NONE */
#include "ruleset.h"		/* hnef_ruleset_valid_size */
#include "rvalid.h"		/* hnef_game_valid */
#include "type_piece.h"		/* hnef_type_piece_* */
#include "type_square.h"	/* hnef_type_square_* */

/*
 * Parses "piece/square x <arg>", where <arg> is a HNEF_BIT_U8 (pointed
 * to by bit).
 *
 * Only makes sure that the bit can be converted from a string. Does not
 * validate the value in any way.
 *
 * bit is unchanged if it fails.
 */
static
enum HNEF_FR
hnef_parse_type_bitu8 (
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR argame,
/*@in@*/
/*@notnull@*/
	HNEF_BIT_U8	* const HNEF_RSTR bit,
/*@in@*/
/*@notnull@*/
	enum HNEF_RREAD	* const HNEF_RSTR fail_read
	)
/*@globals errno@*/
/*@modifies errno, * bit, * fail_read@*/
{
	unsigned short num;

	assert	(NULL != argame);
	assert	(NULL != bit);
	assert	(NULL != fail_read);

	if	(!hnef_texttoushort(argame, & num))
	{
		* fail_read	= HNEF_RREAD_FAIL_STRTOL;
		return		HNEF_FR_SUCCESS;
	}
/*@i1@*/\
	else if	(num > (unsigned short)UCHAR_MAX)
	{
		* fail_read	= HNEF_RREAD_FAIL_NOOB;
		return		HNEF_FR_SUCCESS;
	}
	* bit	= (HNEF_BIT_U8)num;
	return	HNEF_FR_SUCCESS;
}

/*
 * Parses "piece/square x <arg>", where <arg> is an unsigned short
 * (pointed to by shrt).
 *
 * Only makes sure that the number can be converted from a string. Does
 * not validate the value in any way.
 */
static
enum HNEF_FR
hnef_parse_type_ushrt (
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR argame,
/*@in@*/
/*@notnull@*/
	unsigned short	* const HNEF_RSTR shrt,
/*@in@*/
/*@notnull@*/
	enum HNEF_RREAD	* const HNEF_RSTR fail_read
	)
/*@globals errno@*/
/*@modifies errno, * shrt, * fail_read@*/
{
	assert	(NULL != argame);
	assert	(NULL != shrt);
	assert	(NULL != fail_read);

	if	(!hnef_texttoushort(argame, shrt))
	{
		* fail_read	= HNEF_RREAD_FAIL_STRTOL;
		return		HNEF_FR_SUCCESS;
	}
	return	HNEF_FR_SUCCESS;
}

/*
 * Parses "piece x <arg>", where x is a single bit, and tp is the piece
 * corresponding to bit x. "piece x" has already been read by
 * `hnef_parse_type`.
 */
static
enum HNEF_FR
hnef_parse_piece_arg (
/*@in@*/
/*@notnull@*/
	struct hnef_type_piece	* const HNEF_RSTR tp,
/*@in@*/
/*@notnull@*/
	enum HNEF_RREAD		* const	HNEF_RSTR fail_read
	)
/*@globals errno, internalState@*/
/*@modifies errno, internalState, * tp, * fail_read@*/
{
	char * tken = NULL;

	assert	(NULL != tp);
	assert	(NULL != fail_read);

	if	(NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
	||	hnef_line_empty(tken))
	{
		* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
		return		HNEF_FR_SUCCESS;
	}

	if	(0 == strcmp(tken, HNEF_RREAD_L_CAPT_EDGE))
	{
		tp->capt_edge	= HNEF_TRUE;
		return		HNEF_FR_SUCCESS;
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_CAPT_LOSS))
	{
		tp->capt_loss	= HNEF_TRUE;
		return		HNEF_FR_SUCCESS;
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_CUSTODIAL))
	{
		tp->custodial	= HNEF_TRUE;
		return		HNEF_FR_SUCCESS;
	}
	else if (0 == strcmp(tken, HNEF_RREAD_L_DBL_TRAP))
	{
		tp->dbl_trap	= HNEF_TRUE;
		return		HNEF_FR_SUCCESS;
	}
	else if (0 == strcmp(tken, HNEF_RREAD_L_DBL_TRAP_CAPT))
	{
		tp->dbl_trap_capt	= HNEF_TRUE;
		return			HNEF_FR_SUCCESS;
	}
	else if (0 == strcmp(tken, HNEF_RREAD_L_DBL_TRAP_COMPL))
	{
		tp->dbl_trap_compl	= HNEF_TRUE;
		return			HNEF_FR_SUCCESS;
	}
	else if (0 == strcmp(tken, HNEF_RREAD_L_DBL_TRAP_EDGE))
	{
		tp->dbl_trap_edge	= HNEF_TRUE;
		return			HNEF_FR_SUCCESS;
	}
	else if (0 == strcmp(tken, HNEF_RREAD_L_DBL_TRAP_ENCL))
	{
		tp->dbl_trap_encl	= HNEF_TRUE;
		return			HNEF_FR_SUCCESS;
	}
	else if (0 == strcmp(tken, HNEF_RREAD_L_ESCAPE))
	{
		tp->escape		= HNEF_TRUE;
		return			HNEF_FR_SUCCESS;
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_CAPTURES))
	{
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
		|| hnef_line_empty(tken))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}
		return	hnef_parse_type_bitu8
			(tken, & tp->captures, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_NORETURN))
	{
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
		|| hnef_line_empty(tken))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}
		return	hnef_parse_type_bitu8
			(tken, & tp->noreturn, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_OCCUPIES))
	{
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
		|| hnef_line_empty(tken))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}
		return	hnef_parse_type_bitu8
			(tken, & tp->occupies, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_CAPT_SIDES))
	{
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
		|| hnef_line_empty(tken))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}
		return	hnef_parse_type_ushrt
			(tken, & tp->capt_sides, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_OWNER))
	{
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
		|| hnef_line_empty(tken))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}
		return	hnef_parse_type_ushrt
			(tken, & tp->owner, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_UI_BIT))
	{
		enum HNEF_FR fr = HNEF_FR_SUCCESS;
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
		|| hnef_line_empty(tken))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}

		fr	= hnef_parse_type_bitu8
			(tken, & tp->ui_bit, fail_read);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}
		else if	(HNEF_BIT_U8_EMPTY != tp->ui_bit
		&&	!hnef_single_bit((unsigned int)tp->ui_bit))
		{
			* fail_read	= HNEF_RREAD_FAIL_BIT_SINGLE;
			return		HNEF_FR_SUCCESS;
		}
		else
		{
			return	HNEF_FR_SUCCESS;
		}
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_DBL_TRAP_SQ))
	{
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
		|| hnef_line_empty(tken))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}
		return	hnef_parse_type_bitu8
			(tken, & tp->dbl_trap_squares, fail_read);
	}
/*
 * NOTE:	Additional arguments go here.
 */
	else
	{
		* fail_read	= HNEF_RREAD_FAIL_ARG_UNK;
		return		HNEF_FR_SUCCESS;
	}
}

/*
 * Parses "square x <arg>", where x is a single bit, and ts is the
 * square corresponding to bit x. "square x" has already been read by
 * `hnef_parse_type`.
 */
static
enum HNEF_FR
hnef_parse_square_arg (
/*@in@*/
/*@notnull@*/
	struct hnef_type_square	* const HNEF_RSTR ts,
/*@in@*/
/*@notnull@*/
	enum HNEF_RREAD		* const	HNEF_RSTR fail_read
	)
/*@globals errno, internalState@*/
/*@modifies errno, internalState, * ts, * fail_read@*/
{
	char * tken = NULL;

	assert	(NULL != ts);
	assert	(NULL != fail_read);

	if	(NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
	||	hnef_line_empty(tken))
	{
		* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
		return		HNEF_FR_SUCCESS;
	}

	if	(0 == strcmp(tken, HNEF_RREAD_L_ESCAPE))
	{
		ts->escape		= HNEF_TRUE;
		return			HNEF_FR_SUCCESS;
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_CAPTURES))
	{
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
		|| hnef_line_empty(tken))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}
		return	hnef_parse_type_bitu8
			(tken, & ts->captures, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_CAPT_SIDES))
	{
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
		|| hnef_line_empty(tken))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}
		return	hnef_parse_type_ushrt
			(tken, & ts->capt_sides, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_CAPT_SIDES_PIECES))
	{
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
		|| hnef_line_empty(tken))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}
		return	hnef_parse_type_bitu8
			(tken, & ts->capt_sides_pieces, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_UI_BIT))
	{
		enum HNEF_FR fr = HNEF_FR_SUCCESS;
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
		|| hnef_line_empty(tken))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}

		fr	= hnef_parse_type_bitu8
			(tken, & ts->ui_bit, fail_read);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}
		else if	(HNEF_BIT_U8_EMPTY != ts->ui_bit
		&&	!hnef_single_bit((unsigned int)ts->ui_bit))
		{
			* fail_read	= HNEF_RREAD_FAIL_BIT_SINGLE;
			return		HNEF_FR_SUCCESS;
		}
		else
		{
			return	HNEF_FR_SUCCESS;
		}
	}
/*
 * NOTE:	Additional arguments go here.
 */
	else
	{
		* fail_read	= HNEF_RREAD_FAIL_ARG_UNK;
		return		HNEF_FR_SUCCESS;
	}
}

/*
 * Parses "piece/square bit <arg>", where "piece/square bit" has already
 * been read by `hnef_parse_type`.
 *
 * The <arg> is added as a type_piece or type_square in game->rules.
 *
 * If read_sq, then it's a square type, else it's a piece type.
 */
static
enum HNEF_FR
hnef_parse_type_bit (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
	const HNEF_BOOL		read_sq,
/*@in@*/
/*@notnull@*/
	enum HNEF_RREAD		* const HNEF_RSTR fail_read
	)
/*@globals errno, internalState@*/
/*@modifies errno, internalState, * game, * fail_read@*/
{
	char		* tken	= NULL;
	unsigned short	num;

	assert	(NULL != game);
	assert	(NULL != fail_read);

	if	(NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
	||	hnef_line_empty(tken))
	{
		* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
		return		HNEF_FR_SUCCESS;
	}
/*@i1@*/\
	else if	(game->rules->type_piecec >= HNEF_TYPE_MAX)
	{
		* fail_read	= HNEF_RREAD_FAIL_TYPE_OVERFLOW;
		return		HNEF_FR_SUCCESS;
	}

	if	(!hnef_texttoushort(tken, & num))
	{
		* fail_read	= HNEF_RREAD_FAIL_STRTOL;
		return		HNEF_FR_SUCCESS;
	}
	else if	(num > (unsigned short)128
	||	!hnef_single_bit((unsigned int)num))
	{
		* fail_read	= HNEF_RREAD_FAIL_BIT_SINGLE;
		return		HNEF_FR_SUCCESS;
	}
	else
	{
		const HNEF_BIT_U8 bit	= (HNEF_BIT_U8)num;
		enum HNEF_FR fr		= HNEF_FR_SUCCESS;
		if	(read_sq)
		{
			fr = hnef_type_square_set(game->rules, bit);
		}
		else
		{
			fr = hnef_type_piece_set(game->rules, bit);
		}

		if	(HNEF_FR_FAIL_ILL_ARG == fr)
		{
			* fail_read	= HNEF_RREAD_FAIL_BIT_SINGLE;
			return		HNEF_FR_SUCCESS;
		}
		else if	(HNEF_FR_FAIL_ILL_STATE == fr)
		{
			* fail_read	= HNEF_RREAD_FAIL_TYPE_DUP;
			return		HNEF_FR_SUCCESS;
		}
		else if	(HNEF_FR_FAIL_NULLPTR == fr)
		{
			* fail_read	= HNEF_RREAD_FAIL_TYPE_ORDER;
			return		HNEF_FR_SUCCESS;
		}
		else
		{
			return	fr;
		}
	}
}

/*
 * Parses "piece/square <arg>", where <arg> is either a HNEF_BIT_U8 of
 * an argument (and "piece/square" has already been read by
 * "hnef_game_parseline").
 *
 * read_sq is true if we're parsing a square bit; false if it's a piece
 * bit.
 */
static
enum HNEF_FR
hnef_parse_type (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
	const HNEF_BOOL		read_sq,
/*@in@*/
/*@notnull@*/
	enum HNEF_RREAD		* const HNEF_RSTR fail_read
	)
/*@globals errno, internalState@*/
/*@modifies errno, internalState, * game, * fail_read@*/
{
	char	* tken	= NULL;

	assert	(NULL != game);
	assert	(NULL != fail_read);

	if	(NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS))
	||	hnef_line_empty(tken))
	{
		* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
		return		HNEF_FR_SUCCESS;
	}

	if	(0 == strcmp(tken, HNEF_RREAD_L_BIT))
	{
		return	hnef_parse_type_bit(game, read_sq, fail_read);
	}
	else
	{
		HNEF_BIT_U8	bit;
		{
			unsigned short	num;
			if	(!hnef_texttoushort(tken, & num))
			{
				* fail_read = HNEF_RREAD_FAIL_STRTOL;
				return	HNEF_FR_SUCCESS;
			}
/*@i1@*/\
			else if	(num > (unsigned short)128
			||	!hnef_single_bit((unsigned int)num))
			{
				* fail_read =
					HNEF_RREAD_FAIL_BIT_SINGLE;
				return	HNEF_FR_SUCCESS;
			}
			bit	= (HNEF_BIT_U8)num;
		}
		if	(read_sq)
		{
			struct hnef_type_square * const ts =
				hnef_type_square_get(game->rules, bit);
			if	(HNEF_BIT_U8_EMPTY == ts->bit)
			{
				* fail_read =
					HNEF_RREAD_FAIL_TYPE_UNDEF;
				return	HNEF_FR_SUCCESS;
			}
			return hnef_parse_square_arg(ts, fail_read);
		}
		else
		{
			struct hnef_type_piece * const tp =
				hnef_type_piece_get(game->rules, bit);
			if	(HNEF_BIT_U8_EMPTY == tp->bit)
			{
				* fail_read =
					HNEF_RREAD_FAIL_TYPE_UNDEF;
				return	HNEF_FR_SUCCESS;
			}
			return hnef_parse_piece_arg(tp, fail_read);
		}
	}
}

/*
 * Reads the pieces or squares array in ruleset. If read_sq, then read
 * game->rules->squares; else read game->rules->pieces.
 *
 * This requires that width and height are set. If they are not (i.e. if
 * they are invalid), then it fails.
 */
static
enum HNEF_FR
hnef_parse_arrb (
/*@partial@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
	const HNEF_BOOL		read_sq,
/*@in@*/
/*@notnull@*/
	enum HNEF_RREAD		* const HNEF_RSTR fail_read
	)
/*@globals errno, internalState@*/
/*@modifies errno, internalState, * game, * fail_read@*/
{
	struct hnef_ruleset	* HNEF_RSTR rules	= NULL;
	unsigned short		num,
				blen,
				i;
	HNEF_BIT_U8 * HNEF_RSTR	arr;

	assert	(NULL != game);
	assert	(NULL != fail_read);

	rules	= game->rules;

	if	(!hnef_ruleset_valid_size(rules->bwidth)
	||	!hnef_ruleset_valid_size(rules->bheight))
	{
		* fail_read	= HNEF_RREAD_FAIL_NOPREL_BSIZE;
		return		HNEF_FR_SUCCESS;
	}
	blen	= (unsigned short)(rules->bwidth * rules->bheight);

	if	(read_sq)
	{
		if	(NULL != rules->squares)
		{
			/*
			 * Already read (or not cleared from before).
			 */
			* fail_read	= HNEF_RREAD_FAIL_ARG_DUP;
			return		HNEF_FR_SUCCESS;
		}
		rules->squares	= malloc(sizeof(* rules->squares)
				* (size_t)blen);
		arr	= rules->squares;
	}
	else
	{
		if	(NULL != rules->pieces)
		{
			/*
			 * Already read (or not cleared from before).
			 */
			* fail_read	= HNEF_RREAD_FAIL_ARG_DUP;
			return		HNEF_FR_SUCCESS;
		}
		rules->pieces	= malloc(sizeof(* rules->pieces)
				* (size_t)blen);
		arr	= rules->pieces;
	}
	if	(NULL == arr)
	{
		return	HNEF_FR_FAIL_ALLOC;
	}

	/*
	 * First word is "pieces" / "squares" argument, so skip it.
	 */
	for	(i = 0; i < blen; ++i)
	{
		char * tken = NULL;
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS)))
		{
			/*
			 * Too few arguments.
			 */
			* fail_read	= HNEF_RREAD_FAIL_ARRB_SIZE;
			return		HNEF_FR_SUCCESS;
		}

		if	(!hnef_texttoushort(tken, & num))
		{
			* fail_read	= HNEF_RREAD_FAIL_STRTOL;
			return		HNEF_FR_SUCCESS;
		}
		else if	(num > (unsigned short)128
		|| (0 != num && !hnef_single_bit((unsigned int)num)))
		{
			* fail_read	= HNEF_RREAD_FAIL_BIT_SINGLE;
			return		HNEF_FR_SUCCESS;
		}
		else
		{
			arr[i]		= (HNEF_BIT_U8)num;
		}
	}

	if	(NULL != strtok(NULL, HNEF_PARSE_DELIMS))
	{
		/*
		 * We should be out of tkens now.
		 */
		* fail_read	= HNEF_RREAD_FAIL_ARG_EXCESS;
		return		HNEF_FR_SUCCESS;
	}

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_parse_strarg (
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR str,
/*@in@*/
/*@notnull@*/
	char		* HNEF_RSTR * const HNEF_RSTR ptr,
/*@in@*/
/*@notnull@*/
	enum HNEF_RREAD	* const HNEF_RSTR fail_read
	)
/*@modifies ptr, * fail_read@*/
{
	assert	(NULL != str);
	assert	(NULL != ptr);
	assert	(NULL != fail_read);

	if	(hnef_line_empty(str))
	{
		* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
		return		HNEF_FR_SUCCESS;
	}

	* ptr	= hnef_line_alloc_cpy(str);
	return	NULL == * ptr
		? HNEF_FR_FAIL_ALLOC
/* SPLint pointer to pointer (pointer to C-string */ /*@i1@*/\
		: HNEF_FR_SUCCESS;
}

/*
 * Reads "width" or "height" into the "size" pointer.
 *
 * "size" is a pointer to rules->bwidth or rules->bheight.
 */
static
enum HNEF_FR
hnef_parse_size (
/*@in@*/
/*@notnull@*/
	const struct hnef_ruleset	* const HNEF_RSTR rules,
/*@in@*/
/*@notnull@*/
	const char			* const HNEF_RSTR str,
/*@in@*/
/*@notnull@*/
	unsigned short			* const HNEF_RSTR size,
/*@in@*/
/*@notnull@*/
	enum HNEF_RREAD			* const HNEF_RSTR fail_read
	)
/*@globals errno@*/
/*@modifies errno, * size, * fail_read@*/
{
	assert	(NULL != rules);
	assert	(NULL != str);
	assert	(NULL != size);
	assert	(NULL != fail_read);

	if	(NULL != rules->pieces
	||	NULL != rules->squares)
	{
		* fail_read	= HNEF_RREAD_FAIL_ARRB_PRESIZE;
		return		HNEF_FR_SUCCESS;
	}

	if	(hnef_line_empty(str))
	{
		* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
		return		HNEF_FR_SUCCESS;
	}
/*@i2@*/\
	else if	(HNEF_BOARDPOS_NONE != * size)
	{
		* fail_read	= HNEF_RREAD_FAIL_ARG_DUP;
		return		HNEF_FR_SUCCESS;
	}

	if	(!hnef_texttoushort(str, size))
	{
		* size		= HNEF_BOARDPOS_NONE;
		* fail_read	= HNEF_RREAD_FAIL_STRTOL;
		return		HNEF_FR_SUCCESS;
	}
/*@i2@*/\
	else if	(!hnef_ruleset_valid_size(* size))
	{
		* size		= HNEF_BOARDPOS_NONE;
		* fail_read	= HNEF_RREAD_FAIL_NOOB;
		return		HNEF_FR_SUCCESS;
	}
	return	HNEF_FR_SUCCESS;
}

/*
 * Returns a pointer to the second token in `line`, or `null` if it
 * doesn't exist.
 */
/*@in@*/
/*@null@*/
static
char *
hnef_game_namearg (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	char	* HNEF_RSTR line
	)
/*@*/
{
	HNEF_BOOL space_found = HNEF_FALSE;

	assert	(NULL != line);

	while	('\0' != line[0])
	{
		/*
		 * NOTE: This is assumed to be identical to the `strtok`
		 * tokenization space characters used in this and other
		 * source files.
		 */
		if	(isspace((int)line[0]))
		{
			space_found = HNEF_TRUE;
		}
/*@i1@*/\
		else if	(space_found)
		{
			return line;
		}
		++line;
	}
	return NULL;
}

/*
 * Parses a single line from a ruleset file. The line is tokenized.
 * There is no syntax other than tokenization.
 *
 * `line` is mangled by `strtok` (and it can therefore not be
 * read-only).
 */
enum HNEF_FR
hnef_game_parseline (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	char			* const line,
/*@out@*/
/*@notnull@*/
	enum HNEF_RREAD		* const HNEF_RSTR fail_read
	)
/*@globals internalState@*/
/*@modifies internalState, * game, * line, * fail_read@*/
{
/*@temp@*/
	char	* tken		= NULL,
		* namearg	= NULL;

	assert	(NULL != game);
	assert	(NULL != line);
	assert	(NULL != fail_read);

	* fail_read	= HNEF_RREAD_SUCCESS;
	namearg		= hnef_game_namearg(line);

	if	(NULL == (tken = strtok(line, HNEF_PARSE_DELIMS)))
	{
		return HNEF_FR_SUCCESS;
	}

	if	(0 == strcmp(tken, HNEF_RREAD_L_ID))
	{
		if	(NULL != game->rules->id)
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_DUP;
			return		HNEF_FR_SUCCESS;
		}
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS)))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return	HNEF_FR_SUCCESS;
		}
		return	hnef_parse_strarg
/* SPLint pointer to pointer (pointer to C-string) */ /*@i1@*/\
			(tken, & game->rules->id, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_NAME))
	{
		if	(NULL != game->rules->name)
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_DUP;
			return		HNEF_FR_SUCCESS;
		}
		if	(NULL == namearg)
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}
		return	hnef_parse_strarg
/* SPLint pointer to pointer (pointer to C-string) */ /*@i1@*/\
			(namearg, & game->rules->name, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_UITHEME))
	{
		if	(NULL != game->rules->ui_theme)
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_DUP;
			return		HNEF_FR_SUCCESS;
		}
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS)))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}
		return	hnef_parse_strarg
/* SPLint pointer to pointer (pointer to C-string) */ /*@i1@*/\
			(tken, & game->rules->ui_theme, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_WIDTH))
	{
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS)))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}
		return	hnef_parse_size(game->rules, tken,
				& game->rules->bwidth, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_HEIGHT))
	{
		if (NULL == (tken = strtok(NULL, HNEF_PARSE_DELIMS)))
		{
			* fail_read	= HNEF_RREAD_FAIL_ARG_MISS;
			return		HNEF_FR_SUCCESS;
		}
		return	hnef_parse_size(game->rules, tken,
				& game->rules->bheight, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_PIECES))
	{
		return	hnef_parse_arrb(game, HNEF_FALSE, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_SQUARES))
	{
		return	hnef_parse_arrb(game, HNEF_TRUE, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_PIECE))
	{
		return	hnef_parse_type(game, HNEF_FALSE, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_SQUARE))
	{
		return	hnef_parse_type(game, HNEF_TRUE, fail_read);
	}
	else if	(0 == strcmp(tken, HNEF_RREAD_L_FREPEAT))
	{
		game->rules->forbid_repeat = HNEF_TRUE;
		return HNEF_FR_SUCCESS;
	}
/*
* NOTE:	Additional arguments go here.
*/
	else
	{
		* fail_read	= HNEF_RREAD_FAIL_ARG_UNK;
		return		HNEF_FR_SUCCESS;
	}
}

/*
 * Reads a ruleset from `file` into `game`, but does not check if the
 * rules are valid. `fail_read` is set to a failure code if the file was
 * too malformed to be parsed.
 *
 * Instead of calling this function (if you don't want to read the rules
 * from a C `FILE *`), you can read every line and call
 * `hnef_game_parseline` for each line.
 */
enum HNEF_FR
hnef_game_read_file (
	struct hnef_game	* const HNEF_RSTR game,
	FILE			* const HNEF_RSTR file,
	enum HNEF_RREAD		* const HNEF_RSTR fail_read
	)
{
	char		* buf	= NULL;
	size_t		bufmem	= 0;
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;

	assert	(NULL != game);
	assert	(NULL != file);
	assert	(NULL != fail_read);

	* fail_read	= HNEF_RREAD_SUCCESS;

/*@i1@*/\
	while	(!feof	(file)
/*@i1@*/\
	&&	!ferror	(file))
	{
		buf = hnef_line_read(file, buf, & bufmem, & fr);
		if	(!hnef_fr_good(fr))
		{
			break;
		}
		if	(NULL == buf)
		{
			continue;
		}

		fr = hnef_game_parseline(game, buf, fail_read);
		if	(!hnef_fr_good(fr)
		||	!hnef_rread_good(* fail_read))
		{
			break;
		}
	}

	if	(NULL != buf)
	{
		free(buf);
	}
	return fr;
}

/*
 * Like `hnef_game_read_file`, but resets the game before reading,
 * checks if the rules are valid, fully initializes the game (if valid)
 * or resets the game after reading (if invalid). This is a convenience
 * function and you can do all of those things manually instead.
 */
enum HNEF_FR
hnef_game_init_file (
	struct hnef_game	* const HNEF_RSTR game,
	FILE			* const HNEF_RSTR file,
	enum HNEF_RREAD		* const HNEF_RSTR fail_read,
	enum HNEF_RVALID	* const HNEF_RSTR fail_valid
	)
{
	enum HNEF_FR fr = HNEF_FR_SUCCESS;

	assert	(NULL != game);
	assert	(NULL != file);
	assert	(NULL != fail_read);
	assert	(NULL != fail_valid);

	* fail_valid	= HNEF_RVALID_SUCCESS;

	hnef_game_clear(game);

	fr	= hnef_game_read_file(game, file, fail_read);
	if	(!hnef_fr_good(fr)
	||	!hnef_rread_good(* fail_read))
	{
		hnef_game_clear(game);
		return fr;
	}

	if	(hnef_rvalid_good(* fail_valid = hnef_game_valid(game)))
	{
		fr = hnef_game_init_done(game);
		if	(!hnef_fr_good(fr))
		{
			hnef_game_clear(game);
		}
	}
	else
	{
		hnef_game_clear(game);
	}
	return fr;
}

