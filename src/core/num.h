/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_NUM_H
#define HNEF_CORE_NUM_H

#include "boolt.h"	/* HNEF_BOOL */
#include "config.h"	/* HNEF_RSTR */

/*@unused@*/
extern
int
hnef_max_int (
	const int,
	const int
	)
/*@modifies nothing@*/
;

/*@unused@*/
extern
int
hnef_min_int (
	const int,
	const int
	)
/*@modifies nothing@*/
;

extern
unsigned short
hnef_max_ushrt (
	const unsigned short,
	const unsigned short
	)
/*@modifies nothing@*/
;

extern
unsigned short
hnef_min_ushrt (
	const unsigned short,
	const unsigned short
	)
/*@modifies nothing@*/
;

extern
HNEF_BOOL
hnef_single_bit (
	const unsigned int
	)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
/*@unused@*/
extern
HNEF_BOOL
hnef_texttoint (
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR str_num,
/*@out@*/
/*@notnull@*/
	int		* const HNEF_RSTR num
	)
/*@globals errno@*/
/*@modifies errno, * num@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
HNEF_BOOL
hnef_texttoushort (
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR str_num,
/*@out@*/
/*@notnull@*/
	unsigned short	* const HNEF_RSTR num
	)
/*@globals errno@*/
/*@modifies errno, * num@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
HNEF_BOOL
hnef_texttosize (
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR str_num,
/*@out@*/
/*@notnull@*/
	size_t *	const HNEF_RSTR num
	)
/*@globals errno@*/
/*@modifies errno, * num@*/
;
/*@=protoparamname@*/

#endif

