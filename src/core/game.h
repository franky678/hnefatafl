/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_GAME_H
#define HNEF_CORE_GAME_H

#include "config.h"	/* HNEF_RSTR */
#include "funct.h"	/* HNEF_FR */
#include "gamet.h"	/* hnef_game */

/*@-protoparamname@*/
extern
void
hnef_game_reset_board (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies game->board@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
void
hnef_game_reset (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies * game@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_game_init_done (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies * game@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
void
hnef_game_clear (
/*@notnull@*/
/*@partial@*/
	struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies * game@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
HNEF_BOOL
hnef_game_rules_equal (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR g1,
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR g2
	)
/*@modifies * g1, * g2@*/
;
/*@=protoparamname@*/

/*@null@*/
/*@only@*/
/*@partial@*/
/*@unused@*/
extern
struct hnef_game *
hnef_alloc_game (void)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
/*@unused@*/
extern
void
hnef_free_game (
/*@notnull@*/
/*@owned@*/
/*@special@*/
	struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies * game, game@*/
/*@releases game@*/
;
/*@=protoparamname@*/

#endif

