/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */

#include "config.h"

const long	HNEFATAFL_CORE_VERSION	= 140816L;

/*@observer@*/
/*@unchecked@*/
static
const char	* const license_arrv[4] =
	{
		"Copyright © 2013-2014 Alexander Söderlund (Sweden),\n"
		"2013-2014 Alexander Dolgunin (Russia),\n"
		"2014 Damian Walker (United Kingdom)\n\n",

		"Permission to use, copy, modify, and distribute "
		"this software for any\n"
		"purpose with or without fee is hereby granted, "
		"provided that the above\n"
		"copyright notice and this permission notice appear in "
		"all copies.\n\n",

		"THE SOFTWARE IS PROVIDED \"AS IS\" AND THE AUTHOR "
		"DISCLAIMS ALL WARRANTIES\n"
		"WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED "
		"WARRANTIES OF\n"
		"MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE "
		"AUTHOR BE LIABLE FOR\n"
		"ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL ",

		"DAMAGES OR ANY DAMAGES\n"
		"WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR "
		"PROFITS, WHETHER IN AN\n"
		"ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS "
		"ACTION, ARISING OUT OF\n"
		"OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS "
		"SOFTWARE.\n"
	};

/*@unchecked@*/
static
const size_t	license_arrc	= (size_t)4;

const char *
hnef_core_id (void)
{
	return	"core";
}

/*
 * Returns the number of strings in the license text.
 * `hnef_core_license_arrv` is the string array.
 */
size_t
hnef_core_license_arrc (void)
{
	return	license_arrc;
}

/*
 * Returns the string array that is the license text.
 * `hnef_core_license_arrc` is the number of strings.
 */
const char * const *
hnef_core_license_arrv (void)
{
	return	license_arrv;
}

/*
 * Prints the entire license text to `out`.
 */
HNEF_BOOL
hnef_core_license_print (
	FILE	* const HNEF_RSTR out
	)
{
	size_t	i;

	assert	(NULL != out);

	for	(i = 0; i < hnef_core_license_arrc(); ++i)
	{
		if	(EOF == fputs(hnef_core_license_arrv()[i], out))
		{
			return	HNEF_FALSE;
		}
	}
	return			HNEF_TRUE;
}

