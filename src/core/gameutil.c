/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <limits.h>	/* USHRT_MAX */
#include <string.h>	/* strcmp */

#include "board.h"	/* hnef_game_safe */
#include "func.h"	/* hnef_fr_* */
#include "game.h"	/* hnef_game_reset_* */
#include "gameutil.h"
#include "line.h"	/* hnef_line_* */
#include "linet.h"	/* HNEF_PARSE_* */
#include "listmh.h"	/* hnef_listmh_copy */
#include "num.h"	/* hnef_textto* */
#include "player.h"	/* hnef_player_index_valid */
#include "playert.h"	/* hnef_player */
#include "rreadt.h"	/* HNEF_RREAD_L_* */
#include "rvalid.h"	/* hnef_game_valid */

/*
 * The game must be valid and fully initialized for all these functions.
 */

/*
 * Determines whether `bit` (which must be a single bit) or `ui_bit`
 * (which must be `HNEF_BIT_U8_EMPTY` or a single bit) should be used,
 * considering their values.
 *
 * Returns a single bit which is not `HNEF_BIT_U8_EMPTY`.
 */
static
HNEF_BIT_U8
hnef_game_uibit_type (
	const HNEF_BIT_U8	bit,
	const HNEF_BIT_U8	ui_bit
	)
/*@modifies nothing@*/
{
	assert	(hnef_single_bit((unsigned int)bit));
	assert	(HNEF_BIT_U8_EMPTY == ui_bit
	||	hnef_single_bit((unsigned int)ui_bit));

	if	(HNEF_BIT_U8_EMPTY == ui_bit)
	{
		assert	(hnef_single_bit((unsigned int)bit));
		return	bit;
	}
	else
	{
		assert	(hnef_single_bit((unsigned int)ui_bit));
		return	ui_bit;
	}
}

HNEF_BIT_U8
hnef_game_uibit_square (
	const struct hnef_type_square	* const HNEF_RSTR type
	)
{
	assert	(NULL != type);
	return	hnef_game_uibit_type(type->bit, type->ui_bit);
}

HNEF_BIT_U8
hnef_game_uibit_piece (
	const struct hnef_type_piece	* const HNEF_RSTR type
	)
{
	assert	(NULL != type);
	return	hnef_game_uibit_type(type->bit, type->ui_bit);
}

/*
 * Resets the board in `g` and then plays `replayc` moves in
 * `replayhist`. Note that `replayhist` may contain more moves than
 * `replayc` -- if so they are ignored.
 *
 * Note that `replayhist` only happens to b `listmh`. The `irrev`
 * variables in the structs don't matter.
 */
static
enum HNEF_FR
hnef_game_replay (
/*@in@*/
/*@notnull@*/
	struct hnef_game		* const g,
/*@in@*/
/*@notnull@*/
	const struct hnef_listmh	* const replayhist,
	const size_t			replayc
	)
/*@modifies * g@*/
{
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;
	size_t		i;

	assert	(NULL != g);
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(g)));
	assert	(NULL != replayhist);

	hnef_game_reset_board	(g);

	for	(i = 0; i < replayc; ++i)
	{
		const struct hnef_moveh	old	= replayhist->elems[i];
		HNEF_BOOL			legal;

		fr	= hnef_game_move(g, old.pos, old.dest, & legal);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}
/*@i1@*/\
		else if	(!legal)
		{
			return	HNEF_FR_FAIL_GAME_REPLAY;
		}
	}
	g->movehist->elemc	= replayc;

	return	HNEF_FR_SUCCESS;
}

/*
 * Writes the ruleset `id` and game history to a save file.
 *
 * The `id` is enclosed in single quotes. However, if the `id` contains
 * single quotes, then the single quotation is terminated and the
 * single quote in the ID is escaped, after which the single quotation
 * is resumed again. So, for example:
 *
 *	id'string
 *	->
 *	'id'\''string'
 *
 * The moves are just written in "pos dest" pairs, one per line. Note
 * that `irrev` is not written since it's not required for replaying the
 * game (`irrev` will be set by `board_move_unsafe()` when replayed).
 */
enum HNEF_FR
hnef_game_save_file (
	struct hnef_game	* const g,
	FILE			* const file
	)
{
	size_t i;

	assert	(NULL != g);
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(g)));
	assert	(NULL != g->rules);
	assert	(NULL != g->rules->id);
	assert	(!hnef_line_empty(g->rules->id));
	assert	(NULL != file);

	if (fprintf(file, "%s %s\n", HNEF_RREAD_L_ID, g->rules->id) < 0)
	{
		return	HNEF_FR_FAIL_IO_FILE_WRITE;
	}

	for	(i = 0; i < g->movehist->elemc; ++i)
	{
		const struct hnef_moveh	* const m =
					& g->movehist->elems[i];
		if (fprintf(file, "%hu %hu\n", m->pos, m->dest) < 0)
		{
			return	HNEF_FR_FAIL_IO_FILE_WRITE;
		}
	}

	return	HNEF_FR_SUCCESS;
}

/*
 * The failure is always informative and never fatal. The ongoing game
 * is never destroyed or reset by this function.
 */
enum HNEF_FR
hnef_game_save (
	struct hnef_game	* const g,
	const char		* const filename
	)
{
	FILE *		file	= NULL;
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;

	assert	(NULL != g);
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(g)));
	assert	(NULL != filename);

	file	= fopen(filename, "w");
	if	(NULL == file)
	{
		return	HNEF_FR_FAIL_IO_FILE_OPEN;
	}

	fr	= hnef_game_save_file(g, file);
	if	(!hnef_fr_good(fr))
	{
		(void)fclose	(file);
		return		fr;
	}

/*@i1@*/\
	if	(ferror(file))
	{
		(void)fclose	(file);
		return		fr;
	}

	if	(0 != fclose(file))
	{
		return	HNEF_FR_FAIL_IO_FILE_CLOSE;
	}
	return	HNEF_FR_SUCCESS;
}

/*
 * When you attempt to load a game, the ongoing game is always destroyed
 * regardless if the load succeeds or fails.
 *
 * The failure is always informative and never fatal. However, it may be
 * be an out-of-memory condition, which is severe.
 *
 * `id_good` is only relevant if the function didn't fail.
 */
enum HNEF_FR
hnef_game_load (
	struct hnef_game	* const g,
	const char		* const filename,
	HNEF_BOOL		* const id_good
	)
{
	FILE			* file	= NULL;
	enum HNEF_FR		fr	= HNEF_FR_SUCCESS;
	HNEF_BOOL		id_have	= HNEF_FALSE;
/*@owned@*/
	char			* buf	= NULL;
	size_t			bufmem	= 0;

	assert	(NULL != g);
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(g)));
	assert	(NULL != g->rules);
	assert	(NULL != g->rules->id);
	assert	(NULL != filename);

	* id_good = HNEF_TRUE;

	file = fopen(filename, "r");
	if	(NULL == file)
	{
		return	HNEF_FR_FAIL_IO_FILE_OPEN;
	}

	g->opt_replayhist->elemc = 0;

/*@i1@*/\
	while	(!feof(file))
	{
		buf = hnef_line_read(file, buf, & bufmem, & fr);
		if	(!hnef_fr_good(fr))
		{
			goto RETURN_FAIL;
		}

		if	(NULL == buf
		||	hnef_line_empty(buf))
		{
			continue;
		}

		if	(id_have)
		{
			char		* tken	= NULL;
			unsigned short	pos	= USHRT_MAX,
					dest	= USHRT_MAX;

			if	(NULL == (tken = strtok(buf,
						HNEF_PARSE_DELIMS))
			||	!hnef_texttoushort(tken, & pos)
			||	NULL == (tken = strtok(NULL,
						HNEF_PARSE_DELIMS))
			||	!hnef_texttoushort(tken, & dest))
			{
				fr	= HNEF_FR_FAIL_GAME_SAVE_MALF;
				goto	RETURN_FAIL;
			}

			/*
			 * The `irrev` parameter doesn't matter. `pos`
			 * and `dest` will be validated when we try to
			 * make moves, so we don't have to do it here.
			 */
			fr	= hnef_listmh_add(g->opt_replayhist,
						pos, dest, HNEF_FALSE);
			if	(!hnef_fr_good(fr))
			{
				goto RETURN_FAIL;
			}
		}
		else
		{
			char * tken = NULL;
			if	(NULL == (tken = strtok(buf,
						HNEF_PARSE_DELIMS))
			||	0 != strcmp(tken, HNEF_RREAD_L_ID))
			{
				fr	= HNEF_FR_FAIL_GAME_SAVE_MALF;
				goto	RETURN_FAIL;
			}

			if	(NULL == (tken = strtok(NULL,
						HNEF_PARSE_DELIMS)))
			{
				fr	= HNEF_FR_FAIL_GAME_SAVE_MALF;
				goto	RETURN_FAIL;
			}

			id_have = HNEF_TRUE;
			* id_good = 0 == strcmp(g->rules->id, tken);
		}
	}

/*@i1@*/\
	if	(ferror(file))
	{
		if	(NULL != buf)
		{
			free(buf);
			buf = NULL;
		}
		(void)fclose(file);
		g->opt_replayhist->elemc = 0;
		return	HNEF_FR_FAIL_IO_FILE_READ;
	}
	if	(0 != fclose(file))
	{
		if	(NULL != buf)
		{
			free(buf);
			buf = NULL;
		}
		g->opt_replayhist->elemc = 0;
		return	HNEF_FR_FAIL_IO_FILE_CLOSE;
	}

	if	(!id_have)
	{
		if	(NULL != buf)
		{
			free(buf);
			buf = NULL;
		}
		return	HNEF_FR_FAIL_GAME_SAVE_MALF;
	}

	fr	= hnef_game_replay(g, g->opt_replayhist,
				g->opt_replayhist->elemc);
	g->opt_replayhist->elemc	= 0;
	if	(!hnef_fr_good(fr))
	{
		if	(NULL != buf)
		{
			free(buf);
			buf = NULL;
		}
		hnef_game_reset	(g);
		return fr;
	}

	if	(NULL != buf)
	{
		free(buf);
		buf = NULL;
	}
	return HNEF_FR_SUCCESS;

	RETURN_FAIL:
	if	(NULL != buf)
	{
		free(buf);
		buf = NULL;
	}
	hnef_game_reset			(g);
	g->opt_replayhist->elemc	= 0;
	(void)fclose			(file);
	return fr;
}

/*
 * Undoes `undoc` moves.
 *
 * If the undo fails because the `undoc` parameter is invalid, then
 * `success` is set to false, but `HNEF_FR_SUCCESS` is returned
 * (indicating a non-fatal failure).
 *
 * If the `undoc` parameter is valid, then the program should always
 * successfully undo. If not, it will return a `HNEF_FR_` failure value.
 * If that happens (which should not be possible all the moves that have
 * to be replayed in this function were played before), the game is
 * reset (id est started over from the beginning).
 */
enum HNEF_FR
hnef_game_undo (
	struct hnef_game	* const g,
	HNEF_BOOL		* const success,
	const size_t		undoc
	)
{
	enum HNEF_FR	fr		= HNEF_FR_SUCCESS;
	size_t		elemc_new;

	assert	(NULL != g);
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(g)));
	assert	(NULL != success);

	* success	= HNEF_FALSE;

	if	(undoc < (size_t)1
	||	g->movehist->elemc < (size_t)1
	||	undoc > g->movehist->elemc)
	{
		return	HNEF_FR_SUCCESS;
	}

	elemc_new	= g->movehist->elemc - undoc;

	/*
	 * Copy the move history in the replay history variable, and
	 * then replay using it (we can't replay using `g->movehist`
	 * since moves are added to `g->movehist` while making moves on
	 * the board).
	 */
	fr	= hnef_listmh_copy(g->movehist, g->opt_replayhist);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}
	assert	(g->movehist->elemc == g->opt_replayhist->elemc);

	fr	= hnef_game_replay(g, g->opt_replayhist, elemc_new);
	g->opt_replayhist->elemc	= 0;
	if	(!hnef_fr_good(fr))
	{
		hnef_game_reset	(g);
		return		fr;
	}

	* success	= HNEF_TRUE;
	return		HNEF_FR_SUCCESS;
}

