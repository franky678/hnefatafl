/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include "rreadt.h"

const char	* HNEF_RREAD_L_BIT		= "bit",
		* HNEF_RREAD_L_CAPTURES		= "captures",
		* HNEF_RREAD_L_CAPT_EDGE	= "capt_edge",
		* HNEF_RREAD_L_CAPT_LOSS	= "capt_loss",
		* HNEF_RREAD_L_CAPT_SIDES	= "capt_sides",
		* HNEF_RREAD_L_CAPT_SIDES_PIECES = "capt_sides_pieces",
		* HNEF_RREAD_L_CUSTODIAL	= "custodial",
		* HNEF_RREAD_L_DBL_TRAP		= "dbl_trap",
		* HNEF_RREAD_L_DBL_TRAP_CAPT	= "dbl_trap_capt",
		* HNEF_RREAD_L_DBL_TRAP_COMPL	= "dbl_trap_compl",
		* HNEF_RREAD_L_DBL_TRAP_EDGE	= "dbl_trap_edge",
		* HNEF_RREAD_L_DBL_TRAP_ENCL	= "dbl_trap_encl",
		* HNEF_RREAD_L_DBL_TRAP_SQ	= "dbl_trap_squares",
		* HNEF_RREAD_L_ESCAPE		= "escape",
		* HNEF_RREAD_L_FREPEAT		= "forbid_repeat",
		* HNEF_RREAD_L_HEIGHT		= "height",
		* HNEF_RREAD_L_ID		= "id",
		* HNEF_RREAD_L_NAME		= "name",
		* HNEF_RREAD_L_NORETURN		= "noreturn",
		* HNEF_RREAD_L_OCCUPIES		= "occupies",
		* HNEF_RREAD_L_OWNER		= "owner",
		* HNEF_RREAD_L_UITHEME		= "ui_theme",
		* HNEF_RREAD_L_PIECE		= "piece",
		* HNEF_RREAD_L_PIECES		= "pieces",
		* HNEF_RREAD_L_SQUARE		= "square",
		* HNEF_RREAD_L_SQUARES		= "squares",
		* HNEF_RREAD_L_UI_BIT		= "ui_bit",
		* HNEF_RREAD_L_WIDTH		= "width";

