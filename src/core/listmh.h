/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_LISTMH_H
#define HNEF_CORE_LISTMH_H

#include "config.h"	/* HNEF_RSTR */
#include "funct.h"	/* HNEF_FR */
#include "listmht.h"	/* hnef_listmh */

/*@null@*/
/*@only@*/
/*@partial@*/
extern
struct hnef_listmh *
hnef_alloc_listmh (
	const size_t
	)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
extern
void
hnef_free_listmh (
/*@notnull@*/
/*@owned@*/
/*@special@*/
	struct hnef_listmh	* const HNEF_RSTR list
	)
/*@modifies list->elems, list@*/
/*@releases list->elems, list@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_listmh_add (
/*@notnull@*/
/*@partial@*/
	struct hnef_listmh	* const list,
	const unsigned short	pos,
	const unsigned short	dest,
	const HNEF_BOOL		irrev
	)
/*@modifies * list@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_listmh_copy (
/*@in@*/
/*@notnull@*/
	const struct hnef_listmh	* const HNEF_RSTR src,
/*@in@*/
/*@notnull@*/
	struct hnef_listmh		* const HNEF_RSTR dest
	)
/*@modifies * dest@*/
;
/*@=protoparamname@*/

#endif

