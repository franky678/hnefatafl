/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef HNEFATAFL_ZHASH

#include <assert.h>	/* assert */
#include <errno.h>	/* errno */
#include <math.h>	/* sqrt */

#include "zhash.h"
#include "types.h"	/* hnef_type_index_get */

/*@unchecked@*/
static
const size_t	HNEF_ZHASH_MEM_COL_DEF	= (size_t)48000000,
		HNEF_ZHASH_MEM_TAB_DEF	= (size_t)32000000;

HNEF_BOOL
hnef_zhash_mem_tab_valid (
	const size_t	mem_tab
	)
{
	return	mem_tab >= HNEF_ZHASH_MEM_TAB_MIN;
}

HNEF_BOOL
hnef_zhash_mem_col_valid (
	const size_t	mem_col
	)
{
	return	mem_col >= HNEF_ZHASH_MEM_COL_MIN;
}

size_t
hnef_zhash_mem_tab_def (void)
{
	assert	(hnef_zhash_mem_tab_valid(HNEF_ZHASH_MEM_TAB_DEF));
	return	HNEF_ZHASH_MEM_TAB_DEF;
}

size_t
hnef_zhash_mem_col_def (void)
{
	assert	(hnef_zhash_mem_tab_valid(HNEF_ZHASH_MEM_COL_DEF));
	return	HNEF_ZHASH_MEM_COL_DEF;
}

static
unsigned int
hnef_zhash (
/*@in@*/
/*@notnull@*/
	const struct hnef_zhashtable	* const HNEF_RSTR ht,
/*@in@*/
/*@notnull@*/
	unsigned int			* * const HNEF_RSTR zob,
/*@in@*/
/*@notnull@*/
	const struct hnef_board		* const HNEF_RSTR b
	)
/*@modifies nothing@*/
{
	unsigned int	key	= 0;
	unsigned short	i,
			j;

	assert	(NULL != ht);
	assert	(NULL != zob);
	assert	(NULL != b);

	for	(i = (unsigned short)0; i < ht->z_i; ++i)
	{
		for	(j = (unsigned short)0; j < ht->z_j; ++j)
		{
			if	(HNEF_BIT_U8_EMPTY != b->pieces[j])
			{
				key ^= zob[hnef_type_index_get
					(b->pieces[j])][j];
			}
		}
	}
	return	key;
}

unsigned int
hnef_zhashkey (
	const struct hnef_zhashtable	* const HNEF_RSTR ht,
	const struct hnef_board		* const HNEF_RSTR b
	)
{
	assert	(NULL != ht);
	assert	(NULL != b);
	return	hnef_zhash(ht, ht->zobrist_key, b);
}

unsigned int
hnef_zhashlock (
	const struct hnef_zhashtable	* const HNEF_RSTR ht,
	const struct hnef_board		* const HNEF_RSTR b
	)
{
	assert	(NULL != ht);
	assert	(NULL != b);
	return	hnef_zhash(ht, ht->zobrist_lock, b);
}

static
size_t
hnef_zhashindex (
/*@in@*/
/*@notnull@*/
	const struct hnef_zhashtable	* const HNEF_RSTR ht,
	unsigned int			key
	)
/*@modifies nothing@*/
{
	assert	(NULL != ht);
	/*
	 * `key & (ht->arrc - 1)` gives more collisions.
	 */
	return	(size_t)key % (size_t)ht->arrc;
}

/*
 * node->used is set to false.
 */
static
void
hnef_zhashnode_set (
/*@in@*/
/*@notnull@*/
	struct hnef_zhashnode		* const HNEF_RSTR node,
	const unsigned int		hashkey,
	const unsigned int		hashlock,
	const int			value,
	const enum HNEF_ZVALUE_TYPE	value_type,
	unsigned short			p_turn,
	unsigned short			depthleft
	)
/*@modifies * node@*/
{
	assert	(NULL != node);

	node->hashkey		= hashkey;
	node->hashlock		= hashlock;
	node->value		= value;
	node->value_type	= value_type;
	node->p_turn		= p_turn;
	node->depthleft		= depthleft;
	node->used		= HNEF_FALSE;
}

/*
 * Checks if node may be overwritten with the given new values.
 *
 * Positive:	node may be overwritten by new values.
 * 0:		node may not be overwritten.
 * Negative:	new values can be discarded.
 */
static
int
hnef_zhashnode_action (
/*@in@*/
/*@notnull@*/
	const struct hnef_zhashnode	* const HNEF_RSTR node,
/*@in@*/
/*@notnull@*/
	const struct hnef_board		* const HNEF_RSTR b,
	const unsigned int		key,
	const unsigned int		lock,
	const enum HNEF_ZVALUE_TYPE	value_type,
	unsigned short			depthleft
	)
/*@modifies nothing@*/
{
	assert	(NULL != node);
	assert	(NULL != b);

	if	(HNEF_AIM_ZHNEF_VACANT == node->value_type)
	{
		return	1;
	}
	else
	{
		if	(key == node->hashkey && lock == node->hashlock
		&&	b->turn == node->p_turn)
		{
			/*
			 * Same board. Candidate for overwriting.
			 */
			if (HNEF_AIM_ZHNEF_EXACT == value_type
			&& (HNEF_AIM_ZHNEF_ALPHA == node->value_type
			|| HNEF_AIM_ZHNEF_BETA == node->value_type))
			{
				/*
				 * New value is more exact.
				 */
				return	1;
			}
			else if	(value_type == node->value_type)
			{
				/*
				 * Same value type.
				 */
				return	depthleft > node->depthleft
					? 1
					: -1;
			}
			else
			{
				/*
				 * Different value type -- never allowed
				 * to overwrite.
				 */
				return	0;
			}
		}
		else
		{
			/*
			 * Not the same board -- may never overwrite.
			 */
			return	0;
		}
	}
}

/*
 * Sets b as a new hash in ht with value, value_type and depthleft.
 *
 * This may overwrite an existing node, if the value of the new one is
 * more exactly known and they are the same board position.
 *
 * NOTE:	If there is no more space in zhashcols->memchunk, then
 *		the node is not added, with no indication of failure.
 *
 * NOTE:	If value_type is HNEF_AIM_ZHNEF_VACANT, then the node is
 *		not added, with no indication of failure.
 */
void
hnef_zhashtable_put (
	struct hnef_zhashtable		* const HNEF_RSTR ht,
	const struct hnef_board		* const HNEF_RSTR b,
	const int			value,
	const enum HNEF_ZVALUE_TYPE	value_type,
	unsigned short			depthleft
	)
{
	unsigned int	key,
			lock;
	size_t		index;
	struct hnef_zhashnode	* HNEF_RSTR node = NULL;

	assert	(NULL != ht);
	assert	(NULL != b);
	assert	(HNEF_AIM_ZHNEF_VACANT != value_type);

	key	= hnef_zhashkey		(ht, b),
	lock	= hnef_zhashlock	(ht, b);
	index	= hnef_zhashindex	(ht, key);
	node	= & ht->arr[index];

	for	(;;)
	{
		const int act	= hnef_zhashnode_action(node, b, key,
				lock, value_type, depthleft);
		if	(act > 0)
		{
			/*
			 * Overwrite existing node with new values.
			 */
			hnef_zhashnode_set(node, key, lock, value,
				value_type, b->turn, depthleft);
			return;
		}
/*@i1@*/\
		else if	(act < 0)
		{
			/*
			 * Discard new values.
			 */
			return;
		}
		/*
		 * Node may not be overwritten, nor discarded. Check
		 * next node.
		 */

		if	(NULL != node->next)
		{
			node	= node->next;
		}
		else
		{
			break;
		}
	}

	/*
	 * No existing node could be overwritten, so see if there's room
	 * in memchunk for another one.
	 */
	if	(ht->cols->elemc < ht->cols->capc)
	{
		node->next = & ht->cols->memchunk[ht->cols->elemc++];
		hnef_zhashnode_set(node->next, key, lock, value,
			value_type, b->turn, depthleft);
		/*
		 * NOTE:	We don't need to set node->next->next
		 *		to NULL since it should have been done
		 *		when clearing old nodes.
		 */
	}
	/*
	 * NOTE:	else: memchunk (collision list space) is
	 *		depleted, so we can't add any more nodes.
	 *		Increasing g->ai_minimax_hash_mem_col would
	 *		help if this happens.
	 */
}

struct hnef_zhashnode *
hnef_zhashtable_get (
	const struct hnef_zhashtable	* const HNEF_RSTR ht,
	const unsigned int		key,
	const unsigned int		lock,
	const unsigned short		p_turn
	)
{
	struct hnef_zhashnode	* HNEF_RSTR node = NULL;

	assert	(NULL != ht);

	node	= & ht->arr[hnef_zhashindex(ht, key)];

	if	(HNEF_AIM_ZHNEF_VACANT == node->value_type)
	{
		return	NULL;
	}
	do
	{
		if	(key == node->hashkey
		&&	lock == node->hashlock
		&&	p_turn == node->p_turn)
		{
			break;
		}
	}
	while	(NULL != (node = node->next));
	return	node;
}

static
void
hnef_zhashnode_copy (
/*@in@*/
/*@notnull@*/
	const struct hnef_zhashnode	* const HNEF_RSTR src,
/*@in@*/
/*@notnull@*/
	struct hnef_zhashnode		* const HNEF_RSTR dest
	)
/*@modifies * dest@*/
{
	assert	(NULL != src);
	assert	(NULL != dest);
	/*
	 * Note that src->next points to a zhashnode that's either in
	 * zhashtable->arr, or zhashtable->cols->memchunk. Even though
	 * the pointer is lost, it doesn't matter because it will all be
	 * freed.
	 */
	dest->next		= src->next;
	dest->value_type	= src->value_type;
	dest->used		= src->used;
	dest->hashkey		= src->hashkey;
	dest->hashlock		= src->hashlock;
	dest->value		= src->value;
	dest->p_turn		= src->p_turn;
	dest->depthleft		= src->depthleft;
}

/*
 * zhashtable_clearunused uses the following method to remove unused
 * nodes:
 *
 * 1.	Swap the node to be removed (R) with the last used node in
 *	memchunk (L). This is done by simply copying L -> R, and setting
 *	R->next to NULL.
 *
 * 2.	When the nodes have been "swapped", the problem is that L is
 *	still pointed to by some zhashnode->next, since the node is in
 *	use. This is always the case, unless L == R (which happens
 *	rarely). If it is the case, then we have to find the node which
 *	points to L, and make it point to R instead, since L is replaced
 *	by R.
 *
 *	If it's not the case, then we can simply remove L and R since
 *	they are the same (and they are both the last node in memchunk),
 *	and we don't have to update stray nodes.
 *
 * 3.	The node which points to L is in the collision lists in bucket
 *	zhashindex(ht, L->hashkey), since only colliding nodes point to
 *	each other by zhashnode->next, and the node we're looking for
 *	points to L. Thus they are in the same bucket and that bucket's
 *	index is the zhashindex of L->hashkey. So all we have to do is
 *	go through that collision list and when we find the pointer to
 *	L (zstray_old), then make it point to R (zstray_new) instead.
 *
 * This gives O(1) removal of unused nodes, without having to make the
 * collision lists doubly linked, and without having to sacrifice O(1)
 * insertion.
 */
static
void
hnef_zhashnode_update_stray (
/*@in@*/
/*@notnull@*/
	struct hnef_zhashtable		* const HNEF_RSTR ht,
/*@in@*/
/*@notnull@*/
	const struct hnef_zhashnode	* const HNEF_RSTR zstray_old,
/*@dependent@*/
/*@in@*/
/*@notnull@*/
	struct hnef_zhashnode		* const HNEF_RSTR zstray_new
	)
/*@modifies * ht@*/
{
	size_t			zstray_ind;
	struct hnef_zhashnode	* HNEF_RSTR zstray_list = NULL;

	assert	(NULL != ht);
	assert	(NULL != zstray_old);
	assert	(NULL != zstray_new);
	assert	(zstray_old != zstray_new);

	zstray_ind	= hnef_zhashindex(ht, zstray_old->hashkey);
	zstray_list	= & ht->arr[zstray_ind];

	while	(NULL != zstray_list)
	{
		if	(zstray_list->next == zstray_old)
		{
			zstray_list->next = zstray_new;
			return;
		}
		zstray_list = zstray_list->next;
	}
	/*
	 * Not found!
	 */
	assert	(HNEF_FALSE);
}

/*
 * Removes noden (node->next) from node's collision list, and sets
 * node->next to noden->next (node->next->next).
 *
 * This is done by swapping noden with the last node from memchunk,
 * removing the last node from memchunk, and updating the stray pointer,
 * if any.
 *
 * *	noden should be node->next.
 *
 * *	Therefore noden should be in node's collision list, following
 *	node.
 *
 * This shrinks memchunk.
 */
static
void
hnef_zhashnode_remove_next (
/*@in@*/
/*@notnull@*/
	struct hnef_zhashtable	* const ht,
/*@in@*/
/*@notnull@*/
	struct hnef_zhashnode	* const HNEF_RSTR node,
/*@dependent@*/
/*@in@*/
/*@notnull@*/
	struct hnef_zhashnode	* const noden
	)
/*@modifies * ht, * node, * noden@*/
{
	struct hnef_zhashnode	* last	= NULL;

	assert	(NULL != ht);
	assert	(NULL != node);
	assert	(NULL != noden);
	assert	(ht->cols->elemc > 0);

	node->next	= noden->next;
	last		= & ht->cols->memchunk[--ht->cols->elemc];

	/*
	 * "Swap" last and noden, by copying last into noden and then
	 * reducing elemc.
	 */
	if	(last != noden)
	{
		hnef_zhashnode_copy		(last, noden);
		last->next			= NULL;
		hnef_zhashnode_update_stray	(ht, last, noden);
	}
	else
	{
		/*
		 * noden and last are the same node. Just remove its
		 * reference (which we saved into node->next) and it can
		 * be considered deleted.
		 */
		last->next	= NULL;
	}
}

/*
 * Clears all unused nodes in ht. Then sets all used nodes to unused, so
 * that they will be cleared the next time this function is called.
 *
 * For details on exactly how this is done, see the descriptions of
 * zhashnode_remove_next and zhashnode_update_stray.
 */
void
hnef_zhashtable_clearunused (
	struct hnef_zhashtable	* const ht
	)
{
	size_t i;

	assert	(NULL != ht);

	for	(i = (size_t)0; i < ht->arrc; ++i)
	{
		struct hnef_zhashnode	*	node	= & ht->arr[i];
		struct hnef_zhashnode	*	noden	= node->next;
		if	(HNEF_AIM_ZHNEF_VACANT == node->value_type)
		{
			continue;
		}

		/*
		 * First check all nodes that follow the first (id est
		 * all collisions).
		 */
		while	(NULL != noden)
		{
			if	(noden->used)
			{
				noden->used	= HNEF_FALSE;
				node		= noden;
				noden		= node->next;
			}
			else
			{
				assert	(NULL != noden);
				hnef_zhashnode_remove_next
					(ht, node, noden);
				noden	= node->next;
			}
		}

		/*
		 * All collisions have been checked. Check first node.
		 */
		node	= & ht->arr[i];
		if	(node->used)
		{
			node->used	= HNEF_FALSE;
		}
		else
		{
			noden	= node->next;
			if	(NULL == noden)
			{
				/*
				 * There are no collisions: just set
				 * this one to unused and we're done.
				 */
				node->value_type =
					HNEF_AIM_ZHNEF_VACANT;
			}
			else
			{
				/*
				 * "Remove" this node by replacing it
				 * with its next node, and then removing
				 * the next node.
				 */
				hnef_zhashnode_copy	(noden, node);
				hnef_zhashnode_remove_next(ht, node,
							noden);
			}
		}
	}
}

/*
 * This is used for initializing zhashtable->zobrist_key and
 * zobrist_lock.
 *
 * z_i and z_j are the desired lengths of the arrays.
 */
/*@null@*/
/*@only@*/
static
unsigned int * *
hnef_alloc_zobrist_init (
	const unsigned short	z_i,
	const unsigned short	z_j
	)
/*@globals internalState@*/
/*@modifies internalState@*/
{
	/*
	 * zobrist and zobrist_lock may be NULL.
	 */
	unsigned short	i;
	unsigned int	* * zobrist = NULL;

	assert	(z_i > 0);
	assert	(z_j > 0);

	zobrist	= malloc(sizeof(* zobrist) * z_i);

	if	(NULL == zobrist)
	{
		return	NULL;
	}
	for	(i = (unsigned short)0; i < z_i; ++i)
	{
		unsigned short	j;

		zobrist[i]	= malloc(sizeof(* zobrist[i]) * z_j);
		if	(NULL == zobrist[i])
		{
			unsigned short	k;
			for	(k = (unsigned short)0; k < i; ++k)
			{
				free	(zobrist[k]);
			}
			free		(zobrist);
			return		NULL;
		}

		for	(j = (unsigned short)0; j < z_j; ++j)
		{
			zobrist[i][j] = (unsigned int)rand();
			zobrist[i][j] ^= (unsigned int)rand() << 15;
			zobrist[i][j] ^= (unsigned int)rand() << 30;
		}
	}
	return	zobrist;
}

/*
 * Allocates a zhashcols struct with capc g->ai_minimax_hash_mem_col.
 */
/*@null@*/
/*@only@*/
static
struct hnef_zhashcols *
hnef_alloc_zhashcols_init (
	const size_t	mem_col
	)
/*@modifies nothing@*/
{
	struct hnef_zhashcols	* cols	= malloc(sizeof(* cols));
	if	(NULL == cols)
	{
		return	NULL;
	}

	assert	(hnef_zhash_mem_col_valid(mem_col));

	cols->elemc	= (size_t)0;
	cols->capc	= (size_t)(mem_col
/* splint safer to use pointer in sizeof */ /*@i1@*/\
			/ sizeof(struct hnef_zhashnode));
	if	(cols->capc < (size_t)1)
	{
		free	(cols);
		return	NULL;
	}

	/*
	 * NOTE:	We depend on calloc to set zhashnode->next to
	 *		NULL by 0-initializing.
	 */
	cols->memchunk	= calloc(cols->capc, sizeof(* cols->memchunk));
	if	(NULL == cols->memchunk)
	{
		free	(cols);
		return	NULL;
	}

	return	cols;
}

/*
 * Checks if num is a prime.
 *
 * Returns false if num is < 2 (or can't be square-rooted due to being
 * negative).
 *
 * NOTE:	May be more approriate in num.c, but is here since it's
 *		only used for one thing.
 */
static
HNEF_BOOL
hnef_is_prime (
	const size_t	num
	)
/*@globals errno@*/
/*@modifies errno@*/
{
	size_t	divid;
	if	(num < (size_t)2)
	{
		return	HNEF_FALSE;
	}
	{
		int		errno_old	= errno;
		double		tmp;
				errno		= 0;
				tmp		= sqrt((double)num);
		if	(0 != errno)
		{
			/*
			 * Only happens if num is negative, which it
			 * can't be, being a size_t.
			 */
			return	HNEF_FALSE;
		}
		errno		= errno_old;
		divid		= (size_t)(tmp + 0.0001);
	}
	do
	{
		if	(0 == num % divid)
		{
			return	HNEF_FALSE;
		}
	}
	while	(--divid > (size_t)1);
	return	HNEF_TRUE;
}

/*
 * Finds next prime number >= num.
 *
 * NOTE:	May be more approriate in num.c, but is here since it's
 *		only used for one thing.
 */
static
size_t
hnef_next_prime_ge (
	size_t	num
	)
/*@globals errno@*/
/*@modifies errno@*/
{
	while	(!hnef_is_prime(num))
	{
		num++;
	}
	return	num;
}

/*
 * Finds next prime number < num.
 *
 * NOTE:	May be more approriate in num.c, but is here since it's
 *		only used for one thing.
 */
static
size_t
hnef_next_prime_lt (
	size_t	num
	)
/*@globals errno@*/
/*@modifies errno@*/
{
	while	(!hnef_is_prime(--num))
	{
	}
	return	num;
}

/*
 * 0 size is not allowed (returns NULL).
 *
 * All variables in ht->arr are 0-initialized by calloc.
 *
 * zhashtable->arr takes at most g->ai_minimax_hash_mem_tab bytes
 * (plus/minus however much it takes to get zhashtable->arrc to the
 * nearest prime).
 *
 * zhashtable->cols takes at most g->ai_minimax_hash_mem_col bytes.
 *
 * Allocating a hash table requires the game to be valid and fully
 * initialized. A hash table is tied to a ruleset, so you have to
 * de-allocate any old hash tables when you change the ruleset.
 */
struct hnef_zhashtable *
hnef_alloc_zhashtable (
	const struct hnef_game	* const HNEF_RSTR g,
	const size_t		mem_tab,
	const size_t		mem_col
	)
{
	struct hnef_zhashtable	* ht	= NULL;

	assert	(NULL != g);
	assert	(hnef_zhash_mem_tab_valid(mem_tab));
	assert	(hnef_zhash_mem_col_valid(mem_col));

	ht	= malloc(sizeof(* ht));
	if	(NULL == ht)
	{
		return	NULL;
	}

	ht->mem_tab	= mem_tab;
	ht->mem_col	= mem_col;

	ht->arrc	= (size_t)(ht->mem_tab
/* splint safer to use pointer in sizeof */ /*@i1@*/\
				/ sizeof(struct hnef_zhashnode));

	if	(ht->arrc < (size_t)1)
	{
		free	(ht);
		return	NULL;
	}

	{
		size_t	p_ge	= hnef_next_prime_ge(ht->arrc);
		size_t	p_lt	= hnef_next_prime_lt(ht->arrc);
		size_t	d_p_ge	= p_ge		- ht->arrc;
		size_t	d_p_lt	= ht->arrc	- p_lt;
		ht->arrc	= d_p_ge < d_p_lt ? p_ge : p_lt;
	}

	/*
	 * NOTE:	We depend on calloc to set zhashnode->next to
	 *		NULL and zhashnode->value_type to
	 *		HNEF_AIM_ZHNEF_VACANT by 0-initializing.
	 */
	ht->arr		= calloc(ht->arrc, sizeof(* ht->arr));
	if	(NULL == ht->arr)
	{
		free	(ht);
		return	NULL;
	}

	/*
	 * Set to NULL for free_zhashtable and alloc_zobrist_init.
	 */
	ht->zobrist_key	= ht->zobrist_lock	= NULL;

	ht->cols	= hnef_alloc_zhashcols_init(ht->mem_col);
	if	(NULL == ht->cols)
	{
		hnef_free_zhashtable	(ht);
		return			NULL;
	}

	/*
	 * `opt_blen` requires `hnef_game_valid()` and initialization to
	 * be done.
	 */
	ht->z_i	= g->rules->type_piecec;
	ht->z_j	= g->rules->opt_blen;
	assert	(ht->z_i > 0);
	assert	(ht->z_j > 0);

	ht->zobrist_key = hnef_alloc_zobrist_init(ht->z_i, ht->z_j);
	if	(NULL == ht->zobrist_key)
	{
		hnef_free_zhashtable	(ht);
		return			NULL;
	}

	ht->zobrist_lock = hnef_alloc_zobrist_init(ht->z_i, ht->z_j);
	if	(NULL == ht->zobrist_lock)
	{
		hnef_free_zhashtable	(ht);
		return			NULL;
	}

	return	ht;
}

/*
 * Frees ht and all collision lists.
 */
void
hnef_free_zhashtable (
	struct hnef_zhashtable	* const ht
	)
{
	assert	(NULL != ht);

	if	(NULL != ht->cols)
	{
		free	(ht->cols->memchunk);
		free	(ht->cols);
	}

	if	(NULL != ht->zobrist_key)
	{
		unsigned short	i;
		for	(i = (unsigned short)0; i < ht->z_i; ++i)
		{
/* splint doesn't understand pointer to pointer */ /*@i2@*/\
			free	(ht->zobrist_key[i]);
		}
		free		(ht->zobrist_key);
	}

	if	(NULL != ht->zobrist_lock)
	{
		unsigned short	i;
		for	(i = (unsigned short)0; i < ht->z_i; ++i)
		{
/* splint doesn't understand pointer to pointer */ /*@i2@*/\
			free	(ht->zobrist_lock[i]);
		}
		free		(ht->zobrist_lock);
	}

	if	(NULL != ht->arr)
	{
		free	(ht->arr);
	}
	free	(ht);
}

#endif

