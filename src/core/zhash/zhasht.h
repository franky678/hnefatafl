/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef HNEFATAFL_ZHASH

#ifndef HNEF_CORE_AIMHASH_T_H
#define HNEF_CORE_AIMHASH_T_H

#include <stdlib.h>	/* size_t */

#include "boolt.h"	/* HNEF_BOOL */

/*@unchecked@*/
extern
const size_t	HNEF_ZHASH_MEM_COL_MIN,
		HNEF_ZHASH_MEM_TAB_MIN;

enum HNEF_ZVALUE_TYPE
{

	/*
	 * The node is not used -- it's "empty" and can be overwritten.
	 */
	HNEF_AIM_ZHNEF_VACANT	= 0,

	/*
	 * Evaluated by being a leaf-node, either by aiminimax_evaluate
	 * or by the game being over.
	 */
	HNEF_AIM_ZHNEF_EXACT,

	/*
	 * Alpha cutoff: the square is at most equal to value.
	 */
	HNEF_AIM_ZHNEF_ALPHA,

	/*
	 * Beta cutoff: the square is at least equal to value.
	 */
	HNEF_AIM_ZHNEF_BETA

};

/*
 * The uninitialized values of every variable in this struct is 0 or
 * NULL (by calloc).
 *
 * NOTE:	The elements are arranged to minimize padding, assuming
 *		the sizes of the types are:
 *
 *			pointer > enum / HNEF_BOOL > int > short
 *
 *		Note that pointer >= int, and (probably) enum == int.
 *		If pointer == enum == int == 4 byte and short == 2 byte,
 *		then the total size is:
 *		4 + 4 * 2 + 4 * 3 + 2 * 2 = 28 byte.
 *
 *		That gives, for the following sizes of zhashtable->arr:
 *
 *		8 MB:	  285 714.29	(Very high load in Tablut.)
 *		16 MB:	  571 428.57
 *		32 MB:	1 142 857.1	(Usually < 0.7 load in Tablut.)
 *		64 MB:	2 285 714.3
 */
/*@exposed@*/
struct hnef_zhashnode
{

	/*
	 * Singly linked list for collissions. (We never need to
	 * traverse this list backwards.)
	 */
/*@dependent@*/
/*@null@*/
	struct hnef_zhashnode	* next;

	/*
	 * Exactness of value.
	 */
	enum HNEF_ZVALUE_TYPE	value_type;

	/*
	 * Whether this positions was used during the calculation of
	 * theprevious move.
	 *
	 * Upon being used (to retrieve a value), a position is set to
	 * true.
	 *
	 * On every new move, all hashnodes that are unused are removed,
	 * and all nodes that are used are flagged as unused. This
	 * ensures that only relevant nodes will be kept.
	 *
	 * The initial (uninitialized) value is HNEF_FALSE.
	 */
	HNEF_BOOL		used;

	/*
	 * hashkey is the Zobrist hash (zobrist_key).
	 *
	 * hashlock is the second Zobrist hash (zobrist_lock).
	 */
	unsigned int		hashkey,
				hashlock;

	/*
	 * Value of this position.
	 */
	int			value;

	/*
	 * p_turn is the player turn for the given board.
	 *
	 * depthleft is the depth left to search.
	 */
	unsigned short		p_turn,
				depthleft;

};

/*
 * zhashtable->arr is the hash table array. This takes (up to, due to
 * rounding) at least HNEF_ZHASH_MEM_TAB_MIN bytes.
 *
 * zhashtable->cols->memchunk is space for the collision lists. This
 * takes at least HNEF_ZHASH_MEM_COL_MIN bytes.
 *
 * The table can not grow (arrc can not increase post-allocation).
 * Therefore it's useless to keep track of load factor.
 *
 * Both zhashtable->arr and zhashcols->memchunk store zhashnodes in a
 * continuous block of memory. That means that all zhashnodes will be
 * aligned to structure padding. A zhashnode with 32 bit pointers is 28
 * bytes (making some assumptions about data types), and with 64 bit
 * pointers 32 bytes. Thus no space is wasted when putting zhashnodes in
 * arrays.
 *
 * By contrast, if we had just naively used malloc every time we need to
 * add a new collision, we would allocate 32 bytes per zhashnode, since
 * malloc aligns to 16 bytes, thus wasting 4 bytes (16 * 2 - 28) per
 * zhashnode, in addition to causing heap fragmentation (though it
 * should be insignificant unless free immediately returns memory to the
 * operating system, which it doesn't on Linux at least).
 *
 * The benefit of collision lists is fast retrieval and deletion, but
 * zhashtable_clearunused achieves nearly equally fast deletion, and the
 * use of a single non-growable array for colliding nodes means no heap
 * fragmentation since they don't have to be allocated all the time.
 * Insertion is as fast.
 */
/*@exposed@*/
struct hnef_zhashtable
{

	/*
	 * The table. An array of hashnode pointers. Length = arrc.
	 *
	 * Unused ("empty") nodes have value_type HNEF_AIM_ZHNEF_VACANT
	 * and can be overwritten when something needs to be added.
	 */
/*@notnull@*/
/*@owned@*/
	struct hnef_zhashnode	* arr;

	/*
	 * Capacity of arr.
	 */
	size_t			arrc;

	/*
	 * Memory for collisions lists.
	 */
	struct hnef_zhashcols	* cols;

	/*
	 * Numbers used to generate the zobrist keys.
	 *
	 * Length is zobrist[z_i][z_j].
	 *
	 * First dimension (i) is piece type's index from 0 to 7
	 * converted from its bit value (HNEF_BIT_U8), which is
	 * 1, 2, 4...128.
	 *
	 * Second dimension (j) is board position.
	 *
	 * zobrist_lock works the same way but is generated from
	 * different random numbers.
	 */
/*@notnull@*/
/*@owned@*/
	unsigned int		* * zobrist_key,
				* * zobrist_lock;

	/*
	 * z_i is the length of the first array in zobrist_key. This is
	 * equal to ruleset->type_piecec.
	 *
	 * z_j is the length of the second array in zobrist_key. This is
	 * equal to the board length (ruleset->pieces, or bwidth *
	 * bheight).
	 */
	unsigned short		z_i,
				z_j;

	/*
	 * Memory used for table and columns.
	 */
	size_t			mem_tab,
				mem_col;

};

/*
 * Memory for collision lists in zhashtable.
 *
 * When a new node is added to memchunk, it's always appended last.
 *
 * When a move is removed from memchunk, the last node's (at index
 * memchunk[elemc - 1]) values are copied into the removed node, and
 * then elemc reduces by 1. This works because the nodes in memchunk are
 * randomly ordered, and only referred to from linked collision lists
 * (zhashnode->next).
 *
 * NOTE:	This has one weakness: it depends on zhashnode pointers
 *		(reached from zhashtable->arr[x].zhashnode->next)
 *		pointing to memory in zhashcols->memchunk. Thus you can
 *		never re-alloc memchunk, since that will invalidate all
 *		old pointers. You could probably re-generate them, but
 *		it's far easier to require the user to set fixed sizes
 *		for the hash table and collision lists.
 */
struct hnef_zhashcols
{

	/*
	 * Memory for colliding zhashnodes (zhashnode->next) that are
	 * not stored in zhashtable->arr.
	 *
	 * Note that nodes in memchunk are not flagged with value_type
	 * HNEF_AIM_ZHNEF_VACANT. It's not necessary since all nodes in
	 * memchunk up to `elemc - 1` will be non-vacant, and all nodes
	 * from `elemc` and onwards will be vacant.
	 */
/*@notnull@*/
/*@owned@*/
	struct hnef_zhashnode	* memchunk;

	/*
	 * elemc is the amount of used zhashnodes in memchunk. elemc <=
	 * capc.
	 *
	 * capc is the length of memchunk.
	 */
	size_t			elemc,
				capc;

};

#endif

#endif

