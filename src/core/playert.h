/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_PLAYER_T_H
#define HNEF_CORE_PLAYER_T_H

#include "boardt.h"	/* HNEF_BIT_U8 */

/*@unchecked@*/
extern
const unsigned short	HNEF_PLAYERS_MAX;

/*@unchecked@*/
extern
const unsigned short	HNEF_PLAYER_UNINIT;

/*@exposed@*/
struct hnef_player
{

	/*
	 * Player index, from 0 to HNEF_PLAYERS_MAX - 1.
	 *
	 * The index must be valid according to player_index_valid.
	 */
	unsigned short	index;

	/*
	 * Optimization.
	 *
	 * Bitmask of pieces owned by the player. Set by player_initopt.
	 *
	 * The computer player benefits from having opt_owned_esc
	 * pre-calculated.
	 */
	HNEF_BIT_U8	opt_owned,
			opt_owned_esc;

};

#endif

