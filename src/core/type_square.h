/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_TYPE_SQUARE_T
#define HNEF_CORE_TYPE_SQUARE_T

#include "config.h"		/* HNEF_RSTR */
#include "funct.h"		/* HNEF_FR */
#include "rulesett.h"		/* hnef_ruleset */
#include "type_squaret.h"	/* hnef_type_square */

/*
 * Value for capt_sides not overridden by hnef_type_square.
 */
/*@unchecked@*/
extern
unsigned short HNEF_TSQUARE_CAPT_SIDES_NONE;

extern
HNEF_BOOL
hnef_type_square_can_capture (
/*@in@*/
/*@notnull@*/
	const struct hnef_type_square	* const HNEF_RSTR,
	const HNEF_BIT_U8
	)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
extern
void
hnef_type_square_init (
/*@in@*/
/*@notnull@*/
	struct hnef_type_square	* const HNEF_RSTR ts
	)
/*@modifies * ts@*/
;
/*@=protoparamname@*/

/*@in@*/
/*@notnull@*/
extern
struct hnef_type_square *
hnef_type_square_get (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	struct hnef_ruleset * const HNEF_RSTR,
	const HNEF_BIT_U8
	)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_type_square_set (
/*@in@*/
/*@notnull@*/
	struct hnef_ruleset	* const HNEF_RSTR rules,
	const HNEF_BIT_U8	bit
	)
/*@modifies * rules@*/
;
/*@=protoparamname@*/

#endif

