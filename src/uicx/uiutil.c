/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */

#include "gleipnir_lang.h"	/* gleip_lang_* */

#include "cliiot.h"	/* HNEF_CMD_* */
#include "filefind.h"	/* hnef_fopen_dirguess */
#include "funcx.h"	/* hnef_frx_good */
#include "langkeyt.h"	/* HNEF_LK_* */
#include "uiutil.h"

#ifdef	HNEFATAFL_UI_AIM
#include "uiutilt.h"	/* HNEF_AIM_DEPTH_DEF */
#endif	/* HNEFATAFL_UI_AIM */

/*
 * Shared CLI (`cli.c`) / XLib (`xlib.c`) functions.
 */

/*@observer@*/
/*@unchecked@*/
static
const char	* const SUBDIR_LANG	= "lang",
		* const SUBDIR_RULES	= "rules";

#ifdef	HNEFATAFL_UI_AIM

enum HNEF_FR
hnef_computer_move (
	struct hnef_ui	* const HNEF_RSTR ui,
	void		* data,
	enum HNEF_FR	(* func_interrupt)
				(void *, HNEF_BOOL *, HNEF_BOOL *),
	enum HNEF_FR	(* func_progress)
				(void *, int, int),
	HNEF_BOOL	* const HNEF_RSTR success
	)
{
	struct hnef_control	* HNEF_RSTR control	= NULL;
	struct hnef_game	* HNEF_RSTR game	= NULL;
	enum HNEF_FR		fr		= HNEF_FR_SUCCESS;
	HNEF_BOOL		legal;
	unsigned short		pos,
				dest;

	assert	(NULL != ui);
	assert	(NULL != success);

	* success	= HNEF_FALSE;
	game		= ui->game;

	assert	(NULL != game);
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(game)));

#ifndef	NDEBUG
	{
		HNEF_BOOL	gameover;
		unsigned short	winner	= HNEF_PLAYER_UNINIT;
		gameover	= hnef_game_over(game, & winner);
		assert		(!gameover);
	}
#endif	/* NDEBUG */
	assert	(NULL != game->board);

	control	= ui->controlv[game->board->turn];
	assert	(NULL != control);
	assert	(NULL != control->aiminimax);

	fr	= aiminimax_command(game, control->aiminimax,
		& pos, & dest, data, func_interrupt, func_progress);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	if	(HNEF_BOARDPOS_NONE == pos
	||	HNEF_BOARDPOS_NONE == dest)
	{
		assert	(!* success);
		return	HNEF_FR_SUCCESS;
	}

	assert	(HNEF_BOARDPOS_NONE != pos);
	assert	(HNEF_BOARDPOS_NONE != dest);

	fr	= hnef_game_move(game, pos, dest, & legal);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	/*
	 * The computer should never select illegal moves.
	 */
	assert	(legal);

	* success	= legal;
	return		HNEF_FR_SUCCESS;
}

#endif	/* HNEFATAFL_UI_AIM */

/*
 * -	If the game is invalid, it always returns `HUMAN`.
 * -	If the game is over, it always returns `HUMAN`.
 * -	Else it returns the `hnef_control->type` whose turn it is.
 */
enum HNEF_CONTROL_TYPE
hnef_control_type_get (
	struct hnef_ui	* const HNEF_RSTR ui
	)
{
	unsigned short	winner	= HNEF_PLAYER_UNINIT;

	assert	(NULL != ui);
	assert	(NULL != ui->game);

	if	(!hnef_rvalid_good(hnef_game_valid(ui->game))
	||	hnef_game_over(ui->game, & winner))
	{
		/*
		 * `game->board` is `NULL` if the game is invalid, so we
		 * can't check whose turn it is.
		 */
		return	HNEF_CONTROL_TYPE_HUMAN;
	}
	else
	{
		/*
		 * The game is valid, so `board` must be `!NULL`.
		 */
		assert	(NULL != ui->game->board);
		return	ui->controlv[ui->game->board->turn]->type;
	}
}

#ifdef	HNEFATAFL_UI_AIM

HNEF_BOOL
hnef_control_computer (
	struct hnef_ui	* const HNEF_RSTR ui
	)
{
	assert	(NULL != ui);
	assert	(NULL != ui->game);
	return	HNEF_CONTROL_TYPE_AIM == hnef_control_type_get(ui);
}

#endif	/* HNEFATAFL_UI_AIM */

/*
 * Checks if `frx` is good. It's considered good if it's `NULL`.
 */
static
HNEF_BOOL
hnef_frx_good_null (
/*@in@*/
/*@null@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
/*@modifies nothing@*/
{
	return	NULL == frx
		? HNEF_TRUE
		: hnef_frx_good(* frx);
}

/*
 * Sets player `index` to `type`.
 *
 * -	`depth` is the search depth parameter for the minimax player.
 *	-	Requires `HNEFATAFL_UI_AIM` to be defined.
 *	-	0 means use default value.
 * -	`mem_*` are memory parameters for hashing.
 *	-	Requires `HNEFATAFL_UI_ZHASH` to be defined.
 *	-	0 means use default value.
 *
 * Setting `type` to `*_HUMAN` is always possible.
 *
 * Setting `type` to `*_AIM` is possible if the game is invalid but then
 * the `aiminimax` struct can't be allocated. If so, it must be
 * allocated when a ruleset is loaded; in that case the depth and memory
 * settings are set in controller but the `hnef_aiminimax` struct is not
 * allocated (see `cli.c::parseline_rules()`).
 */
static
enum HNEF_FR
hnef_ui_player_set (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
	const unsigned short		index,
	const enum HNEF_CONTROL_TYPE	type
#ifdef	HNEFATAFL_UI_AIM
	,
	unsigned short			depth
#ifdef	HNEFATAFL_UI_ZHASH
	,
	size_t				mem_tab,
	size_t				mem_col
#endif	/* HNEFATAFL_UI_ZHASH */
#endif	/* HNEFATAFL_UI_AIM */
	)
#ifdef	HNEFATAFL_UI_ZHASH
/*@globals errno, internalState@*/
#else	/* HNEFATAFL_UI_ZHASH */
/*@globals internalState@*/
#endif	/* HNEFATAFL_UI_ZHASH */

#ifdef	HNEFATAFL_UI_ZHASH
/*@modifies errno, internalState@*/
#endif	/* HNEFATAFL_UI_ZHASH */
/*@modifies * ui@*/
{
	struct hnef_control	* HNEF_RSTR control	= NULL;

	assert	(NULL != ui);
	assert	(index < ui->game->playerc);

#ifdef	HNEFATAFL_UI_AIM
	assert	(HNEF_UI_AIMARG_DEPTH_INVALID == depth
	||	hnef_aiminimax_depth_max_valid(depth));
#ifdef	HNEFATAFL_UI_ZHASH
	assert	(HNEF_UI_AIMARG_MEM_INVALID == mem_tab
	||	hnef_zhash_mem_tab_valid(mem_tab));
	assert	(HNEF_UI_AIMARG_MEM_INVALID == mem_col
	||	hnef_zhash_mem_col_valid(mem_col));
#endif	/* HNEFATAFL_UI_ZHASH */
#endif	/* HNEFATAFL_UI_AIM */

	control		= ui->controlv[index];
	control->type	= type;

	if	(HNEF_CONTROL_TYPE_HUMAN == type)
	{
#ifdef	HNEFATAFL_UI_AIM
		if	(NULL != control->aiminimax)
		{
			hnef_free_aiminimax	(control->aiminimax);
			control->aiminimax	= NULL;
		}
		control->aiminimax_depth = HNEF_UI_AIMARG_DEPTH_INVALID;
#ifdef	HNEFATAFL_UI_ZHASH
		control->aiminimax_mem_tab = HNEF_UI_AIMARG_MEM_INVALID;
		control->aiminimax_mem_col = HNEF_UI_AIMARG_MEM_INVALID;
#endif	/* HNEFATAFL_UI_ZHASH */
#endif	/* HNEFATAFL_UI_AIM */
	}
#ifdef	HNEFATAFL_UI_AIM
	else if	(HNEF_CONTROL_TYPE_AIM == type)
	{
		if	(HNEF_UI_AIMARG_DEPTH_INVALID == depth)
		{
			depth	= HNEF_AIM_DEPTH_DEF;
		}
#ifdef	HNEFATAFL_UI_ZHASH
		if	(HNEF_UI_AIMARG_MEM_INVALID == mem_tab)
		{
			mem_tab	= hnef_zhash_mem_tab_def();
		}
		if	(HNEF_UI_AIMARG_MEM_INVALID == mem_col)
		{
			mem_col	= hnef_zhash_mem_col_def();
		}
#endif	/* HNEFATAFL_UI_ZHASH */

		control->aiminimax_depth	= depth;
#ifdef	HNEFATAFL_UI_ZHASH
		control->aiminimax_mem_tab	= mem_tab;
		control->aiminimax_mem_col	= mem_col;
#endif	/* HNEFATAFL_UI_ZHASH */

		if	(hnef_rvalid_good(hnef_game_valid(ui->game)))
		{
			enum HNEF_FR	fr	= HNEF_FR_SUCCESS;
			struct hnef_aiminimax	* HNEF_RSTR aim_old =
						control->aiminimax;
#ifdef	HNEFATAFL_UI_ZHASH
			struct hnef_zhashtable	* HNEF_RSTR table_old =
						NULL == aim_old
						? NULL
						: aim_old->tp_tab;

			assert	(NULL == aim_old
			||	NULL != aim_old->tp_tab);

			if	(NULL != table_old
			&&	(	control->aiminimax_mem_tab
						!= table_old->mem_tab
				||	control->aiminimax_mem_col
						!= table_old->mem_col))
			{
				/*
				 * Memory settings don't equal, so we
				 * have to allocate a new table and we
				 * can't reuse the old one.
				 */
				table_old	= NULL;
			}
#endif	/* HNEFATAFL_UI_ZHASH */

			control->aiminimax = hnef_alloc_aiminimax
				(ui->game, index,
				control->aiminimax_depth, & fr
#ifdef	HNEFATAFL_UI_ZHASH
				, table_old
				, control->aiminimax_mem_tab
				, control->aiminimax_mem_col
#endif	/* HNEFATAFL_UI_ZHASH */
				);
			if	(NULL == control->aiminimax)
			{
				assert	(!hnef_fr_good(fr));
				/*
				 * `aim_old` is intact, so restore it.
				 */
				control->aiminimax = aim_old;
				return	HNEF_FR_FAIL_ALLOC;
			}
			else
			{
				assert	(hnef_fr_good(fr));
				/*
				 * `aim_old` has a pointer to the hash
				 * table which we just moved to the new
				 * `aiminimax`. So we need to set that
				 * reference to `NULL` and then free it.
				 */
				if	(NULL != aim_old)
				{
#ifdef	HNEFATAFL_UI_ZHASH
					if	(NULL != table_old)
					{
						/*
						 * We have moved the
						 * table, so don't free
						 * it.
						 */
						aim_old->tp_tab	= NULL;
					}
#endif	/* HNEFATAFL_UI_ZHASH */
					hnef_free_aiminimax(aim_old);
				}
			}
		}
		else
		{
			/*
			 * The game is not valid, so the `aiminimax`
			 * struct can't be allocated until a ruleset is
			 * loaded.
			 */
			assert	(NULL == control->aiminimax);
		}
	}
#endif	/* HNEFATAFL_UI_AIM */
	else
	{
		assert	(HNEF_FALSE);
		return	HNEF_FR_FAIL_ILL_STATE;
	}

	return	HNEF_FR_SUCCESS;
}

static
HNEF_BOOL
hnef_uitil_undo_smart_is (
/*@in@*/
/*@notnull@*/
	const struct hnef_ui	* const HNEF_RSTR ui,
	const unsigned short	index
	)
/*@modifies nothing@*/
{
	unsigned short	index_other;

	assert	(NULL != ui);
	assert	(index < ui->game->playerc
	&&	index < ui->controlc);

	index_other	= 0 == index
			? (unsigned short)1
			: 0;
	assert	(index_other < ui->game->playerc
	&&	index_other < ui->controlc);

	return HNEF_CONTROL_TYPE_HUMAN == ui->controlv[index]->type
	&& HNEF_CONTROL_TYPE_HUMAN != ui->controlv[index_other]->type;
}

/*
 * Returns 1 or 2 moves to undo, depending on if it's a smart undo.
 * However, if only one move has been made, it always returns 1, even if
 * it is a smart undo.
 *
 * `index` is the index of the player who is undoing.
 */
static
size_t
hnef_uiutil_undo_smart_count (
/*@in@*/
/*@notnull@*/
	const struct hnef_ui	* const HNEF_RSTR ui,
	const unsigned short	index
	)
/*@modifies nothing@*/
{
	assert	(NULL != ui);
	assert	(index < ui->game->playerc
	&&	index < ui->controlc);

	if	(hnef_uitil_undo_smart_is(ui, index))
	{
		return	ui->game->movehist->elemc > (size_t)1
			? (size_t)2
			: (size_t)1;
	}
	else
	{
		return	(size_t)1;
	}
}

enum HNEF_FR
hnef_ui_parseline_game_load_filename (
	struct hnef_ui	* const HNEF_RSTR ui,
	const char	* const HNEF_RSTR filename,
	HNEF_BOOL	* const HNEF_RSTR success,
	enum HNEF_FR	(* func_print_langkey)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const char	* const),
	enum HNEF_FR	(* func_print_fr)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const enum HNEF_FR),
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
{
	HNEF_BOOL	id_good;
	enum HNEF_FR	fr		= HNEF_FR_SUCCESS;

	assert	(NULL != ui);
	assert	(NULL != filename);
	assert	(NULL != success);

	* success	= HNEF_FALSE;

	if	(!hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_GAME_INVALID);
	}

	if	(hnef_line_empty(filename))
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_GAME_LOAD_MISS);
	}

	hnef_listm_clear(ui->opt_movelist);

	fr	= hnef_game_load(ui->game, filename, & id_good);
	if	(!hnef_fr_good(fr))
	{
		return	NULL == func_print_fr
			? HNEF_FR_SUCCESS
			: func_print_fr(ui, frx, fr);
	}

	if	(!id_good)
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_GAME_LOAD_ID);
	}

	* success	= HNEF_TRUE;

	return	HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_ui_parseline_game_load (
	struct hnef_ui			* const HNEF_RSTR ui,
	const struct gleip_lines	* const GLEIP_RSTR line,
	HNEF_BOOL			* const HNEF_RSTR success,
	enum HNEF_FR			(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
	enum HNEF_FR			(* func_print_fr)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_FR),
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
{
	assert	(NULL != ui);
	assert	(NULL != line);
	assert	(NULL != success);

	* success	= HNEF_FALSE;

	if	(gleip_lines_size(line) < (size_t)2)
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_GAME_LOAD_MISS);
	}

	return	hnef_ui_parseline_game_load_filename(ui,
			gleip_lines_get_ro(line, (size_t)1),
			success,
			func_print_langkey,
			func_print_fr,
			frx);
}

enum HNEF_FR
hnef_ui_parseline_game_save (
	struct hnef_ui			* const HNEF_RSTR ui,
	const struct gleip_lines	* const GLEIP_RSTR line,
	enum HNEF_FR			(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
	enum HNEF_FR			(* func_print_fr)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_FR),
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
{
	enum HNEF_FR	fr			= HNEF_FR_SUCCESS;
	const char	* HNEF_RSTR filename	= NULL;

	if	(!hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_GAME_INVALID);
	}

	assert	(NULL != ui);
	assert	(NULL != line);

	if	(gleip_lines_size(line) < (size_t)2
	||	hnef_line_empty(filename =
				gleip_lines_get_ro(line, (size_t)1)))
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_GAME_SAVE_MISS);
	}
	assert	(NULL != filename);

	fr	= hnef_game_save(ui->game, filename);
	if	(!hnef_fr_good(fr))
	{
		return	NULL == func_print_fr
			? HNEF_FR_SUCCESS
			: func_print_fr(ui, frx, fr);
	}

	return	HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_ui_parseline_game_undo (
	struct hnef_ui			* const HNEF_RSTR ui,
	const struct gleip_lines	* const GLEIP_RSTR line,
	HNEF_BOOL			* const HNEF_RSTR success,
	enum HNEF_FR			(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
{
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;
	size_t		undoc;

	assert	(NULL != ui);
	assert	(NULL != line);
	assert	(NULL != success);

	* success	= HNEF_FALSE;

	if	(!hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_GAME_INVALID);
	}
	assert	(NULL != ui->game->board);

	if	(ui->game->movehist->elemc < (size_t)1)
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_GAME_UNDO_START);
	}

	if	(gleip_lines_size(line) > (size_t)1)
	{
		if	(!hnef_texttosize(gleip_lines_get_ro
					(line, (size_t)1), & undoc))
		{
			return	NULL == func_print_langkey
				? HNEF_FR_SUCCESS
				: func_print_langkey(ui, frx,
					HNEF_LK_CMD_GAME_UNDO_NUM);
		}
	}
	else
	{
		undoc	= hnef_uiutil_undo_smart_count(ui,
				ui->game->board->turn);
	}

	fr	= hnef_game_undo(ui->game, success, undoc);
	if	(!hnef_fr_good(fr))
	{
		/*
		 * Moves that have been played must be possible to
		 * replay, so this is never an error that we should
		 * print and ignore.
		 */
		return	fr;
	}
/*@i2@*/\
	else if	(!* success)
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_GAME_UNDO_NUM);
	}

	return	HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_ui_parseline_game_new (
	struct hnef_ui		* const HNEF_RSTR ui,
	enum HNEF_FR		(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
{
	assert	(NULL != ui);

	if	(!hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_GAME_INVALID);
	}

	hnef_game_reset	(ui->game);
	hnef_listm_clear(ui->opt_movelist);

	return	HNEF_FR_SUCCESS;
}

/*
 * Reads a language file.
 *
 * `ui->lang` is only updated if the new language file is valid. Else
 * it's not updated (not even if the old language file was invalid).
 */
static
enum HNEF_FR
hnef_ui_parseline_lang_file (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	FILE		* const HNEF_RSTR file,
/*@null@*/
	enum HNEF_FR	(* func_print_langkey)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const char	* const),
/*@null@*/
	enum HNEF_FR	(* func_print_fr)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const enum HNEF_FR),
/*@null@*/
	enum HNEF_FR	(* func_print_str)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const char	* const),
/*@in@*/
/*@null@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
/*@globals errno@*/
/*@modifies errno, * ui, * file, * frx@*/
{
	int	valid	= 1;

	assert	(NULL != ui);
	assert	(NULL != ui->lang);
	assert	(NULL != ui->lang_tmp);
	assert	(NULL != file);

/*@i1@*/\
	if	(!gleip_lang_read_file(ui->lang_tmp, file,
					ui->opt_rulesetline, & valid))
	{
		gleip_lang_clear(ui->lang_tmp);
		return	NULL == func_print_fr
			? HNEF_FR_SUCCESS
			: func_print_fr(ui, frx, HNEF_FR_FAIL_ALLOC);
	}
/*@i1@*/\
	else if	(ferror(file))
	{
		gleip_lang_clear(ui->lang_tmp);
		return	NULL == func_print_fr
			? HNEF_FR_SUCCESS
			: func_print_fr(ui, frx,
					HNEF_FR_FAIL_IO_FILE_READ);
	}
/*@i1@*/\
	else if	(!valid)
	{
		enum HNEF_FR	fr	= HNEF_FR_SUCCESS;
		const char	* const HNEF_RSTR why =
			gleip_lang_invalid_why(ui->lang_tmp);

		if	(NULL != func_print_langkey)
		{
			fr	= func_print_langkey(ui, frx,
				HNEF_LK_CMD_LANG_MALF);
			if	(!hnef_fr_good(fr)
			||	!hnef_frx_good_null(frx))
			{
				gleip_lang_clear(ui->lang_tmp);
				return	fr;
			}
		}

		if	(NULL != func_print_str
		&&	NULL != why)
		{
			fr	= func_print_str(ui, frx, why);
			if	(!hnef_fr_good(fr)
			||	!hnef_frx_good_null(frx))
			{
				gleip_lang_clear(ui->lang_tmp);
				return	fr;
			}
		}

		gleip_lang_clear(ui->lang_tmp);
	}
	else
	{
		struct gleip_lang * const GLEIP_RSTR tmp = ui->lang;
/*@i1@*/\
		assert	(gleip_lang_valid(ui->lang_tmp));
		ui->lang	= ui->lang_tmp;
		ui->lang_tmp	= tmp;
/*@i1@*/\
		assert	(gleip_lang_valid(ui->lang));
	}
	return	HNEF_FR_SUCCESS;
/* SPLint frx is modified */ /*@i1@*/\
}

/*
 * Opens a file specified by `filename` and reads it.
 *
 * This opens a file by dir-guessing the filename in the SUBDIR_LANG
 * sub-directory. The absolute filename itself is tried if
 * `try_absolute`.
 */
enum HNEF_FR
hnef_ui_parseline_lang_filename (
	struct hnef_ui	* const HNEF_RSTR ui,
	const char	* const HNEF_RSTR filename,
	const HNEF_BOOL	try_absolute,
	enum HNEF_FR	(* func_print_langkey)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const char	* const),
	enum HNEF_FR	(* func_print_fr)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const enum HNEF_FR),
	enum HNEF_FR	(* func_print_str)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const char	* const),
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
{
	FILE		* HNEF_RSTR file	= NULL;
	enum HNEF_FR	fr			= HNEF_FR_SUCCESS;

	assert	(NULL != ui);
	assert	(NULL != filename);
	assert	(!hnef_line_empty(SUBDIR_LANG));

	if	(hnef_line_empty(filename))
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_LANG_MISS);
	}

	file	= hnef_fopen_dirguess(ui->filefind,
			SUBDIR_LANG, filename, try_absolute, & fr);
	if	(!hnef_fr_good(fr))
	{
		assert	(NULL == file);
		return	fr;
	}
/*@i1@*/\
	else if	(NULL == file)
	{
		return	NULL == func_print_fr
			? HNEF_FR_SUCCESS
			: func_print_fr
				(ui, frx, HNEF_FR_FAIL_IO_FILE_OPEN);
	}

	fr	= hnef_ui_parseline_lang_file(ui, file,
				func_print_langkey,
				func_print_fr,
				func_print_str,
				frx);
	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good_null(frx))
	{
		(void)fclose(file);
		return	fr;
	}

	return	EOF == fclose(file)
		? HNEF_FR_FAIL_IO_FILE_CLOSE
		: HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_ui_parseline_lang (
	struct hnef_ui			* const HNEF_RSTR ui,
	const struct gleip_lines	* const GLEIP_RSTR line,
	const HNEF_BOOL			try_absolute,
	enum HNEF_FR			(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
	enum HNEF_FR			(* func_print_fr)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_FR),
	enum HNEF_FR			(* func_print_str)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
{
	assert	(NULL != ui);
	assert	(NULL != line);

	if	(gleip_lines_size(line) < (size_t)2)
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_LANG_MISS);
	}

	return	hnef_ui_parseline_lang_filename(ui,
		gleip_lines_get_ro(line, (size_t)1),
		try_absolute,
		func_print_langkey,
		func_print_fr,
		func_print_str,
		frx);
}

/*
 * Reads a ruleset file.
 *
 * `ui->game` is only updated if the new ruleset is valid. Else it's not
 * updated (not even if the old ruleset is invalid).
 */
static
enum HNEF_FR
hnef_ui_parseline_rules_file (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	FILE		* const HNEF_RSTR file,
/*@null@*/
	enum HNEF_FR	(* func_print_fr)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const enum HNEF_FR),
/*@null@*/
	enum HNEF_FR	(* func_print_rread)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const enum HNEF_RREAD),
/*@null@*/
	enum HNEF_FR	(* func_print_rvalid)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const enum HNEF_RVALID),
/*@null@*/
	enum HNEF_FR	(* func_print_lines)
				(struct hnef_ui		* const,
				enum HNEF_FRX		* const,
				const struct gleip_lines * const),
/*@in@*/
/*@null@*/
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem, internalState@*/
/*@modifies errno, fileSystem, internalState, * ui, * file, * frx@*/
{
	enum HNEF_FR		fr	= HNEF_FR_SUCCESS;
	enum HNEF_RREAD		rread	= HNEF_RREAD_SUCCESS;
	enum HNEF_RVALID	rvalid	= HNEF_RVALID_SUCCESS;

#ifdef	HNEFATAFL_UI_AIM
	unsigned short		i;
#endif	/* HNEFATAFL_UI_AIM */

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->game_tmp);
	assert	(NULL != file);

	hnef_game_clear(ui->game_tmp);

#ifdef	HNEFATAFL_UI_AIM
	/*
	 * `hnef_game_clear()` resets the ruleset. So the computer
	 * players also have to be freed and, later, allocated again.
	 */
	for	(i = 0; i < ui->controlc; ++i)
	{
		struct hnef_control	* HNEF_RSTR control =
					ui->controlv[i];
		if	(NULL != control->aiminimax)
		{
			assert(HNEF_CONTROL_TYPE_AIM == control->type);
			hnef_free_aiminimax	(control->aiminimax);
			control->aiminimax	= NULL;
		}
	}
#endif	/* HNEFATAFL_UI_AIM */

	fr = hnef_game_init_file(ui->game_tmp, file, & rread, & rvalid);
	if	(!hnef_fr_good(fr)
	||	!hnef_rread_good(rread)
	||	!hnef_rvalid_good(rvalid))
	{
		enum HNEF_FR	fr2	= HNEF_FR_SUCCESS;

		if	(!hnef_fr_good(fr))
		{
			if	(hnef_fr_fail_io(fr))
			{
				if	(NULL != func_print_fr)
				{
					fr2	= func_print_fr
						(ui, frx, fr);
					if (!hnef_fr_good(fr2)
					|| !hnef_frx_good_null(frx))
					{
						return	fr2;
					}
				}
			}
			else
			{
				return	fr;
			}
		}
		else if	(!hnef_rread_good(rread))
		{
			if	(NULL != func_print_rread)
			{
				fr2 = func_print_rread(ui, frx, rread);
				if	(!hnef_fr_good(fr2)
				||	!hnef_frx_good_null(frx))
				{
					return	fr2;
				}
			}

			if	(NULL != func_print_lines)
			{
				fr2	= func_print_lines
					(ui, frx, ui->opt_rulesetline);
				if	(!hnef_fr_good(fr2)
				||	!hnef_frx_good_null(frx))
				{
					return	fr2;
				}
			}
		}
		else if	(!hnef_rvalid_good(rvalid))
		{
			if	(NULL != func_print_rvalid)
			{
				fr2	= func_print_rvalid
					(ui, frx, rvalid);
				if	(!hnef_fr_good(fr2)
				||	!hnef_frx_good_null(frx))
				{
					return	fr2;
				}
			}
		}
		else
		{
			assert	(HNEF_FALSE);
		}

		gleip_lines_rm(ui->opt_rulesetline);
		return	HNEF_FR_SUCCESS;
	}

/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(ui->game_tmp)));
	{
		struct hnef_game * const HNEF_RSTR tmp = ui->game;
		ui->game	= ui->game_tmp;
		ui->game_tmp	= tmp;
	}
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(ui->game)));

	hnef_game_clear	(ui->game_tmp);
	hnef_listm_clear(ui->opt_movelist);

#ifdef	HNEFATAFL_UI_AIM
	/*
	 * Now, that the ruleset has been successfully loaded, we need
	 * to allocate the computer players again.
	 */
	for	(i = 0; i < ui->controlc; ++i)
	{
		struct hnef_control	* HNEF_RSTR control =
					ui->controlv[i];
		if	(HNEF_CONTROL_TYPE_AIM == control->type)
		{
			assert	(NULL == control->aiminimax);

#ifdef	HNEFATAFL_UI_AIM
			assert	(HNEF_UI_AIMARG_DEPTH_INVALID
					== control->aiminimax_depth
			||	hnef_aiminimax_depth_max_valid
					(control->aiminimax_depth));
#ifdef	HNEFATAFL_UI_ZHASH
			assert	(HNEF_UI_AIMARG_MEM_INVALID
					== control->aiminimax_mem_tab
			||	hnef_zhash_mem_tab_valid
					(control->aiminimax_mem_tab));
			assert	(HNEF_UI_AIMARG_MEM_INVALID
					== control->aiminimax_mem_col
			||	hnef_zhash_mem_col_valid
					(control->aiminimax_mem_col));
#endif	/* HNEFATAFL_UI_ZHASH */
#endif	/* HNEFATAFL_UI_AIM */

			control->aiminimax = hnef_alloc_aiminimax
				(ui->game, i, control->aiminimax_depth,
				& fr
#ifdef	HNEFATAFL_UI_ZHASH
				, NULL	/* Old hash table */
				, control->aiminimax_mem_tab
				, control->aiminimax_mem_col
#endif	/* HNEFATAFL_UI_ZHASH */
				);
			if	(NULL == control->aiminimax)
			{
				return	HNEF_FR_FAIL_ALLOC;
			}
		}
	}
#endif	/* HNEFATAFL_UI_AIM */

	gleip_lines_rm(ui->opt_rulesetline);
	return	HNEF_FR_SUCCESS;
/* SPLint frx is modified */ /*@i1@*/\
}

/*
 * Opens a file specified by `filename` and reads it.
 *
 * This opens a file by dir-guessing the filename in the SUBDIR_RULES
 * sub-directory. The absolute filename itself is tried if
 * `try_absolute`.
 */
enum HNEF_FR
hnef_ui_parseline_rules_filename (
	struct hnef_ui		* const HNEF_RSTR ui,
	const char		* const HNEF_RSTR filename,
	const HNEF_BOOL		try_absolute,
	enum HNEF_FR		(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
	enum HNEF_FR		(* func_print_fr)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_FR),
	enum HNEF_FR		(* func_print_rread)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_RREAD),
	enum HNEF_FR		(* func_print_rvalid)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_RVALID),
	enum HNEF_FR		(* func_print_lines)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
				const struct gleip_lines * const),
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
{
	FILE		* HNEF_RSTR file	= NULL;
	enum HNEF_FR	fr			= HNEF_FR_SUCCESS;

	assert	(NULL != ui);
	assert	(NULL != filename);
	assert	(!hnef_line_empty(SUBDIR_RULES));

	if	(hnef_line_empty(filename))
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_RULES_MISS);
	}

	file	= hnef_fopen_dirguess(ui->filefind,
			SUBDIR_RULES, filename, try_absolute, & fr);
	if	(!hnef_fr_good(fr))
	{
		assert	(NULL == file);
		return	fr;
	}
/*@i1@*/\
	else if	(NULL == file)
	{
		return	NULL == func_print_fr
			? HNEF_FR_SUCCESS
			: func_print_fr
				(ui, frx, HNEF_FR_FAIL_IO_FILE_OPEN);
	}

	fr	= hnef_ui_parseline_rules_file(ui, file,
			func_print_fr,
			func_print_rread,
			func_print_rvalid,
			func_print_lines,
			frx);
	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good_null(frx))
	{
		(void)fclose(file);
		return	fr;
	}

	return	EOF == fclose(file)
		? HNEF_FR_FAIL_IO_FILE_CLOSE
		: HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_ui_parseline_rules (
	struct hnef_ui			* const HNEF_RSTR ui,
	const struct gleip_lines	* const GLEIP_RSTR line,
	const HNEF_BOOL			try_absolute,
	enum HNEF_FR			(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
	enum HNEF_FR			(* func_print_fr)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_FR),
	enum HNEF_FR			(* func_print_rread)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_RREAD),
	enum HNEF_FR			(* func_print_rvalid)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_RVALID),
	enum HNEF_FR			(* func_print_lines)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
				const struct gleip_lines * const),
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
{
	assert	(NULL != ui);
	assert	(NULL != line);

	if	(gleip_lines_size(line) < (size_t)2)
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_RULES_MISS);
	}

	return	hnef_ui_parseline_rules_filename(ui,
			gleip_lines_get_ro(line, (size_t)1),
			try_absolute,
			func_print_langkey,
			func_print_fr,
			func_print_rread,
			func_print_rvalid,
			func_print_lines,
			frx);
}

/*
 * `p`				prints all players.
 * `p <index>`			prints player index.
 * `p <index> <type> <args>`	sets player index to <type> with <args>.
 *
 * -	The human type (`HNEF_CMD_PLAYER_HUMAN`) has no <args>.
 * -	The minimax type (`HNEF_CMD_PLAYER_AIM`) takes the following
 *	<args> (use `HNEF_CMD_ARG_DEF` for default value):
 *	1.	search depth
 *	2.	mem_tab (in bytes)
 *	3.	mem_col (in bytes)
 */
enum HNEF_FR
hnef_ui_parseline_player (
	struct hnef_ui			* const HNEF_RSTR ui,
	const struct gleip_lines	* const GLEIP_RSTR line,
	enum HNEF_FR			(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
	enum HNEF_FR			(* func_print_player)
					(struct hnef_ui	* const,
					const unsigned short,
					enum HNEF_FRX	* const),
	enum HNEF_FR			(* func_print_players)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const),
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
{
	unsigned short	player_index;
	char		player_type;

	assert	(NULL != ui);
	assert	(NULL != line);

	if	(gleip_lines_size(line) < (size_t)2)
	{
		/*
		 * Print all players.
		 */
		return	NULL == func_print_players
			? HNEF_FR_SUCCESS
			: func_print_players(ui, frx);
	}

	if	(!hnef_texttoushort(gleip_lines_get_ro
				(line, (size_t)1), & player_index)
	||	player_index >= ui->game->playerc)
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_PLAYER_INDEX);
	}

	if	(gleip_lines_size(line) < (size_t)3
	||	hnef_line_empty(gleip_lines_get_ro(line, (size_t)2)))
	{
		/*
		 * Print single player.
		 */
		assert	(player_index < ui->game->playerc);
		return	NULL == func_print_player
			? HNEF_FR_SUCCESS
			: func_print_player(ui, player_index, frx);
	}

	player_type	= gleip_lines_get_ro(line, (size_t)2)[0];

	if	(HNEF_CMD_PLAYER_HUMAN == player_type)
	{
		/*
		 * The `depth` and `mem_*` parameters are ignored so the
		 * values don't matter.
		 */
		return	hnef_ui_player_set(ui, player_index,
				HNEF_CONTROL_TYPE_HUMAN
#ifdef	HNEFATAFL_UI_AIM
				, HNEF_UI_AIMARG_DEPTH_INVALID
#ifdef	HNEFATAFL_UI_ZHASH
				, HNEF_UI_AIMARG_MEM_INVALID
				, HNEF_UI_AIMARG_MEM_INVALID
#endif	/* HNEFATAFL_UI_ZHASH */
#endif	/* HNEFATAFL_UI_AIM */
				);
	}
#ifdef	HNEFATAFL_UI_AIM
	else if	(HNEF_CMD_PLAYER_AIM == player_type)
	{
		unsigned short	depth;
#ifdef	HNEFATAFL_UI_ZHASH
		size_t		mem_tab,
				mem_col;
#endif	/* HNEFATAFL_UI_ZHASH */

		if	(gleip_lines_size(line) < (size_t)4
		||	(	!hnef_line_empty(gleip_lines_get_ro
						(line, (size_t)3))
			&&	HNEF_CMD_ARG_DEF ==
				gleip_lines_get_ro(line, (size_t)3)[0]))
		{
			depth	= HNEF_UI_AIMARG_DEPTH_INVALID;
		}
/*@i1@*/\
		else if	(!hnef_texttoushort(gleip_lines_get_ro
					(line, (size_t)3), & depth)
		||	!hnef_aiminimax_depth_max_valid(depth))
		{
			return	NULL == func_print_langkey
				? HNEF_FR_SUCCESS
				: func_print_langkey(ui, frx,
					HNEF_LK_CMD_PLAYER_AIM_DEPTH);
		}
		/*
		 * else: `depth` given and is valid.
		 */

#ifdef	HNEFATAFL_UI_ZHASH
		if	(gleip_lines_size(line) < (size_t)5
		||	(	!hnef_line_empty(gleip_lines_get_ro
						(line, (size_t)4))
			&&	HNEF_CMD_ARG_DEF == gleip_lines_get_ro
						(line, (size_t)4)[0]))
		{
			mem_tab	= HNEF_UI_AIMARG_MEM_INVALID;
		}
/*@i1@*/\
		else if	(!hnef_texttosize(gleip_lines_get_ro
					(line, (size_t)4), & mem_tab)
		||	!hnef_zhash_mem_tab_valid(mem_tab))
		{
			return	NULL == func_print_langkey
				? HNEF_FR_SUCCESS
				: func_print_langkey(ui, frx,
					HNEF_LK_CMD_PLAYER_AIM_MEM);
		}
		/*
		 * else: `mem_tab` given and is valid.
		 */

		if	(gleip_lines_size(line) < (size_t)6
		||	(	!hnef_line_empty(gleip_lines_get_ro
						(line, (size_t)5))
			&&	HNEF_CMD_ARG_DEF == gleip_lines_get_ro
						(line, (size_t)5)[0]))
		{
			mem_col	= HNEF_UI_AIMARG_MEM_INVALID;
		}
/*@i1@*/\
		else if	(!hnef_texttosize(gleip_lines_get_ro
					(line, (size_t)4), & mem_col)
		||	!hnef_zhash_mem_col_valid(mem_col))
		{
			return	NULL == func_print_langkey
				? HNEF_FR_SUCCESS
				: func_print_langkey(ui, frx,
					HNEF_LK_CMD_PLAYER_AIM_MEM);
		}
		/*
		 * else: `mem_col` given and is valid.
		 */
#endif	/* HNEFATAFL_UI_ZHASH */

		/*
		 * This will not allocate the `aiminimax` struct if the
		 * game is invalid. If so, it will be allocated later
		 * by `parseline_rules()`, upon reading a valid ruleset.
		 */
		return	hnef_ui_player_set(ui, player_index,
				HNEF_CONTROL_TYPE_AIM, depth
#ifdef	HNEFATAFL_UI_ZHASH
				, mem_tab, mem_col
#endif	/* HNEFATAFL_UI_ZHASH */
				);
	}
#endif	/* HNEFATAFL_UI_AIM */
	else
	{
		return	NULL == func_print_langkey
			? HNEF_FR_SUCCESS
			: func_print_langkey
				(ui, frx, HNEF_LK_CMD_PLAYER_TYPE);
	}
}

