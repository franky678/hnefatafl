/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include "langkeyt.h"

const char
* const HNEF_LK_CMD_GAME_INVALID	= "cmd_game_invalid",
* const HNEF_LK_CMD_GAME_LOAD_ID	= "cmd_game_load_id",
* const HNEF_LK_CMD_GAME_LOAD_MISS	= "cmd_game_load_miss",
* const HNEF_LK_CMD_GAME_OVER		= "cmd_game_over",
* const HNEF_LK_CMD_GAME_SAVE_MISS	= "cmd_game_save_miss",
* const HNEF_LK_CMD_GAME_UNDO_NUM	= "cmd_game_undo_num",
* const HNEF_LK_CMD_GAME_UNDO_START	= "cmd_game_undo_start",
* const HNEF_LK_CMD_LANG_MALF		= "cmd_lang_malf",
* const HNEF_LK_CMD_LANG_MISS		= "cmd_lang_miss",
* const HNEF_LK_CMD_MOVE_COORD		= "cmd_move_coord",
* const HNEF_LK_CMD_MOVE_ILLEGAL	= "cmd_move_illegal",
* const HNEF_LK_CMD_PLAYER_AIM_DEPTH	= "cmd_player_aim_depth",
* const HNEF_LK_CMD_PLAYER_AIM_MEM	= "cmd_player_aim_mem",
* const HNEF_LK_CMD_PLAYER_INDEX	= "cmd_player_index",
* const HNEF_LK_CMD_PLAYER_TYPE		= "cmd_player_type",
* const HNEF_LK_CMD_RESTRICT		= "cmd_restrict",
* const HNEF_LK_CMD_RULES_LOADHINT	= "cmd_rules_loadhint",
* const HNEF_LK_CMD_RULES_MISS		= "cmd_rules_miss",
* const HNEF_LK_CMD_SUPPRESS		= "cmd_suppress",
* const HNEF_LK_CMD_UNK			= "cmd_unk",
* const HNEF_LK_CMD_XLIB_COMP		= "cmd_xlib_comp",
* const HNEF_LK_HELP_CLI_COPY		= "help_cli_copy",
* const HNEF_LK_HELP_CLI_ENVVAR0	= "help_cli_envvar0",
* const HNEF_LK_HELP_CLI_ENVVAR1	= "help_cli_envvar1",
* const HNEF_LK_HELP_CLI_GAME_LOAD	= "help_cli_game_load",
* const HNEF_LK_HELP_CLI_GAME_NEW	= "help_cli_game_new",
* const HNEF_LK_HELP_CLI_GAME_SAVE	= "help_cli_game_save",
* const HNEF_LK_HELP_CLI_GAME_UNDO0	= "help_cli_game_undo0",
* const HNEF_LK_HELP_CLI_GAME_UNDO1	= "help_cli_game_undo1",
* const HNEF_LK_HELP_CLI_HELP0		= "help_cli_help0",
* const HNEF_LK_HELP_CLI_HELP1		= "help_cli_help1",
* const HNEF_LK_HELP_CLI_LANG		= "help_cli_lang",
* const HNEF_LK_HELP_CLI_MOVE0		= "help_cli_move0",
* const HNEF_LK_HELP_CLI_MOVE2		= "help_cli_move2",
* const HNEF_LK_HELP_CLI_MOVE4		= "help_cli_move4",
* const HNEF_LK_HELP_CLI_PATH0		= "help_cli_path0",
* const HNEF_LK_HELP_CLI_PATH1		= "help_cli_path1",
* const HNEF_LK_HELP_CLI_PLAYER0	= "help_cli_player0",
* const HNEF_LK_HELP_CLI_PLAYER1	= "help_cli_player1",
* const HNEF_LK_HELP_CLI_PLAYER2	= "help_cli_player2",
* const HNEF_LK_HELP_CLI_PLAYER2_H	= "help_cli_player2_h",
* const HNEF_LK_HELP_CLI_PLAYER2_M	= "help_cli_player2_m",
* const HNEF_LK_HELP_CLI_PLAYER2_M_X	= "help_cli_player2_m_x",
* const HNEF_LK_HELP_CLI_PLAYER2_M_Y	= "help_cli_player2_m_y",
* const HNEF_LK_HELP_CLI_PLAYER2_M_Z	= "help_cli_player2_m_z",
* const HNEF_LK_HELP_CLI_PRINT		= "help_cli_print",
* const HNEF_LK_HELP_CLI_QUIT		= "help_cli_quit",
* const HNEF_LK_HELP_CLI_RULES		= "help_cli_rules",
* const HNEF_LK_HELP_CLI_VERSION	= "help_cli_version",
* const HNEF_LK_HELP_CLI_XLIB0		= "help_cli_xlib0",
* const HNEF_LK_HELP_CLI_XLIB1		= "help_cli_xlib1",
* const HNEF_LK_HELP_XLIB_MOVE		= "help_xlib_move",
* const HNEF_LK_HELP_XLIB_PRINT		= "help_xlib_print",
* const HNEF_LK_HELP_XLIB_RULES		= "help_xlib_rules",
* const HNEF_LK_HELP_XLIB_THEME		= "help_xlib_theme",
* const HNEF_LK_HELP_XLIB_FORCE		= "help_xlib_force",
* const HNEF_LK_HELP_XLIB_XLIB		= "help_xlib_xlib",
* const HNEF_LK_XLIB_FONT_CHARSET_MISS	= "xlib_font_charset_miss",
* const HNEF_LK_XLIB_FONT_FALLBACK	= "xlib_font_fallback",
* const HNEF_LK_XLIB_FONT_FALLBACK_LOCALE = "xlib_font_fallback_locale",
* const HNEF_LK_XLIB_THEMES_NOT_FOUND	= "xlib_themes_not_found";

