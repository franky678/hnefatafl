/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <stdlib.h>	/* malloc, realloc */
#include <string.h>	/* memmove, memset, strcpy, strlen */

#include "gleipnir_line.h"

const long	GLEIP_LINE_VERSION	= 140816L;

/*@unchecked@*/
static
const char	NULM			= '\n';

/*@unchecked@*/
static
const size_t	GLEIP_LINE_LEN_DEF	= (size_t)9;

/*@unchecked@*/
static
const double	GLEIP_LINE_MEM_GROWF	= 1.5;

int
gleip_line_empty (
	const char	* const GLEIP_RSTR line
	)
{
	assert	(NULL != line);

/*@+tmpcomments@*/
/*@t4@*/\
	return	'\0' == line[0];
/*@=tmpcomments@*/
}

size_t
gleip_line_mem (
	const char	* const GLEIP_RSTR line
	)
{
	size_t	i	= 0;

	assert	(NULL != line);

	while	('\0' != line[i]
	||	'\n' != line[i + 1])
	{
		++i;
	}

	return	i + 2;
}

static
void
gleip_line_nulfill (
/*@out@*/
/*@notnull@*/
	char		* const GLEIP_RSTR line,
	const size_t	mem,
	const size_t	i
	)
/*@modifies * line@*/
{
	assert	(NULL != line);
	assert	(mem >= (size_t)2);
	assert	(i <= mem - 2);

	memset	(& line[i], (int)'\0', mem - i - 1);
	line[mem - 1]	= '\n';
}

char *
gleip_line_mem_grow (
	char		* line,
	const size_t	mem
	)
{
	size_t	mem_old;

	assert	(NULL != line);
	assert	(mem >= (size_t)2);

	mem_old	= gleip_line_mem(line);
	if	(mem_old >= mem)
	{
		return	line;
	}

	line	= realloc(line, sizeof(* line) * mem);
	if	(NULL == line)
	{
		return	NULL;
	}

	memset		(& line[mem_old - 1], (int)'\0', mem - mem_old);
	line[mem - 1]	= NULM;
	return		line;
}

int
gleip_line_mem_grow_b (
	char		* * const line,
	const size_t	mem
	)
{
	char	* tmp	= NULL;

	assert	(NULL != line);
	assert	(NULL != * line);

/*@+tmpcomments@*/
/*@t2@*/\
	tmp	= gleip_line_mem_grow(* line, mem);
/*@=tmpcomments@*/
	if	(NULL == tmp)
	{
/*@+tmpcomments@*/
/*@t2@*/\
		return	0;
/*@=tmpcomments@*/
	}
	* line	= tmp;
	return	1;
}

/*@in@*/
/*@null@*/
/*@only@*/
static
char *
gleip_line_mem_grow_auto (
/*@in@*/
/*@notnull@*/
/*@only@*/
/*@returned@*/
	char		* line,
	const size_t	mem_min
	)
/*@modifies * line@*/
{
	size_t	mem_aut;

	assert	(NULL != line);

	mem_aut	= (size_t)(gleip_line_mem(line)
		* GLEIP_LINE_MEM_GROWF + 0.000001);

	return	gleip_line_mem_grow(line,
		mem_aut < mem_min
		? mem_min
		: mem_aut);
}

char *
gleip_line_mem_trim (
	char	* line
	)
{
	size_t	len_old,
		mem_old;

	assert	(NULL != line);

	len_old	= strlen		(line);
	mem_old	= gleip_line_mem	(line);
	if	(len_old + 2 == mem_old)
	{
		return	line;
	}

	line	= realloc(line, sizeof(* line) * len_old + 2);
	if	(NULL == line)
	{
		return	NULL;
	}

	line[len_old + 1]	= NULM;
	return			line;
}

int
gleip_line_mem_trim_b (
	char	* * const line
	)
{
	char	* tmp	= NULL;

	assert	(NULL != line);
	assert	(NULL != * line);

/*@+tmpcomments@*/
/*@t2@*/\
	tmp	= gleip_line_mem_trim(* line);
/*@=tmpcomments@*/
	if	(NULL == tmp)
	{
/*@+tmpcomments@*/
/*@t2@*/\
		return	0;
/*@=tmpcomments@*/
	}
	* line	= tmp;
	return	1;
}

char *
gleip_line_add_str (
	char		* line,
	const char	* const src,
	const size_t	i
	)
{
	size_t	len_dst,
		len_src,
		mem_new;

	assert	(NULL != line);
	assert	(NULL != src);
	assert	(i <= strlen(line));

	len_src	= strlen(src);

	if	(len_src < (size_t)1)
	{
		return	line;
	}

	len_dst	= strlen(line);
	mem_new	= len_dst + len_src + 2;

	if	(gleip_line_mem(line) < mem_new)
	{
		line	= gleip_line_mem_grow_auto
			(line, len_dst + len_src + 2);
		if	(NULL == line)
		{
			return	NULL;
		}
	}

	if	(i < len_dst)
	{
		memmove	(& line[i + len_src],
			& line[i],
			len_dst - i);
	}
	memmove	(& line[i], & src[0], len_src);

	return	line;
}

int
gleip_line_add_str_b (
	char		* * const line,
	const char	* const src,
	const size_t	i
	)
{
	char	* tmp	= NULL;

	assert	(NULL != line);
	assert	(NULL != * line);
	assert	(NULL != src);
	assert	(i <= strlen(* line));

/*@+tmpcomments@*/
/*@t3@*/\
	tmp	= gleip_line_add_str(* line, src, i);
/*@=tmpcomments@*/
	if	(NULL == tmp)
	{
/*@+tmpcomments@*/
/*@t2@*/\
		return	0;
/*@=tmpcomments@*/
	}
	* line	= tmp;
	return	1;
}

char *
gleip_line_add_str_back (
	char		* line,
	const char	* const src
	)
{
	assert	(NULL != line);
	assert	(NULL != src);
	return	gleip_line_add_str(line, src, strlen(line));
}

int
gleip_line_add_str_back_b (
	char		* * const line,
	const char	* const src
	)
{
	char	* tmp	= NULL;

	assert	(NULL != line);
	assert	(NULL != * line);
	assert	(NULL != src);

/*@+tmpcomments@*/
/*@t3@*/\
	tmp	= gleip_line_add_str_back(* line, src);
/*@=tmpcomments@*/
	if	(NULL == tmp)
	{
/*@+tmpcomments@*/
/*@t2@*/\
		return	0;
/*@=tmpcomments@*/
	}
	* line	= tmp;
	return	1;
}

char *
gleip_line_add_ch (
	char		* line,
	const char	ch,
	const size_t	i
	)
{
	char	src[2];

	assert	(NULL != line);
	assert	('\0' != ch);
	assert	(i <= strlen(line));

	src[0]	= ch;
	src[1]	= '\0';

	return	gleip_line_add_str(line, src, i);
}

int
gleip_line_add_ch_b (
	char		* * const line,
	const char	ch,
	const size_t	i
	)
{
	char	* tmp	= NULL;

	assert	(NULL != line);
	assert	(NULL != * line);
	assert	('\0' != ch);
	assert	(i <= strlen(* line));

/*@+tmpcomments@*/
/*@t2@*/\
	tmp	= gleip_line_add_ch(* line, ch, i);
/*@=tmpcomments@*/
	if	(NULL == tmp)
	{
/*@+tmpcomments@*/
/*@t2@*/\
		return	0;
/*@=tmpcomments@*/
	}
	* line	= tmp;
	return	1;
}

char *
gleip_line_add_ch_back (
	char		* line,
	const char	ch
	)
{
	assert	(NULL != line);
	assert	('\0' != ch);

	return	gleip_line_add_ch(line, ch, strlen(line));
}

int
gleip_line_add_ch_back_b (
	char		* * const line,
	const char	ch
	)
{
	char	* tmp	= NULL;

	assert	(NULL != line);
	assert	(NULL != * line);
	assert	('\0' != ch);

/*@+tmpcomments@*/
/*@t2@*/\
	tmp	= gleip_line_add_ch_back(* line, ch);
/*@=tmpcomments@*/
	if	(NULL == tmp)
	{
/*@+tmpcomments@*/
/*@t2@*/\
		return	0;
/*@=tmpcomments@*/
	}
	* line	= tmp;
	return	1;
}

void
gleip_line_rm_str (
	char		* GLEIP_RSTR line,
	const size_t	i,
	const size_t	len
	)
{
	size_t	len_dst,
		shift;

	assert	(NULL != line);

	if	(len < (size_t)1)
	{
		return;
	}

	assert	(i < strlen(line));
	assert	(i + len <= strlen(line));

	len_dst	= strlen(line);
	shift	= len_dst - len - i;

	memmove	(& line[i], & line[i + len], shift);
	memset	(& line[len_dst - len], (int)'\0', len);
}

void
gleip_line_rm_str_last (
	char		* GLEIP_RSTR line,
	const size_t	len
	)
{
	assert	(NULL != line);
	assert	(len <= strlen(line));

	gleip_line_rm_str(line, strlen(line) - len, len);
}

void
gleip_line_rm_ch (
	char		* GLEIP_RSTR line,
	const size_t	i
	)
{
	assert	(NULL != line);
	assert	(i < strlen(line));

	gleip_line_rm_str(line, i, (size_t)1);
}

void
gleip_line_rm_ch_last (
	char	* GLEIP_RSTR line
	)
{
	assert	(NULL != line);
/*@+tmpcomments@*/
/*@t1@*/\
	assert	(!gleip_line_empty(line));
/*@=tmpcomments@*/

	gleip_line_rm_ch(line, strlen(line) - 1);
}

void
gleip_line_rm_from (
	char		* GLEIP_RSTR line,
	const size_t	i
	)
{
	assert	(NULL != line);
	assert	(i < strlen(line));

	gleip_line_rm_str(line, i, strlen(line) - i);
}

void
gleip_line_rm_to (
	char		* GLEIP_RSTR line,
	const size_t	len
	)
{
	assert	(NULL != line);
	assert	(len <= strlen(line));

	gleip_line_rm_str(line, 0, len);
}

void
gleip_line_rm (
	char	* GLEIP_RSTR line
	)
{
	assert	(NULL != line);

	gleip_line_rm_str(line, 0, strlen(line));
}

char *
gleip_line_cpy (
	char		* line,
	const char	* const src
	)
{
	size_t	len_dst,
		len_src,
		mem_dst;

	assert	(NULL != line);
	assert	(NULL != src);

	len_dst	= strlen(line);
	len_src	= strlen(src);
	if	(len_src < (size_t)1)
	{
		memset(& line[0], (int)'\0', len_dst);
		return	line;
	}

	mem_dst	= gleip_line_mem(line);
	if	(len_src + 2 > mem_dst)
	{
		line	= gleip_line_mem_grow_auto(line, len_src + 2);
		if	(NULL == line)
		{
			return	NULL;
		}
	}

	memmove	(& line[0], & src[0], len_src);
	if	(len_dst > len_src)
	{
		memset	(& line[len_src], (int)'\0', len_dst - len_src);
	}
	return	line;
}

int
gleip_line_cpy_b (
	char		* * const line,
	const char	* const src
	)
{
	char	* tmp	= NULL;

	assert	(NULL != line);
	assert	(NULL != * line);
	assert	(NULL != src);

/*@+tmpcomments@*/
/*@t3@*/\
	tmp	= gleip_line_cpy(* line, src);
/*@=tmpcomments@*/
	if	(NULL == tmp)
	{
/*@+tmpcomments@*/
/*@t2@*/\
		return	0;
/*@=tmpcomments@*/
	}
	* line	= tmp;
	return	1;
}

char *
gleip_line_alloc_mem (
	const size_t	mem
	)
{
	char	* GLEIP_RSTR line	= NULL;

	assert	(mem >= (size_t)2);

	line	= malloc(sizeof(* line) * mem);
	if	(NULL == line)
	{
		return	NULL;
	}

	gleip_line_nulfill(line, mem, 0);

	return	line;
}

char *
gleip_line_alloc_len (
	const size_t	len
	)
{
	return	gleip_line_alloc_mem(len + 2);
}

char *
gleip_line_alloc (void)
{
	return	gleip_line_alloc_len(GLEIP_LINE_LEN_DEF);
}

char *
gleip_line_alloc_cpy (
	const char	* const src
	)
{
	size_t	len;
	char	* GLEIP_RSTR dst	= NULL;

	assert	(NULL != src);

	len	= strlen(src);
	dst	= malloc(sizeof(* dst) * len + 2);
	if	(NULL == dst)
	{
		return	NULL;
	}

	strcpy		(dst, src);
	dst[len + 1]	= NULM;
	return		dst;
}

