/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <stdio.h>		/* puts */
#include <stdlib.h>		/* EXIT_* */
#include <string.h>		/* strcmp */

#include "test_line.h"		/* test_line */
#include "test_lines.h"		/* test_lines */
#include "test_lineread.h"	/* test_lineread */
#include "test_lang.h"		/* test_lang */

/*@+boolint@*/

int
main (
	const int	argc,
	const char	* const * const argv
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int	verbose	= 0,
		i;

	for	(i = 1; i < argc; ++i)
	{
		const char	* const arg	= argv[i];
		if	(0 == strcmp("-v", arg))
		{
			verbose	= 1;
		}
	}

	if	(test_line	(verbose)
	&&	test_lines	(verbose)
	&&	test_lineread	(verbose)
	&&	test_lang	(verbose))
	{
		(void)puts("SUCCESS");
		return	EXIT_SUCCESS;
	}
	else
	{
		(void)fputs("FAILURE\n", stderr);
		return	EXIT_FAILURE;
	}
}

/*@=boolint@*/

