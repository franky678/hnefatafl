/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>		/* assert */
#include <stdio.h>		/* puts */
#include <stdlib.h>		/* free */

#include "gleipnir_lines.h"	/* gleip_lines_* */

#include "messages.h"		/* print_* */
#include "test_lines.h"

/*@+boolint@*/

/*
 * -	`gleip_lines_shift`
 */
static
int
test_lines_shift (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int			retval	= 1;
	struct gleip_lines	* lines	= NULL;

	if	(verbose)
	{
		(void)puts("test_lines_shift() INIT");
	}

	lines	= gleip_lines_alloc();
	if	(NULL == lines)
	{
		print_fail_alloc();
		return	0;
	}

	if	(!check_lines_integrity(lines, 0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity(lines, 0)
		||	!check_lines_trimmed(lines))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(!gleip_lines_addcpy_back(lines, "A")
	||	!gleip_lines_addcpy_back(lines, "B")
	||	!gleip_lines_addcpy_back(lines, "C")
	||	!gleip_lines_addcpy_back(lines, "D")
	||	!gleip_lines_addcpy_back(lines, "X")
	||	!gleip_lines_addcpy_back(lines, "Y"))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)6)
	||	!check_lines_str	(lines, (size_t)0, "A")
	||	!check_lines_str	(lines, (size_t)1, "B")
	||	!check_lines_str	(lines, (size_t)2, "C")
	||	!check_lines_str	(lines, (size_t)3, "D")
	||	!check_lines_str	(lines, (size_t)4, "X")
	||	!check_lines_str	(lines, (size_t)5, "Y"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)6)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "A")
		||	!check_lines_str	(lines, (size_t)1, "B")
		||	!check_lines_str	(lines, (size_t)2, "C")
		||	!check_lines_str	(lines, (size_t)3, "D")
		||	!check_lines_str	(lines, (size_t)4, "X")
		||	!check_lines_str	(lines, (size_t)5, "Y"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	gleip_lines_shift(lines, (size_t)0, (size_t)4, (size_t)2);
	if	(!check_lines_integrity	(lines, (size_t)6)
	||	!check_lines_str	(lines, (size_t)0, "X")
	||	!check_lines_str	(lines, (size_t)1, "Y")
	||	!check_lines_str	(lines, (size_t)2, "A")
	||	!check_lines_str	(lines, (size_t)3, "B")
	||	!check_lines_str	(lines, (size_t)4, "C")
	||	!check_lines_str	(lines, (size_t)5, "D"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)6)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "X")
		||	!check_lines_str	(lines, (size_t)1, "Y")
		||	!check_lines_str	(lines, (size_t)2, "A")
		||	!check_lines_str	(lines, (size_t)3, "B")
		||	!check_lines_str	(lines, (size_t)4, "C")
		||	!check_lines_str	(lines, (size_t)5, "D"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	gleip_lines_shift(lines, (size_t)0, (size_t)3, (size_t)3);
	if	(!check_lines_integrity	(lines, (size_t)6)
	||	!check_lines_str	(lines, (size_t)0, "B")
	||	!check_lines_str	(lines, (size_t)1, "C")
	||	!check_lines_str	(lines, (size_t)2, "D")
	||	!check_lines_str	(lines, (size_t)3, "X")
	||	!check_lines_str	(lines, (size_t)4, "Y")
	||	!check_lines_str	(lines, (size_t)5, "A"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)6)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "B")
		||	!check_lines_str	(lines, (size_t)1, "C")
		||	!check_lines_str	(lines, (size_t)2, "D")
		||	!check_lines_str	(lines, (size_t)3, "X")
		||	!check_lines_str	(lines, (size_t)4, "Y")
		||	!check_lines_str	(lines, (size_t)5, "A"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	gleip_lines_shift(lines, (size_t)5, (size_t)0, (size_t)5);
	if	(!check_lines_integrity	(lines, (size_t)6)
	||	!check_lines_str	(lines, (size_t)0, "A")
	||	!check_lines_str	(lines, (size_t)1, "B")
	||	!check_lines_str	(lines, (size_t)2, "C")
	||	!check_lines_str	(lines, (size_t)3, "D")
	||	!check_lines_str	(lines, (size_t)4, "X")
	||	!check_lines_str	(lines, (size_t)5, "Y"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)6)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "A")
		||	!check_lines_str	(lines, (size_t)1, "B")
		||	!check_lines_str	(lines, (size_t)2, "C")
		||	!check_lines_str	(lines, (size_t)3, "D")
		||	!check_lines_str	(lines, (size_t)4, "X")
		||	!check_lines_str	(lines, (size_t)5, "Y"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	gleip_lines_shift(lines, (size_t)5, (size_t)1, (size_t)2);
	if	(!check_lines_integrity	(lines, (size_t)6)
	||	!check_lines_str	(lines, (size_t)0, "A")
	||	!check_lines_str	(lines, (size_t)1, "D")
	||	!check_lines_str	(lines, (size_t)2, "X")
	||	!check_lines_str	(lines, (size_t)3, "Y")
	||	!check_lines_str	(lines, (size_t)4, "B")
	||	!check_lines_str	(lines, (size_t)5, "C"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)6)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "A")
		||	!check_lines_str	(lines, (size_t)1, "D")
		||	!check_lines_str	(lines, (size_t)2, "X")
		||	!check_lines_str	(lines, (size_t)3, "Y")
		||	!check_lines_str	(lines, (size_t)4, "B")
		||	!check_lines_str	(lines, (size_t)5, "C"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	gleip_lines_shift(lines, (size_t)3, (size_t)0, (size_t)1);
	if	(!check_lines_integrity	(lines, (size_t)6)
	||	!check_lines_str	(lines, (size_t)0, "D")
	||	!check_lines_str	(lines, (size_t)1, "X")
	||	!check_lines_str	(lines, (size_t)2, "Y")
	||	!check_lines_str	(lines, (size_t)3, "A")
	||	!check_lines_str	(lines, (size_t)4, "B")
	||	!check_lines_str	(lines, (size_t)5, "C"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)6)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "D")
		||	!check_lines_str	(lines, (size_t)1, "X")
		||	!check_lines_str	(lines, (size_t)2, "Y")
		||	!check_lines_str	(lines, (size_t)3, "A")
		||	!check_lines_str	(lines, (size_t)4, "B")
		||	!check_lines_str	(lines, (size_t)5, "C"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	gleip_lines_shift(lines, (size_t)5, (size_t)0, (size_t)1);
	if	(!check_lines_integrity	(lines, (size_t)6)
	||	!check_lines_str	(lines, (size_t)0, "X")
	||	!check_lines_str	(lines, (size_t)1, "Y")
	||	!check_lines_str	(lines, (size_t)2, "A")
	||	!check_lines_str	(lines, (size_t)3, "B")
	||	!check_lines_str	(lines, (size_t)4, "C")
	||	!check_lines_str	(lines, (size_t)5, "D"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)6)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "X")
		||	!check_lines_str	(lines, (size_t)1, "Y")
		||	!check_lines_str	(lines, (size_t)2, "A")
		||	!check_lines_str	(lines, (size_t)3, "B")
		||	!check_lines_str	(lines, (size_t)4, "C")
		||	!check_lines_str	(lines, (size_t)5, "D"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	gleip_lines_shift(lines, (size_t)0, (size_t)2, (size_t)4);
	if	(!check_lines_integrity	(lines, (size_t)6)
	||	!check_lines_str	(lines, (size_t)0, "A")
	||	!check_lines_str	(lines, (size_t)1, "B")
	||	!check_lines_str	(lines, (size_t)2, "C")
	||	!check_lines_str	(lines, (size_t)3, "D")
	||	!check_lines_str	(lines, (size_t)4, "X")
	||	!check_lines_str	(lines, (size_t)5, "Y"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)6)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "A")
		||	!check_lines_str	(lines, (size_t)1, "B")
		||	!check_lines_str	(lines, (size_t)2, "C")
		||	!check_lines_str	(lines, (size_t)3, "D")
		||	!check_lines_str	(lines, (size_t)4, "X")
		||	!check_lines_str	(lines, (size_t)5, "Y"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(!gleip_lines_addcpy_back(lines, "Z"))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)7)
	||	!check_lines_str	(lines, (size_t)0, "A")
	||	!check_lines_str	(lines, (size_t)1, "B")
	||	!check_lines_str	(lines, (size_t)2, "C")
	||	!check_lines_str	(lines, (size_t)3, "D")
	||	!check_lines_str	(lines, (size_t)4, "X")
	||	!check_lines_str	(lines, (size_t)5, "Y")
	||	!check_lines_str	(lines, (size_t)6, "Z"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)7)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "A")
		||	!check_lines_str	(lines, (size_t)1, "B")
		||	!check_lines_str	(lines, (size_t)2, "C")
		||	!check_lines_str	(lines, (size_t)3, "D")
		||	!check_lines_str	(lines, (size_t)4, "X")
		||	!check_lines_str	(lines, (size_t)5, "Y")
		||	!check_lines_str	(lines, (size_t)6, "Z"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	gleip_lines_shift(lines, (size_t)6, (size_t)0, (size_t)6);
	if	(!check_lines_integrity	(lines, (size_t)7)
	||	!check_lines_str	(lines, (size_t)0, "Z")
	||	!check_lines_str	(lines, (size_t)1, "A")
	||	!check_lines_str	(lines, (size_t)2, "B")
	||	!check_lines_str	(lines, (size_t)3, "C")
	||	!check_lines_str	(lines, (size_t)4, "D")
	||	!check_lines_str	(lines, (size_t)5, "X")
	||	!check_lines_str	(lines, (size_t)6, "Y"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)7)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "Z")
		||	!check_lines_str	(lines, (size_t)1, "A")
		||	!check_lines_str	(lines, (size_t)2, "B")
		||	!check_lines_str	(lines, (size_t)3, "C")
		||	!check_lines_str	(lines, (size_t)4, "D")
		||	!check_lines_str	(lines, (size_t)5, "X")
		||	!check_lines_str	(lines, (size_t)6, "Y"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	gleip_lines_shift(lines, (size_t)0, (size_t)1, (size_t)6);
	if	(!check_lines_integrity	(lines, (size_t)7)
	||	!check_lines_str	(lines, (size_t)0, "A")
	||	!check_lines_str	(lines, (size_t)1, "B")
	||	!check_lines_str	(lines, (size_t)2, "C")
	||	!check_lines_str	(lines, (size_t)3, "D")
	||	!check_lines_str	(lines, (size_t)4, "X")
	||	!check_lines_str	(lines, (size_t)5, "Y")
	||	!check_lines_str	(lines, (size_t)6, "Z"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)7)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "A")
		||	!check_lines_str	(lines, (size_t)1, "B")
		||	!check_lines_str	(lines, (size_t)2, "C")
		||	!check_lines_str	(lines, (size_t)3, "D")
		||	!check_lines_str	(lines, (size_t)4, "X")
		||	!check_lines_str	(lines, (size_t)5, "Y")
		||	!check_lines_str	(lines, (size_t)6, "Z"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	gleip_lines_rm_range_back(lines, (size_t)5);

	if	(!check_lines_integrity	(lines, (size_t)2)
	||	!check_lines_str	(lines, (size_t)0, "A")
	||	!check_lines_str	(lines, (size_t)1, "B"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)2)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "A")
		||	!check_lines_str	(lines, (size_t)1, "B"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	gleip_lines_shift(lines, (size_t)0, (size_t)1, (size_t)1);
	if	(!check_lines_integrity	(lines, (size_t)2)
	||	!check_lines_str	(lines, (size_t)0, "B")
	||	!check_lines_str	(lines, (size_t)1, "A"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)2)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "B")
		||	!check_lines_str	(lines, (size_t)1, "A"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	gleip_lines_shift(lines, (size_t)1, (size_t)0, (size_t)1);
	if	(!check_lines_integrity	(lines, (size_t)2)
	||	!check_lines_str	(lines, (size_t)0, "A")
	||	!check_lines_str	(lines, (size_t)1, "B"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)2)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "A")
		||	!check_lines_str	(lines, (size_t)1, "B"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_lines_shift() FINI");
	}
	gleip_lines_free(lines);
	return	retval;
}

/*
 * -	`gleip_lines_new*`
 */
static
int
test_lines_new (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int			retval	= 1;
	struct gleip_lines	* lines	= NULL;

	if	(verbose)
	{
		(void)puts("test_lines_new() INIT");
	}

	lines	= gleip_lines_alloc();
	if	(NULL == lines)
	{
		print_fail_alloc();
		return	0;
	}

	if	(!check_lines_integrity(lines, 0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity(lines, 0)
		||	!check_lines_trimmed(lines))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(verbose)
	{
		(void)puts("\tadding line at back");
	}
	if	(!gleip_lines_new_line_back(lines))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)1)
	||	!check_lines_str	(lines, 0, ""))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)1)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, 0, ""))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(verbose)
	{
		(void)puts("\tadding line at back");
	}
	if	(!gleip_lines_new_line_back(lines))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)2)
	||	!check_lines_str	(lines, (size_t)0, "")
	||	!check_lines_str	(lines, (size_t)1, ""))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)2)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "")
		||	!check_lines_str	(lines, (size_t)1, ""))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(verbose)
	{
		(void)puts("\tadding line at index 0");
	}
	if	(!gleip_lines_new_line(lines, 0))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)3)
	||	!check_lines_str	(lines, (size_t)0, "")
	||	!check_lines_str	(lines, (size_t)1, "")
	||	!check_lines_str	(lines, (size_t)2, ""))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)3)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "")
		||	!check_lines_str	(lines, (size_t)1, "")
		||	!check_lines_str	(lines, (size_t)2, ""))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(verbose)
	{
		(void)puts("\tremoving i=0 count=2");
	}
	gleip_lines_rm_range(lines, 0, (size_t)2);

	if	(!check_lines_integrity	(lines, (size_t)1)
	||	!check_lines_str	(lines, (size_t)0, ""))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)1)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, ""))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	gleip_lines_rm(lines);
	if	(!gleip_lines_addcpy_back(lines, "A")
	||	!gleip_lines_addcpy_back(lines, "B")
	||	!gleip_lines_addcpy_back(lines, "C")
	||	!gleip_lines_addcpy_back(lines, "D"))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)4)
	||	!check_lines_str	(lines, (size_t)0, "A")
	||	!check_lines_str	(lines, (size_t)1, "B")
	||	!check_lines_str	(lines, (size_t)2, "C")
	||	!check_lines_str	(lines, (size_t)3, "D"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)4)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "A")
		||	!check_lines_str	(lines, (size_t)1, "B")
		||	!check_lines_str	(lines, (size_t)2, "C")
		||	!check_lines_str	(lines, (size_t)3, "D"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(!gleip_lines_new(lines, (size_t)2, (size_t)3))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)7)
	||	!check_lines_str	(lines, (size_t)0, "A")
	||	!check_lines_str	(lines, (size_t)1, "B")
	||	!check_lines_str	(lines, (size_t)2, "")
	||	!check_lines_str	(lines, (size_t)3, "")
	||	!check_lines_str	(lines, (size_t)4, "")
	||	!check_lines_str	(lines, (size_t)5, "C")
	||	!check_lines_str	(lines, (size_t)6, "D"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)7)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "A")
		||	!check_lines_str	(lines, (size_t)1, "B")
		||	!check_lines_str	(lines, (size_t)2, "")
		||	!check_lines_str	(lines, (size_t)3, "")
		||	!check_lines_str	(lines, (size_t)4, "")
		||	!check_lines_str	(lines, (size_t)5, "C")
		||	!check_lines_str	(lines, (size_t)6, "D"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(!gleip_lines_new_back(lines, (size_t)3))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)10)
	||	!check_lines_str	(lines, (size_t)0, "A")
	||	!check_lines_str	(lines, (size_t)1, "B")
	||	!check_lines_str	(lines, (size_t)2, "")
	||	!check_lines_str	(lines, (size_t)3, "")
	||	!check_lines_str	(lines, (size_t)4, "")
	||	!check_lines_str	(lines, (size_t)5, "C")
	||	!check_lines_str	(lines, (size_t)6, "D")
	||	!check_lines_str	(lines, (size_t)7, "")
	||	!check_lines_str	(lines, (size_t)8, "")
	||	!check_lines_str	(lines, (size_t)9, ""))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)10)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "A")
		||	!check_lines_str	(lines, (size_t)1, "B")
		||	!check_lines_str	(lines, (size_t)2, "")
		||	!check_lines_str	(lines, (size_t)3, "")
		||	!check_lines_str	(lines, (size_t)4, "")
		||	!check_lines_str	(lines, (size_t)5, "C")
		||	!check_lines_str	(lines, (size_t)6, "D")
		||	!check_lines_str	(lines, (size_t)7, "")
		||	!check_lines_str	(lines, (size_t)8, "")
		||	!check_lines_str	(lines, (size_t)9, ""))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(!gleip_lines_new(lines, 0, (size_t)3))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)13)
	||	!check_lines_str	(lines, (size_t)0, "")
	||	!check_lines_str	(lines, (size_t)1, "")
	||	!check_lines_str	(lines, (size_t)2, "")
	||	!check_lines_str	(lines, (size_t)3, "A")
	||	!check_lines_str	(lines, (size_t)4, "B")
	||	!check_lines_str	(lines, (size_t)5, "")
	||	!check_lines_str	(lines, (size_t)6, "")
	||	!check_lines_str	(lines, (size_t)7, "")
	||	!check_lines_str	(lines, (size_t)8, "C")
	||	!check_lines_str	(lines, (size_t)9, "D")
	||	!check_lines_str	(lines, (size_t)10, "")
	||	!check_lines_str	(lines, (size_t)11, "")
	||	!check_lines_str	(lines, (size_t)12, ""))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)13)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0, "")
		||	!check_lines_str	(lines, (size_t)1, "")
		||	!check_lines_str	(lines, (size_t)2, "")
		||	!check_lines_str	(lines, (size_t)3, "A")
		||	!check_lines_str	(lines, (size_t)4, "B")
		||	!check_lines_str	(lines, (size_t)5, "")
		||	!check_lines_str	(lines, (size_t)6, "")
		||	!check_lines_str	(lines, (size_t)7, "")
		||	!check_lines_str	(lines, (size_t)8, "C")
		||	!check_lines_str	(lines, (size_t)9, "D")
		||	!check_lines_str	(lines, (size_t)10, "")
		||	!check_lines_str	(lines, (size_t)11, "")
		||	!check_lines_str	(lines, (size_t)12, ""))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_lines_new() FINI");
	}
	gleip_lines_free(lines);
	return	retval;
}

/*
 * -	`gleip_lines_addref*`
 */
static
int
test_lines_addref (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int			retval	= 1;
	struct gleip_lines	* lines	= NULL;
/*@only@*/
	char			* tmp	= NULL;

	if	(verbose)
	{
		(void)puts("test_lines_addref() INIT");
	}

	lines	= gleip_lines_alloc();
	if	(NULL == lines)
	{
		print_fail_alloc();
		return	0;
	}

	if	(!check_lines_integrity	(lines, (size_t)0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)0)
		||	!check_lines_trimmed	(lines))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	tmp	= gleip_line_alloc_cpy("Second (#1)");
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		(void)puts("\tadding ref at back");
	}
	if	(!gleip_lines_addref_back(lines, tmp))
	{
		print_fail_alloc();
		retval	= 0;
/*@i1@*/\
		free	(tmp);
		tmp	= NULL;
		goto	RETURN_VAL;
	}
	tmp	= NULL;

	if	(!check_lines_integrity	(lines, (size_t)1)
	||	!check_lines_str	(lines, (size_t)0,
						"Second (#1)"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)1)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0,
							"Second (#1)"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	tmp	= gleip_line_alloc_cpy("First (#2)");
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		(void)puts("\tadding ref at 0");
	}
	if	(!gleip_lines_addref(lines, tmp, 0))
	{
		print_fail_alloc();
		retval	= 0;
/*@i1@*/\
		free	(tmp);
		tmp	= NULL;
		goto	RETURN_VAL;
	}
	tmp	= NULL;

	if	(!check_lines_integrity	(lines, (size_t)2)
	||	!check_lines_str	(lines, (size_t)0,
						"First (#2)")
	||	!check_lines_str	(lines, (size_t)1,
						"Second (#1)"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)2)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0,
							"First (#2)")
		||	!check_lines_str	(lines, (size_t)1,
							"Second (#1)"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	tmp	= gleip_line_alloc_cpy("1.5th (#3)");
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		(void)puts("\tadding ref at 1");
	}
	if	(!gleip_lines_addref(lines, tmp, (size_t)1))
	{
		print_fail_alloc();
		retval	= 0;
/*@i1@*/\
		free	(tmp);
		tmp	= NULL;
		goto	RETURN_VAL;
	}
	tmp	= NULL;

	if	(!check_lines_integrity	(lines, (size_t)3)
	||	!check_lines_str	(lines, (size_t)0,
						"First (#2)")
	||	!check_lines_str	(lines, (size_t)1,
						"1.5th (#3)")
	||	!check_lines_str	(lines, (size_t)2,
						"Second (#1)"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)3)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0,
							"First (#2)")
		||	!check_lines_str	(lines, (size_t)1,
							"1.5th (#3)")
		||	!check_lines_str	(lines, (size_t)2,
							"Second (#1)"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(verbose)
	{
		(void)puts("\tnew at back (for later garbage)");
	}
	if	(!gleip_lines_new_line_back(lines))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)4)
	||	!check_lines_str	(lines, (size_t)0,
						"First (#2)")
	||	!check_lines_str	(lines, (size_t)1,
						"1.5th (#3)")
	||	!check_lines_str	(lines, (size_t)2,
						"Second (#1)")
	||	!check_lines_str	(lines, (size_t)3,
						""))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(verbose)
	{
		(void)puts(
			"\tremoving i=2 count=2 (to create garbage)");
	}
	gleip_lines_rm_range(lines, (size_t)2, (size_t)2);

	if	(!check_lines_integrity	(lines, (size_t)2)
	||	!check_lines_str	(lines, (size_t)0,
						"First (#2)")
	||	!check_lines_str	(lines, (size_t)1,
						"1.5th (#3)"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	tmp	= gleip_line_alloc_cpy("1.25th (#4)");
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		(void)puts("\tadding ref at 1");
	}
	if	(!gleip_lines_addref(lines, tmp, (size_t)1))
	{
		print_fail_alloc();
		retval	= 0;
/*@i1@*/\
		free	(tmp);
		tmp	= NULL;
		goto	RETURN_VAL;
	}
	tmp	= NULL;

	if	(!check_lines_integrity	(lines, (size_t)3)
	||	!check_lines_str	(lines, (size_t)0,
						"First (#2)")
	||	!check_lines_str	(lines, (size_t)1,
						"1.25th (#4)")
	||	!check_lines_str	(lines, (size_t)2,
						"1.5th (#3)"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	tmp	= gleip_line_alloc_cpy("1.75th (#5)");
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		(void)puts("\tadding ref at 3");
	}
	if	(!gleip_lines_addref(lines, tmp, (size_t)3))
	{
		print_fail_alloc();
		retval	= 0;
/*@i1@*/\
		free	(tmp);
		tmp	= NULL;
		goto	RETURN_VAL;
	}
	tmp	= NULL;

	if	(!check_lines_integrity	(lines, (size_t)4)
	||	!check_lines_str	(lines, (size_t)0,
						"First (#2)")
	||	!check_lines_str	(lines, (size_t)1,
						"1.25th (#4)")
	||	!check_lines_str	(lines, (size_t)2,
						"1.5th (#3)")
	||	!check_lines_str	(lines, (size_t)3,
						"1.75th (#5)"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_lines_addref() FINI");
	}
	gleip_lines_free(lines);
	return	retval;
}

/*
 * -	`gleip_lines_addcpy*`
 */
static
int
test_lines_addcpy (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int			retval	= 1;
	struct gleip_lines	* lines	= NULL;

	if	(verbose)
	{
		(void)puts("test_lines_addcpy() INIT");
	}

	lines	= gleip_lines_alloc();
	if	(NULL == lines)
	{
		print_fail_alloc();
		return	0;
	}

	if	(!check_lines_integrity	(lines, (size_t)0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)0)
		||	!check_lines_trimmed	(lines))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(verbose)
	{
		(void)puts("\tadding cpy at back (1, short, garbage)");
	}
	if	(!gleip_lines_addcpy_back(lines, "Grb 1"))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)1)
	||	!check_lines_str	(lines, (size_t)0,
						"Grb 1"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)1)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0,
							"Grb 1"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(verbose)
	{
		(void)puts("\tadding cpy at back (2, long, garbage)");
	}
	if	(!gleip_lines_addcpy_back(lines, "Garbage 2 (long)"))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)2)
	||	!check_lines_str	(lines, (size_t)0,
						"Grb 1")
	||	!check_lines_str	(lines, (size_t)1,
						"Garbage 2 (long)"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)2)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0,
							"Grb 1")
		||	!check_lines_str	(lines, (size_t)1,
						"Garbage 2 (long)"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(verbose)
	{
		(void)puts(
			"\tadding cpy at back (3, average, garbage)");
	}
	if	(!gleip_lines_addcpy_back(lines, "Garbage 3"))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)3)
	||	!check_lines_str	(lines, (size_t)0,
						"Grb 1")
	||	!check_lines_str	(lines, (size_t)1,
						"Garbage 2 (long)")
	||	!check_lines_str	(lines, (size_t)2,
						"Garbage 3"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)3)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0,
							"Grb 1")
		||	!check_lines_str	(lines, (size_t)1,
						"Garbage 2 (long)")
		||	!check_lines_str	(lines, (size_t)2,
							"Garbage 3"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	if	(verbose)
	{
		(void)puts("\tremoving all (to create garbage)");
	}
	gleip_lines_rm(lines);

	if	(!check_lines_integrity	(lines, (size_t)0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(verbose)
	{
		(void)puts
		("\tadding cpy at back (1) -- match long(2) >mem_pref");
	}
	if	(!gleip_lines_addcpy_back(lines, "Line 1 ......"))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)1)
	||	!check_lines_str	(lines, (size_t)0,
						"Line 1 ......"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(verbose)
	{
		(void)puts
		("\tadding cpy at back (3) -- match avg(3) <mem_pref");
	}
	if	(!gleip_lines_addcpy_back(lines, "Line 3 ......"))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)2)
	||	!check_lines_str	(lines, (size_t)0,
						"Line 1 ......")
	||	!check_lines_str	(lines, (size_t)1,
						"Line 3 ......"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(verbose)
	{
		(void)puts("\tadding cpy at index=1 (2)");
	}
	if	(!gleip_lines_addcpy(lines, "Line 2", (size_t)1))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)3)
	||	!check_lines_str	(lines, (size_t)0,
						"Line 1 ......")
	||	!check_lines_str	(lines, (size_t)1,
						"Line 2")
	||	!check_lines_str	(lines, (size_t)2,
						"Line 3 ......"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(verbose)
	{
		(void)puts("\tadding cpy at index=0 (0)");
	}
	if	(!gleip_lines_addcpy(lines, "Line 0", 0))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(lines, (size_t)4)
	||	!check_lines_str	(lines, (size_t)0,
						"Line 0")
	||	!check_lines_str	(lines, (size_t)1,
						"Line 1 ......")
	||	!check_lines_str	(lines, (size_t)2,
						"Line 2")
	||	!check_lines_str	(lines, (size_t)3,
						"Line 3 ......"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)4)
		||	!check_lines_trimmed	(lines)
		||	!check_lines_str	(lines, (size_t)0,
							"Line 0")
		||	!check_lines_str	(lines, (size_t)1,
							"Line 1 ......")
		||	!check_lines_str	(lines, (size_t)2,
							"Line 2")
		||	!check_lines_str	(lines, (size_t)3,
						"Line 3 ......"))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_lines_addcpy() FINI");
	}
	gleip_lines_free(lines);
	return	retval;
}

/*
 * -	`gleip_lines_mv*`
 * -	`gleip_lines_alloc_mv*`
 * -	`gleip_lines_rm*`
 */
static
int
test_lines_mv_rm (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int			retval	= 1;
	struct gleip_lines	* dst	= NULL,
				* src	= NULL;

	if	(verbose)
	{
		(void)puts("test_lines_mv_rm() INIT");
	}

	dst	= gleip_lines_alloc();
	if	(NULL == dst)
	{
		print_fail_alloc();
		return	0;
	}

	src	= gleip_lines_alloc();
	if	(NULL == src)
	{
		gleip_lines_free(dst);
		print_fail_alloc();
		return	0;
	}

	if	(!check_lines_integrity	(dst, (size_t)0)
	||	!check_lines_integrity	(src, (size_t)0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(dst, (size_t)0)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)0)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if	(!gleip_lines_addcpy_back	(dst, "A")
	||	!gleip_lines_addcpy_back	(dst, "B")
	||	!gleip_lines_addcpy_back	(dst, "C")
	||	!gleip_lines_addcpy_back	(dst, "D")
	||	!gleip_lines_addcpy_back	(dst, "E")
	||	!gleip_lines_addcpy_back	(dst, "F")
	||	!gleip_lines_addcpy_back	(dst, "G")
	||	!gleip_lines_addcpy_back	(src, "1")
	||	!gleip_lines_addcpy_back	(src, "2")
	||	!gleip_lines_addcpy_back	(src, "3")
	||	!gleip_lines_addcpy_back	(src, "X")
	||	!gleip_lines_addcpy_back	(src, "Y")
	||	!gleip_lines_addcpy_back	(src, "Z")
	||	!gleip_lines_addcpy_back	(src, "4")
	||	!gleip_lines_addcpy_back	(src, "5")
	||	!gleip_lines_addcpy_back	(src, "6"))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)7)
	||	!check_lines_str	(dst, (size_t)0, "A")
	||	!check_lines_str	(dst, (size_t)1, "B")
	||	!check_lines_str	(dst, (size_t)2, "C")
	||	!check_lines_str	(dst, (size_t)3, "D")
	||	!check_lines_str	(dst, (size_t)4, "E")
	||	!check_lines_str	(dst, (size_t)5, "F")
	||	!check_lines_str	(dst, (size_t)6, "G")
	||	!check_lines_integrity	(src, (size_t)9)
	||	!check_lines_str	(src, (size_t)0, "1")
	||	!check_lines_str	(src, (size_t)1, "2")
	||	!check_lines_str	(src, (size_t)2, "3")
	||	!check_lines_str	(src, (size_t)3, "X")
	||	!check_lines_str	(src, (size_t)4, "Y")
	||	!check_lines_str	(src, (size_t)5, "Z")
	||	!check_lines_str	(src, (size_t)6, "4")
	||	!check_lines_str	(src, (size_t)7, "5")
	||	!check_lines_str	(src, (size_t)8, "6"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(!gleip_lines_mv_range
			(dst, src, (size_t)3, (size_t)3, (size_t)3))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)10)
	||	!check_lines_str	(dst, (size_t)0, "A")
	||	!check_lines_str	(dst, (size_t)1, "B")
	||	!check_lines_str	(dst, (size_t)2, "C")
	||	!check_lines_str	(dst, (size_t)3, "X")
	||	!check_lines_str	(dst, (size_t)4, "Y")
	||	!check_lines_str	(dst, (size_t)5, "Z")
	||	!check_lines_str	(dst, (size_t)6, "D")
	||	!check_lines_str	(dst, (size_t)7, "E")
	||	!check_lines_str	(dst, (size_t)8, "F")
	||	!check_lines_str	(dst, (size_t)9, "G")
	||	!check_lines_integrity	(src, (size_t)6)
	||	!check_lines_str	(src, (size_t)0, "1")
	||	!check_lines_str	(src, (size_t)1, "2")
	||	!check_lines_str	(src, (size_t)2, "3")
	||	!check_lines_str	(src, (size_t)3, "4")
	||	!check_lines_str	(src, (size_t)4, "5")
	||	!check_lines_str	(src, (size_t)5, "6"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)10)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)6)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if	(!gleip_lines_mv_range
			(dst, src, (size_t)0, (size_t)0, (size_t)1))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)11)
	||	!check_lines_str	(dst, (size_t)0, "1")
	||	!check_lines_str	(dst, (size_t)1, "A")
	||	!check_lines_str	(dst, (size_t)2, "B")
	||	!check_lines_str	(dst, (size_t)3, "C")
	||	!check_lines_str	(dst, (size_t)4, "X")
	||	!check_lines_str	(dst, (size_t)5, "Y")
	||	!check_lines_str	(dst, (size_t)6, "Z")
	||	!check_lines_str	(dst, (size_t)7, "D")
	||	!check_lines_str	(dst, (size_t)8, "E")
	||	!check_lines_str	(dst, (size_t)9, "F")
	||	!check_lines_str	(dst, (size_t)10, "G")
	||	!check_lines_integrity	(src, (size_t)5)
	||	!check_lines_str	(src, (size_t)0, "2")
	||	!check_lines_str	(src, (size_t)1, "3")
	||	!check_lines_str	(src, (size_t)2, "4")
	||	!check_lines_str	(src, (size_t)3, "5")
	||	!check_lines_str	(src, (size_t)4, "6"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)11)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)5)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if	(!gleip_lines_mv_range
			(dst, src, (size_t)10, (size_t)0, (size_t)3))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)14)
	||	!check_lines_str	(dst, (size_t)0, "1")
	||	!check_lines_str	(dst, (size_t)1, "A")
	||	!check_lines_str	(dst, (size_t)2, "B")
	||	!check_lines_str	(dst, (size_t)3, "C")
	||	!check_lines_str	(dst, (size_t)4, "X")
	||	!check_lines_str	(dst, (size_t)5, "Y")
	||	!check_lines_str	(dst, (size_t)6, "Z")
	||	!check_lines_str	(dst, (size_t)7, "D")
	||	!check_lines_str	(dst, (size_t)8, "E")
	||	!check_lines_str	(dst, (size_t)9, "F")
	||	!check_lines_str	(dst, (size_t)10, "2")
	||	!check_lines_str	(dst, (size_t)11, "3")
	||	!check_lines_str	(dst, (size_t)12, "4")
	||	!check_lines_str	(dst, (size_t)13, "G")
	||	!check_lines_integrity	(src, (size_t)2)
	||	!check_lines_str	(src, (size_t)0, "5")
	||	!check_lines_str	(src, (size_t)1, "6"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)14)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)2)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if	(!gleip_lines_mv_back(dst, src))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)16)
	||	!check_lines_str	(dst, (size_t)0, "1")
	||	!check_lines_str	(dst, (size_t)1, "A")
	||	!check_lines_str	(dst, (size_t)2, "B")
	||	!check_lines_str	(dst, (size_t)3, "C")
	||	!check_lines_str	(dst, (size_t)4, "X")
	||	!check_lines_str	(dst, (size_t)5, "Y")
	||	!check_lines_str	(dst, (size_t)6, "Z")
	||	!check_lines_str	(dst, (size_t)7, "D")
	||	!check_lines_str	(dst, (size_t)8, "E")
	||	!check_lines_str	(dst, (size_t)9, "F")
	||	!check_lines_str	(dst, (size_t)10, "2")
	||	!check_lines_str	(dst, (size_t)11, "3")
	||	!check_lines_str	(dst, (size_t)12, "4")
	||	!check_lines_str	(dst, (size_t)13, "G")
	||	!check_lines_str	(dst, (size_t)14, "5")
	||	!check_lines_str	(dst, (size_t)15, "6")
	||	!check_lines_integrity	(src, (size_t)0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)16)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)0)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if	(!gleip_lines_mv_back(src, dst))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)0)
	||	!check_lines_integrity	(src, (size_t)16)
	||	!check_lines_str	(src, (size_t)0, "1")
	||	!check_lines_str	(src, (size_t)1, "A")
	||	!check_lines_str	(src, (size_t)2, "B")
	||	!check_lines_str	(src, (size_t)3, "C")
	||	!check_lines_str	(src, (size_t)4, "X")
	||	!check_lines_str	(src, (size_t)5, "Y")
	||	!check_lines_str	(src, (size_t)6, "Z")
	||	!check_lines_str	(src, (size_t)7, "D")
	||	!check_lines_str	(src, (size_t)8, "E")
	||	!check_lines_str	(src, (size_t)9, "F")
	||	!check_lines_str	(src, (size_t)10, "2")
	||	!check_lines_str	(src, (size_t)11, "3")
	||	!check_lines_str	(src, (size_t)12, "4")
	||	!check_lines_str	(src, (size_t)13, "G")
	||	!check_lines_str	(src, (size_t)14, "5")
	||	!check_lines_str	(src, (size_t)15, "6"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)0)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)16)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if	(!gleip_lines_mv_range_back
			(dst, src, (size_t)1, (size_t)3))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)3)
	||	!check_lines_str	(dst, (size_t)0, "A")
	||	!check_lines_str	(dst, (size_t)1, "B")
	||	!check_lines_str	(dst, (size_t)2, "C")
	||	!check_lines_integrity	(src, (size_t)13)
	||	!check_lines_str	(src, (size_t)0, "1")
	||	!check_lines_str	(src, (size_t)1, "X")
	||	!check_lines_str	(src, (size_t)2, "Y")
	||	!check_lines_str	(src, (size_t)3, "Z")
	||	!check_lines_str	(src, (size_t)4, "D")
	||	!check_lines_str	(src, (size_t)5, "E")
	||	!check_lines_str	(src, (size_t)6, "F")
	||	!check_lines_str	(src, (size_t)7, "2")
	||	!check_lines_str	(src, (size_t)8, "3")
	||	!check_lines_str	(src, (size_t)9, "4")
	||	!check_lines_str	(src, (size_t)10, "G")
	||	!check_lines_str	(src, (size_t)11, "5")
	||	!check_lines_str	(src, (size_t)12, "6"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)3)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)13)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if	(!gleip_lines_mv_line_back(dst, src, (size_t)10))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)4)
	||	!check_lines_str	(dst, (size_t)0, "A")
	||	!check_lines_str	(dst, (size_t)1, "B")
	||	!check_lines_str	(dst, (size_t)2, "C")
	||	!check_lines_str	(dst, (size_t)3, "G")
	||	!check_lines_integrity	(src, (size_t)12)
	||	!check_lines_str	(src, (size_t)0, "1")
	||	!check_lines_str	(src, (size_t)1, "X")
	||	!check_lines_str	(src, (size_t)2, "Y")
	||	!check_lines_str	(src, (size_t)3, "Z")
	||	!check_lines_str	(src, (size_t)4, "D")
	||	!check_lines_str	(src, (size_t)5, "E")
	||	!check_lines_str	(src, (size_t)6, "F")
	||	!check_lines_str	(src, (size_t)7, "2")
	||	!check_lines_str	(src, (size_t)8, "3")
	||	!check_lines_str	(src, (size_t)9, "4")
	||	!check_lines_str	(src, (size_t)10, "5")
	||	!check_lines_str	(src, (size_t)11, "6"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)4)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)12)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if	(!gleip_lines_mv_range
			(dst, src, (size_t)3, (size_t)1, (size_t)6))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)10)
	||	!check_lines_str	(dst, (size_t)0, "A")
	||	!check_lines_str	(dst, (size_t)1, "B")
	||	!check_lines_str	(dst, (size_t)2, "C")
	||	!check_lines_str	(dst, (size_t)3, "X")
	||	!check_lines_str	(dst, (size_t)4, "Y")
	||	!check_lines_str	(dst, (size_t)5, "Z")
	||	!check_lines_str	(dst, (size_t)6, "D")
	||	!check_lines_str	(dst, (size_t)7, "E")
	||	!check_lines_str	(dst, (size_t)8, "F")
	||	!check_lines_str	(dst, (size_t)9, "G")
	||	!check_lines_integrity	(src, (size_t)6)
	||	!check_lines_str	(src, (size_t)0, "1")
	||	!check_lines_str	(src, (size_t)1, "2")
	||	!check_lines_str	(src, (size_t)2, "3")
	||	!check_lines_str	(src, (size_t)3, "4")
	||	!check_lines_str	(src, (size_t)4, "5")
	||	!check_lines_str	(src, (size_t)5, "6"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)10)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)6)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	/*
	 * Test `gleip_lines_rm_*`.
	 */

	gleip_lines_rm_range(dst, (size_t)6, (size_t)3);
	if	(!check_lines_integrity	(dst, (size_t)7)
	||	!check_lines_str	(dst, (size_t)0, "A")
	||	!check_lines_str	(dst, (size_t)1, "B")
	||	!check_lines_str	(dst, (size_t)2, "C")
	||	!check_lines_str	(dst, (size_t)3, "X")
	||	!check_lines_str	(dst, (size_t)4, "Y")
	||	!check_lines_str	(dst, (size_t)5, "Z")
	||	!check_lines_str	(dst, (size_t)6, "G"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)7)
		||	!check_lines_trimmed	(dst))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
		}
	}

	gleip_lines_rm_range_back(dst, (size_t)2);
	if	(!check_lines_integrity	(dst, (size_t)5)
	||	!check_lines_str	(dst, (size_t)0, "A")
	||	!check_lines_str	(dst, (size_t)1, "B")
	||	!check_lines_str	(dst, (size_t)2, "C")
	||	!check_lines_str	(dst, (size_t)3, "X")
	||	!check_lines_str	(dst, (size_t)4, "Y"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)5)
		||	!check_lines_trimmed	(dst))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
		}
	}

	gleip_lines_rm_range(dst, 0, (size_t)3);
	if	(!check_lines_integrity	(dst, (size_t)2)
	||	!check_lines_str	(dst, (size_t)0, "X")
	||	!check_lines_str	(dst, (size_t)1, "Y"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)2)
		||	!check_lines_trimmed	(dst))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
		}
	}

	gleip_lines_rm_line(dst, 0);
	if	(!check_lines_integrity	(dst, (size_t)1)
	||	!check_lines_str	(dst, (size_t)0, "Y"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)1)
		||	!check_lines_trimmed	(dst))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
		}
	}

	gleip_lines_rm_line_back(dst);
	if	(!check_lines_integrity	(dst, (size_t)0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)0)
		||	!check_lines_trimmed	(dst))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
		}
	}

	/*
	 * Test `gleip_lines_alloc_mv_*`.
	 */

	gleip_lines_free(dst);
	dst	= NULL;

	dst	= gleip_lines_alloc_mv_range(src, (size_t)3, (size_t)3);
	if	(NULL == dst)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)3)
	||	!check_lines_str	(dst, (size_t)0, "4")
	||	!check_lines_str	(dst, (size_t)1, "5")
	||	!check_lines_str	(dst, (size_t)2, "6")
	||	!check_lines_integrity	(src, (size_t)3)
	||	!check_lines_str	(src, (size_t)0, "1")
	||	!check_lines_str	(src, (size_t)1, "2")
	||	!check_lines_str	(src, (size_t)2, "3"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)3)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)3)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	gleip_lines_free(dst);
	dst	= NULL;

	dst	= gleip_lines_alloc_mv_line(src, (size_t)2);
	if	(NULL == dst)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)1)
	||	!check_lines_str	(dst, (size_t)0, "3")
	||	!check_lines_integrity	(src, (size_t)2)
	||	!check_lines_str	(src, (size_t)0, "1")
	||	!check_lines_str	(src, (size_t)1, "2"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)1)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)2)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_lines_mv_rm() FINI");
	}
	if	(NULL != src)
	{
		gleip_lines_free(src);
	}
	if	(NULL != dst)
	{
		gleip_lines_free(dst);
	}
	return	retval;
}

/*
 * -	`gleip_lines_cpy*`
 * -	`gleip_lines_alloc_cpy*`
 */
static
int
test_lines_cpy (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int			retval	= 1;
	struct gleip_lines	* dst	= NULL,
				* src	= NULL;

	if	(verbose)
	{
		(void)puts("test_lines_cpy() INIT");
	}

	dst	= gleip_lines_alloc();
	if	(NULL == dst)
	{
		print_fail_alloc();
		return	0;
	}

	src	= gleip_lines_alloc();
	if	(NULL == src)
	{
		gleip_lines_free(dst);
		print_fail_alloc();
		return	0;
	}

	if	(!check_lines_integrity	(dst, (size_t)0)
	||	!check_lines_integrity	(src, (size_t)0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(dst, (size_t)0)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)0)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if	(!gleip_lines_addcpy_back	(src, "A")
	||	!gleip_lines_addcpy_back	(src, "B")
	||	!gleip_lines_addcpy_back	(src, "C")
	||	!gleip_lines_addcpy_back	(src, "D")
	||	!gleip_lines_addcpy_back	(src, "E")
	||	!gleip_lines_addcpy_back	(src, "F")
	||	!gleip_lines_addcpy_back	(src, "G"))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)0)
	||	!check_lines_integrity	(src, (size_t)7)
	||	!check_lines_str	(src, (size_t)0, "A")
	||	!check_lines_str	(src, (size_t)1, "B")
	||	!check_lines_str	(src, (size_t)2, "C")
	||	!check_lines_str	(src, (size_t)3, "D")
	||	!check_lines_str	(src, (size_t)4, "E")
	||	!check_lines_str	(src, (size_t)5, "F")
	||	!check_lines_str	(src, (size_t)6, "G"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)0)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)7)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if	(!gleip_lines_cpy_range
			(dst, src, (size_t)0, (size_t)3, (size_t)2))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)2)
	||	!check_lines_str	(dst, (size_t)0, "D")
	||	!check_lines_str	(dst, (size_t)1, "E")
	||	!check_lines_integrity	(src, (size_t)7)
	||	!check_lines_str	(src, (size_t)0, "A")
	||	!check_lines_str	(src, (size_t)1, "B")
	||	!check_lines_str	(src, (size_t)2, "C")
	||	!check_lines_str	(src, (size_t)3, "D")
	||	!check_lines_str	(src, (size_t)4, "E")
	||	!check_lines_str	(src, (size_t)5, "F")
	||	!check_lines_str	(src, (size_t)6, "G"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)2)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)7)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if	(!gleip_lines_cpy(dst, src, (size_t)0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)9)
	||	!check_lines_str	(dst, (size_t)0, "A")
	||	!check_lines_str	(dst, (size_t)1, "B")
	||	!check_lines_str	(dst, (size_t)2, "C")
	||	!check_lines_str	(dst, (size_t)3, "D")
	||	!check_lines_str	(dst, (size_t)4, "E")
	||	!check_lines_str	(dst, (size_t)5, "F")
	||	!check_lines_str	(dst, (size_t)6, "G")
	||	!check_lines_str	(dst, (size_t)7, "D")
	||	!check_lines_str	(dst, (size_t)8, "E")
	||	!check_lines_integrity	(src, (size_t)7)
	||	!check_lines_str	(src, (size_t)0, "A")
	||	!check_lines_str	(src, (size_t)1, "B")
	||	!check_lines_str	(src, (size_t)2, "C")
	||	!check_lines_str	(src, (size_t)3, "D")
	||	!check_lines_str	(src, (size_t)4, "E")
	||	!check_lines_str	(src, (size_t)5, "F")
	||	!check_lines_str	(src, (size_t)6, "G"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)9)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)7)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if	(!gleip_lines_cpy_range
			(dst, src, (size_t)7, (size_t)0, (size_t)3))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)12)
	||	!check_lines_str	(dst, (size_t)0, "A")
	||	!check_lines_str	(dst, (size_t)1, "B")
	||	!check_lines_str	(dst, (size_t)2, "C")
	||	!check_lines_str	(dst, (size_t)3, "D")
	||	!check_lines_str	(dst, (size_t)4, "E")
	||	!check_lines_str	(dst, (size_t)5, "F")
	||	!check_lines_str	(dst, (size_t)6, "G")
	||	!check_lines_str	(dst, (size_t)7, "A")
	||	!check_lines_str	(dst, (size_t)8, "B")
	||	!check_lines_str	(dst, (size_t)9, "C")
	||	!check_lines_str	(dst, (size_t)10, "D")
	||	!check_lines_str	(dst, (size_t)11, "E")
	||	!check_lines_integrity	(src, (size_t)7)
	||	!check_lines_str	(src, (size_t)0, "A")
	||	!check_lines_str	(src, (size_t)1, "B")
	||	!check_lines_str	(src, (size_t)2, "C")
	||	!check_lines_str	(src, (size_t)3, "D")
	||	!check_lines_str	(src, (size_t)4, "E")
	||	!check_lines_str	(src, (size_t)5, "F")
	||	!check_lines_str	(src, (size_t)6, "G"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)12)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)7)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if (!gleip_lines_cpy_range_back(dst, src, (size_t)5, (size_t)2))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)14)
	||	!check_lines_str	(dst, (size_t)0, "A")
	||	!check_lines_str	(dst, (size_t)1, "B")
	||	!check_lines_str	(dst, (size_t)2, "C")
	||	!check_lines_str	(dst, (size_t)3, "D")
	||	!check_lines_str	(dst, (size_t)4, "E")
	||	!check_lines_str	(dst, (size_t)5, "F")
	||	!check_lines_str	(dst, (size_t)6, "G")
	||	!check_lines_str	(dst, (size_t)7, "A")
	||	!check_lines_str	(dst, (size_t)8, "B")
	||	!check_lines_str	(dst, (size_t)9, "C")
	||	!check_lines_str	(dst, (size_t)10, "D")
	||	!check_lines_str	(dst, (size_t)11, "E")
	||	!check_lines_str	(dst, (size_t)12, "F")
	||	!check_lines_str	(dst, (size_t)13, "G")
	||	!check_lines_integrity	(src, (size_t)7)
	||	!check_lines_str	(src, (size_t)0, "A")
	||	!check_lines_str	(src, (size_t)1, "B")
	||	!check_lines_str	(src, (size_t)2, "C")
	||	!check_lines_str	(src, (size_t)3, "D")
	||	!check_lines_str	(src, (size_t)4, "E")
	||	!check_lines_str	(src, (size_t)5, "F")
	||	!check_lines_str	(src, (size_t)6, "G"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)14)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)7)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	if	(!gleip_lines_cpy_back(dst, src))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)21)
	||	!check_lines_str	(dst, (size_t)0, "A")
	||	!check_lines_str	(dst, (size_t)1, "B")
	||	!check_lines_str	(dst, (size_t)2, "C")
	||	!check_lines_str	(dst, (size_t)3, "D")
	||	!check_lines_str	(dst, (size_t)4, "E")
	||	!check_lines_str	(dst, (size_t)5, "F")
	||	!check_lines_str	(dst, (size_t)6, "G")
	||	!check_lines_str	(dst, (size_t)7, "A")
	||	!check_lines_str	(dst, (size_t)8, "B")
	||	!check_lines_str	(dst, (size_t)9, "C")
	||	!check_lines_str	(dst, (size_t)10, "D")
	||	!check_lines_str	(dst, (size_t)11, "E")
	||	!check_lines_str	(dst, (size_t)12, "F")
	||	!check_lines_str	(dst, (size_t)13, "G")
	||	!check_lines_str	(dst, (size_t)14, "A")
	||	!check_lines_str	(dst, (size_t)15, "B")
	||	!check_lines_str	(dst, (size_t)16, "C")
	||	!check_lines_str	(dst, (size_t)17, "D")
	||	!check_lines_str	(dst, (size_t)18, "E")
	||	!check_lines_str	(dst, (size_t)19, "F")
	||	!check_lines_str	(dst, (size_t)20, "G")
	||	!check_lines_integrity	(src, (size_t)7)
	||	!check_lines_str	(src, (size_t)0, "A")
	||	!check_lines_str	(src, (size_t)1, "B")
	||	!check_lines_str	(src, (size_t)2, "C")
	||	!check_lines_str	(src, (size_t)3, "D")
	||	!check_lines_str	(src, (size_t)4, "E")
	||	!check_lines_str	(src, (size_t)5, "F")
	||	!check_lines_str	(src, (size_t)6, "G"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)21)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)7)
		||	!check_lines_trimmed	(src))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	gleip_lines_free(dst);
	dst	= NULL;

	dst	= gleip_lines_alloc_cpy(src);
	if	(NULL == dst)
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity	(dst, (size_t)7)
	||	!check_lines_str	(dst, (size_t)0, "A")
	||	!check_lines_str	(dst, (size_t)1, "B")
	||	!check_lines_str	(dst, (size_t)2, "C")
	||	!check_lines_str	(dst, (size_t)3, "D")
	||	!check_lines_str	(dst, (size_t)4, "E")
	||	!check_lines_str	(dst, (size_t)5, "F")
	||	!check_lines_str	(dst, (size_t)6, "G")
	||	!check_lines_integrity	(src, (size_t)7)
	||	!check_lines_str	(src, (size_t)0, "A")
	||	!check_lines_str	(src, (size_t)1, "B")
	||	!check_lines_str	(src, (size_t)2, "C")
	||	!check_lines_str	(src, (size_t)3, "D")
	||	!check_lines_str	(src, (size_t)4, "E")
	||	!check_lines_str	(src, (size_t)5, "F")
	||	!check_lines_str	(src, (size_t)6, "G")
	||	!check_lines_equal	(dst, src, 0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(dst, "\tdst:\t", stdout);
		print_lines_full(src, "\tsrc:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(dst)
		||	!gleip_lines_mem_trim(src))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * `check_lines_str` omitted.
		 */
		if	(!check_lines_integrity	(dst, (size_t)7)
		||	!check_lines_trimmed	(dst)
		||	!check_lines_integrity	(src, (size_t)7)
		||	!check_lines_trimmed	(src)
		||	!check_lines_equal	(dst, src, 0))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(dst, "\tdst:\t", stdout);
			print_lines_full(src, "\tsrc:\t", stdout);
		}
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_lines_cpy() FINI");
	}
	if	(NULL != src)
	{
		gleip_lines_free(src);
	}
	if	(NULL != dst)
	{
		gleip_lines_free(dst);
	}
	return	retval;
}

int
test_lines (
	const int	verbose
	)
{
	return	test_lines_shift	(verbose, 0)
	&&	test_lines_shift	(verbose, 1)
	&&	test_lines_new		(verbose, 0)
	&&	test_lines_new		(verbose, 1)
	&&	test_lines_addref	(verbose, 0)
	&&	test_lines_addref	(verbose, 1)
	&&	test_lines_addcpy	(verbose, 0)
	&&	test_lines_addcpy	(verbose, 1)
	&&	test_lines_mv_rm	(verbose, 0)
	&&	test_lines_mv_rm	(verbose, 1)
	&&	test_lines_cpy		(verbose, 0)
	&&	test_lines_cpy		(verbose, 1);
}

/*@=boolint@*/

