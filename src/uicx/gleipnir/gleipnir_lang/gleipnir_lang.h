/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef	GLEIP_LANG_H
#define	GLEIP_LANG_H

#include <stdio.h>	/* FILE */

#include "gleipnir_lines.h"	/* gleip_lines */

#if	defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#define GLEIP_RSTR restrict
#else
#define	GLEIP_RSTR
#endif

/*@unchecked@*/
/*@unused@*/
extern
const long	GLEIP_LANG_VERSION;

struct gleip_lang
{

/*@in@*/
/*@notnull@*/
/*@owned@*/
	struct gleip_lines	* keys,
				* values;

};

/*@unused@*/
extern
size_t
gleip_lang_size (
	const struct gleip_lang	* const GLEIP_RSTR
	)
/*@*/
;

/*@in@*/
/*@notnull@*/
/*@unused@*/
extern
const char *
gleip_lang_key (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	const struct gleip_lang	* const GLEIP_RSTR,
	const size_t
	)
/*@*/
;

/*@in@*/
/*@null@*/
/*@unused@*/
extern
const char *
gleip_lang_value (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	const struct gleip_lang	* const GLEIP_RSTR,
	const size_t
	)
/*@*/
;

/*@unused@*/
extern
int
gleip_lang_mem_trim (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lang	* const GLEIP_RSTR lang
/*@=protoparamname@*/
	)
/*@modifies * lang->keys->arr, lang->keys->arr, lang->keys->mem@*/
/*@modifies * lang->values->arr, lang->values->arr, lang->values->mem@*/
;

/*@in@*/
/*@null@*/
/*@unused@*/
extern
const char *
gleip_lang_get (
/*@in@*/
/*@notnull@*/
/*@returned@*/
/*@-protoparamname@*/
	const struct gleip_lang	* const GLEIP_RSTR lang,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@returned@*/
	const char		* const GLEIP_RSTR
	)
/*@*/
;

/*@in@*/
/*@null@*/
/*@unused@*/
extern
const char *
gleip_lang_getany (
/*@in@*/
/*@notnull@*/
/*@returned@*/
/*@-protoparamname@*/
	const struct gleip_lang	* const GLEIP_RSTR lang,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@returned@*/
	const char		* const GLEIP_RSTR
	)
/*@*/
;

/*@null@*/
/*@in@*/
/*@unused@*/
extern
const char *
gleip_lang_invalid_why (
/*@in@*/
/*@notnull@*/
/*@returned@*/
/*@-protoparamname@*/
	const struct gleip_lang	* const GLEIP_RSTR lang
/*@=protoparamname@*/
	)
/*@*/
;

/*@unused@*/
extern
int
gleip_lang_valid (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	const struct gleip_lang	* const GLEIP_RSTR lang
/*@=protoparamname@*/
	)
/*@*/
;

/*@unused@*/
extern
void
gleip_lang_clear (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lang	* const GLEIP_RSTR lang
/*@=protoparamname@*/
	)
/*@modifies * lang->values@*/
;

/*@unused@*/
extern
int
gleip_lang_read (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lang	* const GLEIP_RSTR lang,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	void			* const data,
/*@=protoparamname@*/
/*@in@*/
/*@null@*/
/*@-protoparamname@*/
	struct gleip_lines	* GLEIP_RSTR buf,
/*@=protoparamname@*/
/*@out@*/
/*@null@*/
/*@-protoparamname@*/
	int			* const GLEIP_RSTR valid,
/*@=protoparamname@*/
/*@notnull@*/
	int			(*) (void *)
	)
/*@modifies * lang->values, lang->values@*/
/*@modifies * buf->arr, buf->arr, buf->size, buf->mem@*/
/*@modifies * data, * valid@*/
;

/*@unused@*/
extern
int
gleip_lang_read_file (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lang	* const GLEIP_RSTR lang,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	FILE			* const file,
/*@=protoparamname@*/
/*@in@*/
/*@null@*/
/*@-protoparamname@*/
	struct gleip_lines	* GLEIP_RSTR buf,
/*@=protoparamname@*/
/*@in@*/
/*@null@*/
/*@-protoparamname@*/
	int			* const GLEIP_RSTR valid
	)
/*@modifies * lang->values, lang->values@*/
/*@modifies * buf->arr, buf->arr, buf->size, buf->mem@*/
/*@modifies * file, * valid@*/
;

/*@unused@*/
extern
int
gleip_lang_read_cstr (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lang	* const GLEIP_RSTR lang,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	const char		* * const src,
/*@=protoparamname@*/
/*@in@*/
/*@null@*/
/*@-protoparamname@*/
	struct gleip_lines	* GLEIP_RSTR buf,
/*@=protoparamname@*/
/*@in@*/
/*@null@*/
/*@-protoparamname@*/
	int			* const GLEIP_RSTR valid
	)
/*@modifies * lang->values, lang->values@*/
/*@modifies * buf->arr, buf->arr, buf->size, buf->mem@*/
/*@modifies * src, * valid@*/
;

/*@unused@*/
extern
int
gleip_lang_isreg (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR,
/*@in@*/
/*@notnull@*/
	const char		* const
	)
/*@*/
;

/*@unused@*/
extern
int
gleip_lang_regcpy (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lang	* const GLEIP_RSTR lang,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const char		* const
	)
/*@modifies * lang->keys->arr, lang->keys->arr@*/
/*@modifies lang->keys->size, lang->keys->mem@*/
/*@modifies * lang->values->arr, lang->values->arr@*/
/*@modifies lang->values->size, lang->values->mem@*/
;

/*@unused@*/
extern
int
gleip_lang_regref (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lang	* const GLEIP_RSTR lang,
/*@=protoparamname@*/
/*@in@*/
/*@keep@*/
/*@notnull@*/
	char			* const
	)
/*@modifies * lang->keys->arr, lang->keys->arr@*/
/*@modifies lang->keys->size, lang->keys->mem@*/
/*@modifies * lang->values->arr, lang->values->arr@*/
/*@modifies lang->values->size, lang->values->mem@*/
;

/*@unused@*/
extern
void
gleip_lang_unreg_i (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lang	* const GLEIP_RSTR lang,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * lang->keys, lang->keys->size@*/
/*@modifies * lang->values, lang->values->size@*/
;

/*@unused@*/
extern
void
gleip_lang_unreg (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lang	* const GLEIP_RSTR lang,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const char		* const
	)
/*@modifies * lang->keys, lang->keys->size@*/
/*@modifies * lang->values, lang->values->size@*/
;

/*@unused@*/
extern
void
gleip_lang_unreg_all (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lang	* const GLEIP_RSTR lang
/*@=protoparamname@*/
	)
/*@modifies * lang->keys, lang->keys->size@*/
/*@modifies * lang->values, lang->values->size@*/
;

/*@unused@*/
extern
int
gleip_lang_print (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR,
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	FILE			* const GLEIP_RSTR out,
/*@=protoparamname@*/
	const char
	)
/*@globals fileSystem@*/
/*@modifies fileSystem, out@*/
;

/*@unused@*/
extern
int
gleip_lang_cpy (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lang	* const GLEIP_RSTR lang,
/*@=protoparamname@*/
	const struct gleip_lang	* const GLEIP_RSTR
	)
/*@modifies * lang->keys, lang->keys->size, lang->keys->mem@*/
/*@modifies * lang->values, lang->values->size, lang->values->mem@*/
;

/*@unused@*/
extern
void
gleip_lang_free (
/*@in@*/
/*@notnull@*/
/*@only@*/
/*@-protoparamname@*/
	struct gleip_lang	* const GLEIP_RSTR lang
/*@=protoparamname@*/
	)
/*@modifies lang->keys->arr, lang->keys@*/
/*@modifies lang->values->arr, lang->values@*/
/*@releases lang->keys->arr, lang->keys, lang->values->arr,\
	lang->values, lang@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
struct gleip_lang *
gleip_lang_alloc_mem (
	const size_t
	)
/*@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
struct gleip_lang *
gleip_lang_alloc_cpy (
	const struct gleip_lang	* const GLEIP_RSTR
	)
/*@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
struct gleip_lang *
gleip_lang_alloc (void)
/*@*/
;

#endif

