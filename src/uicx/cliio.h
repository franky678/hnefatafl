/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CLIIO_H
#define HNEF_CLIIO_H

#include "gleipnir_lang.h"	/* gleip_lang */

#include "hnefatafl.h"

#include "funcxt.h"	/* HNEF_FRX */

extern
int
hnef_ui_digitc (
	const long
	)
/*@globals errno@*/
/*@modifies errno@*/
;

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_print_langkey (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR lang,
/*@in@*/
/*@notnull@*/
	const char		* const HNEF_RSTR key,
/*@in@*/
/*@notnull@*/
	FILE			* const HNEF_RSTR out
	)
/*@globals fileSystem@*/
/*@modifies fileSystem, * out@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_print_fail_fr (
/*@in@*/
/*@null@*/
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const enum HNEF_FR	fr,
/*@in@*/
/*@notnull@*/
	FILE			* const HNEF_RSTR out
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, * out@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_print_fail_rread (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const enum HNEF_RREAD	rread,
/*@in@*/
/*@notnull@*/
	FILE			* const HNEF_RSTR out
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, * out@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_print_fail_rvalid (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const enum HNEF_RVALID	rvalid,
/*@in@*/
/*@notnull@*/
	FILE			* const HNEF_RSTR out
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, * out@*/
;
/*@=protoparamname@*/

#ifdef	HNEFATAFL_UI_XLIB

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_print_fail_frx (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const enum HNEF_FRX	frx,
/*@in@*/
/*@notnull@*/
	FILE			* const HNEF_RSTR out
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, * out@*/
;
/*@=protoparamname@*/

#endif	/* HNEFATAFL_UI_XLIB */

extern
size_t
hnef_textlen_tabs (
	const size_t,
	size_t
	)
/*@modifies nothing@*/
;

/*@unused@*/	/* Unused without X */
extern
enum HNEF_FR
hnef_beep (void)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout@*/
;

extern
HNEF_BOOL
hnef_print_envvar_single (
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR,
	size_t
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout@*/
;

extern
enum HNEF_FR
hnef_print_envvar (void)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout@*/
;

extern
enum HNEF_FR
hnef_print_copy (void)
/*@globals fileSystem, stdout@*/
/*@modifies fileSystem, stdout@*/
;

extern
enum HNEF_FR
hnef_print_version (void)
/*@globals fileSystem, stdout@*/
/*@modifies fileSystem, stdout@*/
;

/*@in@*/
/*@notnull@*/
/*@observer@*/
/*@unused@*/	/* Unused without X */
extern
const char *
hnef_nine_brief (
/*@in@*/
/*@null@*/
	const char	* const HNEF_RSTR
	)
/*@globals errno, internalState@*/
/*@modifies errno, internalState@*/
;

extern
void
hnef_nine_print (
/*@in@*/
/*@null@*/
	const char	* const HNEF_RSTR
	)
/*@globals errno, fileSystem, internalState, stdout@*/
/*@modifies errno, fileSystem, internalState, stdout@*/
;

/*@in@*/
/*@notnull@*/
/*@observer@*/
extern
const char *
hnef_nine_hint (void)
/*@modifies nothing@*/
;

#endif

