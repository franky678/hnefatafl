/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#ifndef	HNEF_UI_UIXUTIL_H
#define	HNEF_UI_UIXUTIL_H

#include "X11/Xlib.h"	/* GC */

#include "hnefatafl.h"

#include "funcxt.h"	/* HNEF_FRX */
#include "uit.h"	/* hnef_ui */
#include "uixt.h"	/* hnef_uix */

/*@-protoparamname@*/
extern
void
hnef_uix_marker_clear_select (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui
	)
/*@modifies * ui@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
void
hnef_uix_marker_clear_movelist (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui
	)
/*@modifies * ui@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
void
hnef_uix_marker_clear_last (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui
	)
/*@modifies * ui@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
void
hnef_uix_marker_clear_all (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui
	)
/*@modifies * ui@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
void
hnef_uix_paintmap_clear (
/*@partial@*/
/*@notnull@*/
	struct hnef_uix	* const HNEF_RSTR uix
	)
/*@modifies * uix@*/
;
/*@=protoparamname@*/

extern
int
hnef_uix_margin_bottom (
/*@in@*/
/*@notnull@*/
	const struct hnef_uix	* const HNEF_RSTR
	)
/*@modifies nothing@*/
;

extern
int
hnef_uix_square_size_w (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR,
/*@in@*/
/*@notnull@*/
	const struct hnef_uix_themes	* const HNEF_RSTR
	)
/*@modifies nothing@*/
;

extern
int
hnef_uix_square_size_h (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR,
/*@in@*/
/*@notnull@*/
	const struct hnef_uix_themes	* const HNEF_RSTR
	)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
extern
int
hnef_uix_board_w (
/*@in@*/
/*@notnull@*/
	struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	const struct hnef_uix_themes	* const HNEF_RSTR themes
	)
/*@modifies * game@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
int
hnef_uix_board_h (
/*@in@*/
/*@notnull@*/
	struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	const struct hnef_uix_themes	* const HNEF_RSTR themes
	)
/*@modifies * game@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FRX
hnef_uix_window_size_update (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@null@*/
	HNEF_BOOL	* const HNEF_RSTR changed
	)
/*@modifies * ui, * changed@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
void
hnef_uix_window_hide (
/*@in@*/
/*@notnull@*/
	struct hnef_uix	* const HNEF_RSTR uix
	)
/*@modifies * uix@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FRX
hnef_uix_window_update (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui
	)
/*@modifies * ui@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FRX
hnef_uix_window_show (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui
	)
/*@modifies * ui@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
void
hnef_uix_gc_conf (
/*@partial@*/
/*@notnull@*/
	const struct hnef_uix	* const HNEF_RSTR uix,
/*@in@*/
/*@notnull@*/
	GC			gc
	)
/*@modifies * gc@*/
;
/*@=protoparamname@*/

#endif

#endif	/* HNEFATAFL_UI_XLIB */

