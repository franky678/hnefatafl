/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#ifndef HNEF_UI_UIXFONTT_H
#define HNEF_UI_UIXFONTT_H

#include "X11/Xlib.h"	/* XFont* */

#include "hnefatafl.h"

struct hnef_uix_font
{

	/*
	 * If `true` (and properly allocated / initialized), then
	 * `fontset` is `!NULL` and should be used.
	 *
	 * If `false` (and properly allocated / initialized), then
	 * `fontstruct` is `!NULL` and should be used.
	 *
	 * `use_fontset` is false if any of the following is true:
	 *
	 * -	`setlocale()` failed (see `main.c`).
	 * -	`XCreateFontSet()` failed (see `uixfont.c`).
	 *
	 * We want `use_fontset` to be true so we can read user input
	 * and print output in UTF-8. Otherwise UTF-8 will not work and
	 * we'll fall back on Latin-1. We have to expect that XFontSet
	 * (and consequently UTF-8) doesn't work because anything
	 * involving XFontSet seems to be extremely buggy and unreliable
	 * in XLib.
	 */
	HNEF_BOOL	use_fontset;

	/*
	 * Latin-1 (fallback).
	 */
/*@in@*/
/*@null@*/
/*@owned@*/
	XFontStruct	* fontstruct;

	/*
	 * UTF-8 (preferred).
	 */
/*@in@*/
/*@null@*/
/*@owned@*/
	XFontSet	fontset;	/* Pointer */

	int		ascent,
			descent;

};

#endif

#endif	/* HNEFATAFL_UI_XLIB */

