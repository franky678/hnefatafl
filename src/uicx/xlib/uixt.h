/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#ifndef HNEF_UI_UIXT_H
#define HNEF_UI_UIXT_H

#include "X11/Xlib.h"		/* Display, Window, ... */

#include "hnefatafl.h"

#include "funcxt.h"		/* HNEF_FRX */
#include "uixconsolet.h"	/* hnef_uix_console */
#include "uixfontt.h"		/* hnef_uix_font */

/*@unchecked@*/
/*@unused@*/		/* Unused without AI */
extern
const int	HNEF_UIX_SIZE_UNINIT,
		HNEF_UIX_PROGRESS_INVALID;

struct hnef_uix
{

	/*
	 * `quit_xlib` is set to true when the XLib interface should
	 * hide the window and go back to the CLI. This does not entail
	 * quitting the program. This is reset when `hnef_run_xlib()` is
	 * called.
	 *
	 * `lastmove_show` is true if the last move should be shown.
	 */
	HNEF_BOOL		quit_xlib,
				lastmove_show;

#ifdef	HNEFATAFL_UI_AIM
	/*
	 * Set to true if the AI should force a move or stop thinking.
	 */
	HNEF_BOOL		ai_force,
				ai_stop;
#endif	/* HNEFATAFL_UI_AIM */

/*@in@*/
/*@notnull@*/
/*@owned@*/
	struct hnef_uix_console	* console;

/*@in@*/
/*@notnull@*/
/*@owned@*/
	struct hnef_uix_font	* font;

/*@in@*/
/*@notnull@*/
/*@owned@*/
	struct hnef_uix_themes	* themes;

/*@in@*/
/*@notnull@*/
/*@owned@*/
	Display			* display;

	XWindowAttributes	window_atts;

	/*
	 * `window_*` are *actual* window sizes.
	 *
	 * `opt_offset_*` are offsets when painting the board. May be
	 * negative.
	 *
	 * `progress_height` is the height of the progress bar. This
	 * never changes once it's set in `hnef_alloc_uix_init()`.
	 */
	int			window_width,
				window_height,
				opt_offset_x,
				opt_offset_y,
				progress_height;

	/*
	 * `*_last` is for when repainting without `force_repaint`.
	 */
	int			progress,
				progress_max,
				progress_last,
				progress_max_last;

/*@in@*/
/*@notnull@*/
/*@owned@*/
	GC			gc;

	/*
	 * Used for `xic`.
	 */
/*@in@*/
/*@null@*/
/*@owned@*/
	XIM			xim;

	/*
	 * This is only `NULL` if `font->use_fontset`.
	 */
/*@in@*/
/*@null@*/
/*@owned@*/
	XIC			xic;

	Colormap		colormap;

/*@in@*/
/*@notnull@*/
/*@owned@*/
	XEvent			event;

	/*
	 * `window_del` is for listening for `WM_WINDOW_DELETE`.
	 *
	 * `focus_take` is for listening for `WM_TAKE_FOCUS`.
	 */
	Atom			window_del,
				focus_take;

	int			screen;

/*@in@*/
/*@notnull@*/
/*@owned@*/
	XColor			col_red,
				col_flag_blue,
				col_flag_yellow;

	/*
	 * Background / foreground pixel colors.
	 */
	unsigned long		px_white,
				px_black;

	Window			window;

	/*
	 * Dirty squares for painting. `NULL` until
	 * `hnef_uix_themes_load_rules()` has been called. After the
	 * first call to that function it's never `NULL`.
	 */
/*@null@*/
/*@owned@*/
	HNEF_BIT_U8		* paintmap;

	unsigned short		paintmap_width,
				paintmap_height;

	/*
	 * Selected board position. `HNEF_BOARDPOS_NONE` for none.
	 */
	unsigned short		pos_select;

	/*
	 * This variable is used when calling `hnef_computer_move()`.
	 * `core` and `aim` do not know about the `HNEF_FRX` enum, but
	 * we need `hnef_computer_move()` to return a `HNEF_FRX` value,
	 * so this value must be in the `void * data` pointer (i.e. this
	 * struct) passed to `hnef_computer_move()`.
	 *
	 * This value is not used for anything else. You shouldn't use
	 * it or check it in any other places. This variable is an ugly
	 * misfit that exists out of necessity -- pretend it doesn't
	 * exist.
	 */
#ifndef	HNEFATAFL_UI_AIM
/*@unused@*/
#endif	/* HNEFATAFL_UI_AIM */
	enum HNEF_FRX		frx;

	/*
	 * `NULM`-terminated string used for `XmbLookupString()` in case
	 * it returns buffer overflow (which won't happen with UTF-8 if
	 * you make room for 6 characters, but let's do it anyway to be
	 * safe).
	 */
/*@notnull@*/
/*@owned@*/
	char			* opt_lookupstring;

	/*
	 * When we get loads of queued Expose events, we sent
	 * `opt_repaint_due` instead of repainting a million times. Then
	 * we repaint once when any event is handled if this is set.
	 */
	HNEF_BOOL		opt_repaint_due;

};

#endif

#endif	/* HNEFATAFL_UI_XLIB */

