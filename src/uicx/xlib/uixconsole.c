/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#include <assert.h>	/* assert */
#ifndef	S_SPLINT_S /* SPLint bug: ctype.h results in parse error */
#include <ctype.h>	/* is* */
#endif	/* S_SPLINT_S */
#include <errno.h>	/* errno */
#include <string.h>	/* strlen */

#include "gleipnir_lang.h"	/* gleip_lang_* */
#include "gleipnir_line.h"	/* gleip_line_* */
#include "gleipnir_lineread.h"	/* gleip_lineread_* */

#include "cliio.h"	/* hnef_beep */
#include "cliiot.h"	/* HNEF_CMD_* */
#include "funcx.h"	/* hnef_frx_good */
#include "langkey.h"	/* hnef_langkey_get_fr */
#include "langkeyt.h"	/* HNEF_LK_* */
#include "uiutil.h"	/* hnef_ui_* */
#include "uixconsole.h"
#include "uixgame.h"	/* hnef_uix_game_* */
#include "uixpaint.h"	/* hnef_uix_paint_* */
#include "uixtheme.h"	/* hnef_uix_theme_* */
#include "uixutil.h"	/* hnef_uix_* */
#include "uixutilt.h"	/* HNEF_BIT_U8_REPAINT */

/*@observer@*/
/*@unchecked@*/
static
const char	* const HNEF_UIX_OUTPUT_SEP	= ", ";

/*
 * NOTE:	Do not modify this string. In the CLI we print the whole
 *		license, but now we must be much more brief. This must
 *		all fit on one line because the X interface can't
 *		display several lines at the same time.
 */
/*@observer@*/
/*@unchecked@*/
static
const char	* const HNEF_UIX_COPY_BRIEF =
			"© 2013-2014 Alexander Söderlund, "
			"Alexander Dolgunin, "
			"Damian Walker; "
			"INGEN GARANTI / NO WARRANTY";

static
HNEF_BOOL
hnef_uix_console_in_clear (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui
	)
/*@modifies * ui@*/
{
	HNEF_BOOL	changed;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->console);
	assert	(NULL != ui->uix->display);

	changed	= !hnef_line_empty(ui->uix->console->input);

	gleip_line_rm(ui->uix->console->input);

	return	changed;
}

static
HNEF_BOOL
hnef_uix_console_out_clear (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui
	)
/*@modifies * ui@*/
{
	HNEF_BOOL	changed;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->console);
	assert	(NULL != ui->uix->display);

	changed	= !hnef_line_empty(ui->uix->console->output);

	gleip_line_rm(ui->uix->console->output);
	ui->uix->console->output_error	= HNEF_FALSE;

	return	changed;
}

HNEF_BOOL
hnef_uix_console_clear (
	struct hnef_ui	* const HNEF_RSTR ui
	)
{
	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->console);
	assert	(NULL != ui->uix->display);

	return	hnef_uix_console_in_clear	(ui)
	||	hnef_uix_console_out_clear	(ui);
}

/*
 * `msg` should not be a 0-length string.
 *
 * This function does not require the paintmap.
 */
static
enum HNEF_FR
hnef_uix_console_out_print (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx,
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR msg,
	const HNEF_BOOL	error
	)
/*@modifies * ui, * frx@*/
{
	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->console);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);
	assert	(NULL != msg);
	assert	(!hnef_line_empty(msg));

	* frx	= HNEF_FRX_SUCCESS;

/*@i1@*/\
	if	(!gleip_line_cpy_b(& ui->uix->console->output, msg))
	{
		return	HNEF_FR_FAIL_ALLOC;
	}

	ui->uix->console->output_error	= error;

	return	hnef_uix_paint_console_out(ui, frx, HNEF_FALSE);
}

enum HNEF_FR
hnef_uix_print_langkey (
	struct hnef_ui	* const ui,
	enum HNEF_FRX	* const frx,
	const char	* const key,
	const HNEF_BOOL	error
	)
{
	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->lang);
	assert	(NULL != frx);
	assert	(NULL != key);

	return	hnef_uix_console_out_print
		(ui, frx,
		gleip_lang_getany(ui->lang, key), error);
}

static
enum HNEF_FR
hnef_uix_print_fr (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const ui,
/*@in@*/
/*@notnull@*/
	enum HNEF_FRX		* const frx,
	const enum HNEF_FR	fr
	)
/*@globals errno@*/
/*@modifies errno, * ui, * frx@*/
{
	const char	* HNEF_RSTR value	= NULL,
			* HNEF_RSTR errval	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->lang);
	assert	(NULL != frx);
	assert	(HNEF_FR_SUCCESS != fr);

	errval	= 0 == errno
		? NULL
		: strerror(errno);
	errno	= 0;

	(void)hnef_uix_console_out_clear(ui);

	value	= hnef_langkey_get_fr(ui->lang, fr);
	if	(NULL == value
	||	hnef_line_empty(value))
	{
		size_t	len;
		int	digits;

		assert	((int)fr > 0);

		digits	= hnef_ui_digitc((long)fr);
		if	(digits < 0)
		{
			return	HNEF_FR_FAIL_MATH;
		}

		if	(NULL == errval)
		{
			/*
			 * "%s%d", HNEF_UI_ERROR_FR, fr
			 */
			len	= (size_t)digits
				+ strlen(HNEF_UI_ERROR_FR)
				+ (size_t)1;

/*@i1@*/\
			if	(!gleip_line_mem_grow_b
				(& ui->uix->console->output, len + 2))
			{
				return	HNEF_FR_FAIL_ALLOC;
			}

/* SPLint snprintf is not in C89 */ /*@i1@*/\
			if	(sprintf(ui->uix->console->output,
				"%s%d", HNEF_UI_ERROR_FR, (int)fr) < 0)
			{
				return	HNEF_FR_FAIL_IO_WRITE;
			}
		}
		else
		{
			/*
			 * "%s%d (%s)", HNEF_UI_ERROR_FR, fr, errval
			 *      ^^  ^
			 *      12  3 (+ 1 for '\0' = 4)
			 */
			len	= (size_t)digits
				+ strlen(HNEF_UI_ERROR_FR)
				+ strlen(errval)
				+ (size_t)4;

/*@i1@*/\
			if	(!gleip_line_mem_grow_b
				(& ui->uix->console->output, len + 2))
			{
				return	HNEF_FR_FAIL_ALLOC;
			}

/* SPLint snprintf is not in C89 */ /*@i1@*/\
			if	(sprintf(ui->uix->console->output,
				"%s%d (%s)", HNEF_UI_ERROR_FR, (int)fr,
				errval) < 0)
			{
				return	HNEF_FR_FAIL_IO_WRITE;
			}
		}
		ui->uix->console->output_error	= HNEF_TRUE;
		return	hnef_uix_paint_console_out(ui, frx, HNEF_FALSE);
	}
	else
	{
		if	(NULL == errval)
		{
			return	hnef_uix_console_out_print
				(ui, frx, value, HNEF_TRUE);
		}
		else
		{
			/*
			 * "%s (%s)", value, errval
			 *    ^^  ^
			 *    12  3 (+ 1 for '\0' = 4)
			 */
			const size_t len	= strlen(value)
						+ strlen(errval)
						+ (size_t)4;

/*@i1@*/\
			if	(!gleip_line_mem_grow_b
				(& ui->uix->console->output, len + 2))
			{
				return	HNEF_FR_FAIL_ALLOC;
			}

/* SPLint snprintf is not in C89 */ /*@i1@*/\
			if	(sprintf(ui->uix->console->output,
				"%s (%s)", value, errval)
				< 0)
			{
				return	HNEF_FR_FAIL_IO_WRITE;
			}
			ui->uix->console->output_error	= HNEF_TRUE;
			return	hnef_uix_paint_console_out
				(ui, frx, HNEF_FALSE);
		}
	}
}

static
enum HNEF_FR
hnef_uix_print_fail_rread (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	enum HNEF_FRX		* const HNEF_RSTR frx,
	const enum HNEF_RREAD	rread
	)
/*@globals errno@*/
/*@modifies errno, * ui, * frx@*/
{
	const char	* HNEF_RSTR value	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->lang);
	assert	(NULL != frx);
	assert	(HNEF_RREAD_SUCCESS != rread);

	(void)hnef_uix_console_out_clear(ui);

	value	= hnef_langkey_get_rread(ui->lang, rread);
	if	(NULL == value
	||	hnef_line_empty(value))
	{
		size_t	len;
		int	digits;

		assert	((int)rread > 0);

		digits	= hnef_ui_digitc((long)rread);
		if	(digits < 0)
		{
			return	HNEF_FR_FAIL_MATH;
		}

		/*
		 * "%s%d", HNEF_UI_ERROR_RREAD, rread
		 */
		len	= (size_t)digits
			+ strlen(HNEF_UI_ERROR_RREAD)
			+ (size_t)1;

/*@i1@*/\
		if	(!gleip_line_mem_grow_b
			(& ui->uix->console->output, len + 2))
		{
			return	HNEF_FR_FAIL_ALLOC;
		}

/* SPLint snprintf is not in C89 */ /*@i1@*/\
		if	(sprintf(ui->uix->console->output,
			"%s%d", HNEF_UI_ERROR_RREAD, (int)rread) < 0)
		{
			return	HNEF_FR_FAIL_IO_WRITE;
		}

		ui->uix->console->output_error	= HNEF_TRUE;
		return	hnef_uix_paint_console_out(ui, frx, HNEF_FALSE);
	}
	else
	{
		return	hnef_uix_console_out_print
			(ui, frx, value, HNEF_TRUE);
	}
}

static
enum HNEF_FR
hnef_uix_print_fail_rvalid (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	enum HNEF_FRX		* const HNEF_RSTR frx,
	const enum HNEF_RVALID	rvalid
	)
/*@globals errno@*/
/*@modifies errno, * ui, * frx@*/
{
	const char	* HNEF_RSTR value	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->lang);
	assert	(NULL != frx);
	assert	(HNEF_RVALID_SUCCESS != rvalid);

	(void)hnef_uix_console_out_clear(ui);

	value	= hnef_langkey_get_rvalid(ui->lang, rvalid);
	if	(NULL == value
	||	hnef_line_empty(value))
	{
		size_t	len;
		int	digits;

		assert	((int)rvalid > 0);

		digits	= hnef_ui_digitc((long)rvalid);
		if	(digits < 0)
		{
			return	HNEF_FR_FAIL_MATH;
		}

		/*
		 * "%s%d", HNEF_UI_ERROR_RVALID, rvalid
		 */
		len	= (size_t)digits
			+ strlen(HNEF_UI_ERROR_RVALID)
			+ (size_t)1;

/*@i1@*/\
		if	(!gleip_line_mem_grow_b
			(& ui->uix->console->output, len + 2))
		{
			return	HNEF_FR_FAIL_ALLOC;
		}

/* SPLint snprintf is not in C89 */ /*@i1@*/\
		if	(sprintf(ui->uix->console->output,
			"%s%d", HNEF_UI_ERROR_RVALID, (int)rvalid) < 0)
		{
			return	HNEF_FR_FAIL_IO_WRITE;
		}

		ui->uix->console->output_error	= HNEF_TRUE;
		return	hnef_uix_paint_console_out(ui, frx, HNEF_FALSE);
	}
	else
	{
		return	hnef_uix_console_out_print
			(ui, frx, value, HNEF_TRUE);
	}
}

enum HNEF_FR
hnef_uix_print_fail_frx (
	struct hnef_ui		* const HNEF_RSTR ui,
	enum HNEF_FRX		* const HNEF_RSTR frx,
	const enum HNEF_FRX	frxcode
	)
{
	const char	* HNEF_RSTR value	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->lang);
	assert	(NULL != frx);
	assert	(HNEF_FRX_SUCCESS != frxcode);

	(void)hnef_uix_console_out_clear(ui);

	value	= hnef_langkey_get_frx(ui->lang, frxcode);
	if	(NULL == value
	||	hnef_line_empty(value))
	{
		size_t	len;
		int	digits;

		assert	((int)frxcode > 0);

		digits	= hnef_ui_digitc((long)frxcode);
		if	(digits < 0)
		{
			return	HNEF_FR_FAIL_MATH;
		}

		/*
		 * "%s%d", HNEF_UI_ERROR_FRX, frxcode
		 */
		len	= (size_t)digits
			+ strlen(HNEF_UI_ERROR_FRX)
			+ (size_t)1;

/*@i1@*/\
		if	(!gleip_line_mem_grow_b
			(& ui->uix->console->output, len + 2))
		{
			return	HNEF_FR_FAIL_ALLOC;
		}

/* SPLint snprintf is not in C89 */ /*@i1@*/\
		if	(sprintf(ui->uix->console->output,
			"%s%d", HNEF_UI_ERROR_FRX, (int)frxcode) < 0)
		{
			return	HNEF_FR_FAIL_IO_WRITE;
		}

		ui->uix->console->output_error	= HNEF_TRUE;
		return	hnef_uix_paint_console_out(ui, frx, HNEF_FALSE);
	}
	else
	{
		return	hnef_uix_console_out_print
			(ui, frx, value, HNEF_TRUE);
	}
}

static
enum HNEF_FR
hnef_callback_print_langkey (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const ui,
/*@in@*/
/*@notnull@*/
	enum HNEF_FRX	* const frx,
/*@in@*/
/*@notnull@*/
	const char	* const key
	)
/*@modifies * ui, * frx@*/
{
	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->lang);
	assert	(NULL != frx);
	assert	(NULL != key);

	return	hnef_uix_print_langkey(ui, frx, key, HNEF_TRUE);
}

static
enum HNEF_FR
hnef_callback_print_fr (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const ui,
/*@in@*/
/*@notnull@*/
	enum HNEF_FRX		* const frx,
	const enum HNEF_FR	fr
	)
/*@globals errno@*/
/*@modifies errno, * ui, * frx@*/
{
	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->lang);
	assert	(NULL != frx);
	assert	(HNEF_FR_SUCCESS != fr);

	return	hnef_uix_print_fr(ui, frx, fr);
}

static
enum HNEF_FR
hnef_callback_print_fail_rread (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	enum HNEF_FRX		* const HNEF_RSTR frx,
	const enum HNEF_RREAD	rread
	)
/*@globals errno@*/
/*@modifies errno, * ui, * frx@*/
{
	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->lang);
	assert	(NULL != frx);
	assert	(HNEF_RREAD_SUCCESS != rread);

	return	hnef_uix_print_fail_rread(ui, frx, rread);
}

static
enum HNEF_FR
hnef_callback_print_fail_rvalid (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	enum HNEF_FRX		* const HNEF_RSTR frx,
	const enum HNEF_RVALID	rvalid
	)
/*@globals errno@*/
/*@modifies errno, * ui, * frx@*/
{
	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->lang);
	assert	(NULL != frx);
	assert	(HNEF_RVALID_SUCCESS != rvalid);

	return	hnef_uix_print_fail_rvalid(ui, frx, rvalid);
}

static
enum HNEF_FR
hnef_uix_parseline_game_load (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@in@*/
/*@notnull@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem, internalState@*/
/*@modifies errno, fileSystem, internalState, * ui, * frx@*/
{
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;
	HNEF_BOOL	ignored;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != line);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	fr	= hnef_ui_parseline_game_load(ui, line, & ignored,
			hnef_callback_print_langkey,
			hnef_callback_print_fr,
			frx);
	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good(* frx))
	{
		return	fr;
	}

	/*
	 * When loading a game, the game is destroyed regardless if the
	 * load succeeds or not, so we need a full repaint in either
	 * case. Therefore we ignore the `success` parameter.
	 */
	return	hnef_uix_paint(ui, frx, HNEF_TRUE);
}

static
enum HNEF_FR
hnef_uix_parseline_game_save (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, * ui, * frx@*/
{
	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != line);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	return	hnef_ui_parseline_game_save(ui, line,
			hnef_callback_print_langkey,
			hnef_callback_print_fr,
			frx);
}

static
enum HNEF_FR
hnef_uix_parseline_game_undo (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno@*/
/*@modifies errno, * ui, * frx@*/
{
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;
	HNEF_BOOL	success;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != line);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	/*
	 * If we clear the paintmap, the undone pieces on the board will
	 * be repainted because they will be different than the pieces
	 * in the paintmap after undoing (like when moving pieces
	 * normally), but the markers will all have to be cleared,
	 * because otherwise they may not be repainted.
	 *
	 * However, clearing the markers have to be done before we undo,
	 * or they won't be properly cleared since the move list will
	 * have changed (so clearing the last move won't be accurate).
	 * Thus the markers will be cleared when the user gives an
	 * invalid undo command (invalid amount of moves). That's not
	 * desirable (the markers should remain if the undo command is
	 * invalid), so we just do a full repaint.
	 *
	 * The following code has been removed (it allows us to pass a
	 * `HNEF_FALSE` `force_repaint` parameter to
	 * `hnef_uix_paint_board()`):
	 *
	 *	hnef_uix_marker_clear_all(ui);
	 *
	 * This is only done upon successful undo now, before a full
	 * repaint.
	 */

	fr	= hnef_ui_parseline_game_undo(ui, line, & success,
			hnef_callback_print_langkey,
			frx);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	if	(success)
	{
		hnef_uix_marker_clear_all(ui);
		fr	= hnef_uix_paint_board(ui, frx, HNEF_TRUE);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}
	}

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_uix_parseline_game_new (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
/*@modifies * ui, * frx@*/
{
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;

	assert	(NULL != ui);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	fr	= hnef_ui_parseline_game_new(ui,
			hnef_callback_print_langkey,
			frx);
	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good(* frx))
	{
		return	fr;
	}

	return	hnef_rvalid_good(hnef_game_valid(ui->game))
		? hnef_uix_paint_board(ui, frx, HNEF_TRUE)
		: HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
/* SPLint stdout is used *//*@i1@*/\
hnef_uix_parseline_lang (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout, * ui, * frx@*/
{
	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != line);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	/*
	 * Note that the output console is cleared when the user presses
	 * `<Enter>`, which he has to do in order to give commands, and
	 * this function call always follows a command. Thus the output
	 * console is cleared and we don't have to do it here.
	 */

	return	hnef_ui_parseline_lang(ui, line, HNEF_TRUE,
			hnef_callback_print_langkey,
			hnef_callback_print_fr,
			NULL,
			frx);
}

static
enum HNEF_FR
hnef_uix_parseline_rules (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem, internalState, stderr@*/
/*@modifies errno, fileSystem, internalState, stderr, * ui, * frx@*/
{
	enum HNEF_FR	fr			= HNEF_FR_SUCCESS;
	const char	* HNEF_RSTR ui_theme	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != line);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	if	(gleip_lines_size(line) > (size_t)2
	&&	!hnef_line_empty(gleip_lines_get_ro(line, (size_t)2)))
	{
		ui_theme	= gleip_lines_get_ro(line, (size_t)2);
	}

	fr	= hnef_ui_parseline_rules(ui, line, HNEF_TRUE,
			hnef_callback_print_langkey,
			hnef_callback_print_fr,
			hnef_callback_print_fail_rread,
			hnef_callback_print_fail_rvalid,
			NULL,
			frx);
	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good(* frx))
	{
		return	fr;
	}

	if	(hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		fr	= hnef_uix_game_update_rules(ui, ui_theme, frx);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}

		fr	= hnef_uix_paint(ui, frx, HNEF_TRUE);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}
	}
	/*
	 * else:	No change since `ui->game_tmp` doesn't replace
	 *		`ui->game`, so no need to update or paint.
	 */

	return	HNEF_FR_SUCCESS;
}

/*
 * Appends a player's translated name to the output console. Note that
 * the console is not cleared before doing this; the player's name is
 * appended to any text that's already in the console.
 */
static
enum HNEF_FR
hnef_uix_console_out_append_player (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const ui,
	const enum HNEF_CONTROL_TYPE	type
	)
/*@modifies * ui@*/
{
	const char	* HNEF_RSTR val	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->console);

	switch	(type)
	{
		case	HNEF_CONTROL_TYPE_HUMAN:
			val	= gleip_lang_getany(ui->lang,
					HNEF_LK_HELP_CLI_PLAYER2_H);
			break;
#ifdef	HNEFATAFL_UI_AIM
		case	HNEF_CONTROL_TYPE_AIM:
			val	= gleip_lang_getany(ui->lang,
					HNEF_LK_HELP_CLI_PLAYER2_M);
			break;
#endif	/* HNEFATAFL_UI_AIM */
		default:
			assert	(HNEF_FALSE);
			return	HNEF_FR_FAIL_ILL_STATE;
	}
	assert	(NULL != val);

/*@i1@*/\
	return	gleip_line_add_str_back_b
				(& ui->uix->console->output, val)
		? HNEF_FR_SUCCESS
		: HNEF_FR_FAIL_ALLOC;
}

static
enum HNEF_FR
hnef_callback_print_player (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const ui,
	const unsigned short	player_index,
/*@in@*/
/*@notnull@*/
	enum HNEF_FRX		* const frx
	)
/*@modifies * ui, * frx@*/
{
	enum HNEF_FR		fr		= HNEF_FR_SUCCESS;
	struct hnef_control	* HNEF_RSTR control	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(player_index < ui->game->playerc);
	assert	(NULL != frx);

	control	= ui->controlv[player_index];

	(void)hnef_uix_console_out_clear(ui);

	fr	= hnef_uix_console_out_append_player(ui, control->type);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	return	hnef_uix_paint_console_out(ui, frx, HNEF_FALSE);
}

static
enum HNEF_FR
hnef_callback_print_players (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const ui,
/*@in@*/
/*@notnull@*/
	enum HNEF_FRX	* const frx
	)
/*@modifies * ui, * frx@*/
{
	enum HNEF_FR		fr		= HNEF_FR_SUCCESS;
	unsigned short		i;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	(void)hnef_uix_console_out_clear(ui);

	for	(i = 0; i < ui->controlc; ++i)
	{
		struct hnef_control	* HNEF_RSTR control =
					ui->controlv[i];
		fr	= hnef_uix_console_out_append_player
			(ui, control->type);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}

		if	(i + 1 < ui->controlc)
		{
/*@i1@*/\
			if	(!gleip_line_add_str_back_b
				(& ui->uix->console->output,
					HNEF_UIX_OUTPUT_SEP))
			{
				return	HNEF_FR_FAIL_ALLOC;
			}
		}
	}

	return	hnef_uix_paint_console_out(ui, frx, HNEF_FALSE);
}

static
enum HNEF_FR
/* SPLint stdout is used *//*@i2@*/\
hnef_uix_parseline_player (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem, internalState, stdout@*/

/*@modifies errno, fileSystem, stdout, * ui,  * frx@*/
#ifdef	HNEFATAFL_UI_ZHASH
/*@modifies internalState@*/
#endif	/* HNEFATAFL_UI_ZHASH */
{
	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != line);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	return	hnef_ui_parseline_player(ui, line,
			hnef_callback_print_langkey,
			hnef_callback_print_player,
			hnef_callback_print_players,
			frx);
}

/*
 * The CLI has no themes, so this is unique to XLib.
 */
static
enum HNEF_FR
hnef_uix_parseline_theme (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem, internalState, stderr@*/
/*@modifies errno, fileSystem, internalState, stderr, * ui, * frx@*/
{
	const char	* HNEF_RSTR ui_theme	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != line);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	if	(!hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		return	hnef_uix_print_langkey(ui, frx,
			HNEF_LK_CMD_GAME_INVALID, HNEF_TRUE);
	}

	if	(gleip_lines_size(line) > (size_t)1
	&&	!hnef_line_empty(ui_theme =
			gleip_lines_get_ro(line, (size_t)1)))
	{
		enum HNEF_FR fr	= hnef_uix_game_update_rules
				(ui, ui_theme, frx);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}

		fr	= hnef_uix_paint(ui, frx, HNEF_TRUE);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}
	}
	else
	{
		if	(ui->uix->themes->themec > 0)
		{
			size_t	i;

			/*
			 * Ignore return value since we repaint anyway.
			 */
			(void)hnef_uix_console_out_clear(ui);

			for (i = 0; i < ui->uix->themes->themec; ++i)
			{
				struct hnef_uix_theme
					* const HNEF_RSTR theme =
					ui->uix->themes->themev[i];

				assert	(NULL != theme);
				assert	(NULL != theme->ui_theme);

/*@i1@*/\
				if	(!gleip_line_add_str_back_b
					(& ui->uix->console->output,
					theme->ui_theme))
				{
					return	HNEF_FR_FAIL_ALLOC;
				}

				if (i + 1 < ui->uix->themes->themec
/*@i1@*/\
				&& !gleip_line_add_str_back_b
					(& ui->uix->console->output,
					HNEF_UIX_OUTPUT_SEP))
				{
					return	HNEF_FR_FAIL_ALLOC;
				}
			}
			return	hnef_uix_paint_console_out
				(ui, frx, HNEF_FALSE);
		}
		else
		{
			return	hnef_uix_print_langkey(ui, frx,
				HNEF_LK_XLIB_THEMES_NOT_FOUND,
				HNEF_TRUE);
		}
	}

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_uix_parseline_version (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
/*@globals errno@*/
/*@modifies errno, * ui, * frx@*/
{
	int	digits	= 0,
		tmp;
	size_t	len;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	(void)hnef_uix_console_out_clear(ui);

	tmp	= hnef_ui_digitc(HNEFATAFL_CORE_VERSION);
	digits	+= tmp;
	if	(tmp < 0)
	{
		return	HNEF_FR_FAIL_MATH;
	}

	tmp	= hnef_ui_digitc(HNEFATAFL_AIM_VERSION);
	digits	+= tmp;
	if	(tmp < 0)
	{
		return	HNEF_FR_FAIL_MATH;
	}

	tmp	= hnef_ui_digitc(HNEFATAFL_UI_VERSION);
	digits	+= tmp;
	if	(tmp < 0)
	{
		return	HNEF_FR_FAIL_MATH;
	}

	/*
	 * The text string will be:
	 *	"core <v>, aim <v>, uicx <v>"
	 */
	len	= (size_t)digits
		+ strlen(HNEF_UIX_OUTPUT_SEP) * 2
		+ strlen(hnef_core_id())
		+ strlen(hnef_aim_id())
		+ strlen(hnef_ui_id())
		+ (size_t)3;				/* Spaces */

	if (gleip_line_mem(ui->uix->console->output) < len + 2
/*@i1@*/\
	&& !gleip_line_mem_grow_b(& ui->uix->console->output, len + 2))
	{
		return	HNEF_FR_FAIL_ALLOC;
	}

/* SPLint snprintf is not in C89 */ /*@i1@*/\
	if (sprintf(ui->uix->console->output, "%s %ld%s%s %ld%s%s %ld",
			hnef_core_id(),	HNEFATAFL_CORE_VERSION,
				HNEF_UIX_OUTPUT_SEP,
			hnef_aim_id(),	HNEFATAFL_AIM_VERSION,
				HNEF_UIX_OUTPUT_SEP,
			hnef_ui_id(),	HNEFATAFL_UI_VERSION) < 0)
	{
		return	HNEF_FR_FAIL_IO_WRITE;
	}

	return	hnef_uix_paint_console_out(ui, frx, HNEF_FALSE);
}

static
enum HNEF_FR
hnef_uix_parseline_envvar (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@modifies * ui, * frx@*/
{
	const char	* HNEF_RSTR envvar	= NULL,
			* HNEF_RSTR envval	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != line);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	if	(gleip_lines_size(line) < (size_t)2
	||	hnef_line_empty(envvar =
				gleip_lines_get_ro(line, (size_t)1)))
	{
		gleip_line_rm(ui->uix->console->output);
		ui->uix->console->output_error	= HNEF_FALSE;

/*@-boolops@*/
		if (!gleip_line_add_str_back_b
				(& ui->uix->console->output,
				HNEF_UI_ENVVAR_LANG)
		|| !gleip_line_add_str_back_b
				(& ui->uix->console->output,
				HNEF_UIX_OUTPUT_SEP)
		|| !gleip_line_add_str_back_b
				(& ui->uix->console->output,
				HNEF_UI_ENVVAR_PATH)
		|| !gleip_line_add_str_back_b
				(& ui->uix->console->output,
				HNEF_UIX_OUTPUT_SEP)
		|| !gleip_line_add_str_back_b
				(& ui->uix->console->output,
				HNEF_UI_ENVVAR_RC)
		|| !gleip_line_add_str_back_b
				(& ui->uix->console->output,
				HNEF_UIX_OUTPUT_SEP)
		|| !gleip_line_add_str_back_b
				(& ui->uix->console->output,
				HNEF_UI_ENVVAR_RULES)
		|| !gleip_line_add_str_back_b
				(& ui->uix->console->output,
				HNEF_UIX_OUTPUT_SEP)
		|| !gleip_line_add_str_back_b
				(& ui->uix->console->output,
				HNEF_UI_ENVVAR_FONT))
		{
			return	HNEF_FR_FAIL_ALLOC;
		}
/*@=boolops@*/
		return	hnef_uix_paint_console_out(ui, frx, HNEF_FALSE);
	}

	assert	(NULL != envvar);
	assert	(!hnef_line_empty(envvar));

	envval	= getenv(envvar);

	if	(NULL == envval)
	{
		return	hnef_uix_console_out_print
			(ui, frx, HNEF_UI_VAR_UNSET, HNEF_FALSE);
	}
	else
	{
		gleip_line_rm(ui->uix->console->output);
		ui->uix->console->output_error	= HNEF_FALSE;

/*@-boolops@*/
		if (!gleip_line_add_ch_back_b
				(& ui->uix->console->output, '"')
		|| !gleip_line_add_str_back_b
				(& ui->uix->console->output, envval)
		|| !gleip_line_add_ch_back_b
				(& ui->uix->console->output, '"'))
		{
			return	HNEF_FR_FAIL_ALLOC;
		}
/*@=boolops@*/

		return	hnef_uix_paint_console_out(ui, frx, HNEF_FALSE);
	}
}

/*
 * If the path is successfully set, the theme file is updated (which is
 * only useful if the theme file couldn't be found earlier). However,
 * the theme is not updated, so if the board was painted using
 * placeholder graphics, then the user will have to switch theme
 * manually.
 */
static
enum HNEF_FR
hnef_uix_parseline_path (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, * ui, * frx@*/
{
	const char	* HNEF_RSTR patharg	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != line);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	if	(gleip_lines_size(line) > (size_t)1
	&&	!hnef_line_empty(patharg =
			gleip_lines_get_ro(line, (size_t)1)))
	{
		enum HNEF_FR	fr	= HNEF_FR_SUCCESS;
		HNEF_BOOL	success;

		assert	(NULL != patharg);
		assert	(!hnef_line_empty(patharg));

/*@i1@*/\
		if	(!gleip_line_cpy_b
				(& ui->filefind->abs_path_cmd, patharg))
		{
			return	HNEF_FR_FAIL_ALLOC;
		}

		fr	= hnef_uix_themes_init_file(ui, & success);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}
/*@i1@*/\
		else if	(!success)
		{
			fr	= hnef_uix_print_langkey(ui, frx,
				HNEF_LK_XLIB_THEMES_NOT_FOUND,
				HNEF_TRUE);
			if	(!hnef_fr_good(fr)
			||	!hnef_frx_good(* frx))
			{
				return	fr;
			}
		}
		return	HNEF_FR_SUCCESS;
	}
	else
	{
		const char	* HNEF_RSTR envval	= NULL;

		gleip_line_rm(ui->uix->console->output);
		ui->uix->console->output_error	= HNEF_FALSE;

		if	(!hnef_line_empty(ui->filefind->abs_path_cmd)
/*@i1@*/\
		&&	!gleip_line_add_str_back_b
				(& ui->uix->console->output,
				ui->filefind->abs_path_cmd))
		{
			return	HNEF_FR_FAIL_ALLOC;
		}

		if	(NULL != ui->filefind->abs_path)
		{
			if	(!hnef_line_empty
					(ui->uix->console->output)
/*@i1@*/\
			&&	!gleip_line_add_str_back_b
				(& ui->uix->console->output,
					HNEF_UIX_OUTPUT_SEP))
			{
				return	HNEF_FR_FAIL_ALLOC;
			}

/*@i1@*/\
			if	(!gleip_line_add_str_back_b
				(& ui->uix->console->output,
					ui->filefind->abs_path))
			{
				return	HNEF_FR_FAIL_ALLOC;
			}
		}

		if	(NULL != (envval = getenv(HNEF_UI_ENVVAR_PATH)))
		{
			if	(!hnef_line_empty
					(ui->uix->console->output)
/*@i1@*/\
			&&	!gleip_line_add_str_back_b
				(& ui->uix->console->output,
					HNEF_UIX_OUTPUT_SEP))
			{
				return	HNEF_FR_FAIL_ALLOC;
			}

/*@i1@*/\
			if	(!gleip_line_add_str_back_b
				(& ui->uix->console->output,
					envval))
			{
				return	HNEF_FR_FAIL_ALLOC;
			}
		}
		return	hnef_uix_paint_console_out(ui, frx, HNEF_FALSE);
	}
}

static
enum HNEF_FR
hnef_uix_parseline_movemarker (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
/*@modifies * ui, * frx@*/
{
	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	if	(!hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		return	hnef_uix_print_langkey(ui, frx,
			HNEF_LK_CMD_GAME_INVALID, HNEF_TRUE);
	}

	hnef_uix_marker_clear_last(ui);
	ui->uix->lastmove_show	= !ui->uix->lastmove_show;
	return	hnef_uix_paint_board(ui, frx, HNEF_FALSE);
}

static
enum HNEF_FR
hnef_uix_parseline_move (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno@*/
/*@modifies errno, * ui, * frx@*/
{
	/*
	 * This function is very similar but still very different from
	 * `cli.c::hnef_uic_parseline_move()`, so we don't share it with
	 * callbacks. Much of this is code repetition.
	 */

	const struct hnef_ruleset	* HNEF_RSTR rules	= NULL;
	enum HNEF_FR			fr	= HNEF_FR_SUCCESS;
	HNEF_BOOL			legal;
	unsigned short			pos,
					dest,
					ignored	= HNEF_PLAYER_UNINIT;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != line);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	if	(!hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		return	hnef_uix_print_langkey(ui, frx,
			HNEF_LK_CMD_GAME_INVALID, HNEF_TRUE);
	}

	assert	(NULL != ui->uix->paintmap);
	assert	(ui->uix->paintmap_width == ui->game->rules->bwidth);
	assert	(ui->uix->paintmap_height == ui->game->rules->bheight);

	rules	= ui->game->rules;

	if	(gleip_lines_size(line) >= (size_t)5)
	{
		unsigned short	x1	= HNEF_BOARDPOS_NONE,
				y1	= HNEF_BOARDPOS_NONE,
				x2	= HNEF_BOARDPOS_NONE,
				y2	= HNEF_BOARDPOS_NONE;
		if	(!hnef_texttoushort(gleip_lines_get_ro(line,
						(size_t)1), & x1)
		||	!hnef_texttoushort(gleip_lines_get_ro(line,
						(size_t)2), & y1)
		||	!hnef_texttoushort(gleip_lines_get_ro(line,
						(size_t)3), & x2)
		||	!hnef_texttoushort(gleip_lines_get_ro(line,
						(size_t)4), & y2)
		||	x1 >= rules->bwidth
		||	y1 >= rules->bheight
		||	x2 >= rules->bwidth
		||	y2 >= rules->bheight)
		{
			return	hnef_uix_print_langkey(ui, frx,
				HNEF_LK_CMD_MOVE_COORD, HNEF_TRUE);
		}

		pos	= (unsigned short)(y1 * rules->bwidth + x1);
		dest	= (unsigned short)(y2 * rules->bwidth + x2);
		assert	(pos < rules->opt_blen);
		assert	(dest < rules->opt_blen);

		fr	= hnef_game_move(ui->game, pos, dest, & legal);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}

		if	(legal)
		{
			hnef_uix_marker_clear_all(ui);

			if (hnef_game_over(ui->game, & ignored))
			{
				fr	= hnef_uix_print_langkey
					(ui, frx,
					HNEF_LK_CMD_GAME_OVER,
						HNEF_FALSE);
				if	(!hnef_fr_good(fr)
				||	!hnef_frx_good(* frx))
				{
					return	fr;
				}
			}

			return	hnef_uix_paint_board
				(ui, frx, HNEF_FALSE);
		}
		else
		{
			return	hnef_uix_print_langkey(ui, frx,
				HNEF_LK_CMD_MOVE_ILLEGAL, HNEF_TRUE);
		}
	}
	else if	(gleip_lines_size(line) >= (size_t)3)
	{
		unsigned short	x	= HNEF_BOARDPOS_NONE,
				y	= HNEF_BOARDPOS_NONE;
		size_t		i;

		if	(hnef_game_over(ui->game, & ignored))
		{
			return	hnef_uix_print_langkey(ui, frx,
				HNEF_LK_CMD_GAME_OVER, HNEF_TRUE);
		}

		if	(!hnef_texttoushort(gleip_lines_get_ro(line,
							(size_t)1), & x)
		||	!hnef_texttoushort(gleip_lines_get_ro(line,
							(size_t)2), & y)
		||	x >= rules->bwidth
		||	y >= rules->bheight)
		{
			return	hnef_uix_print_langkey(ui, frx,
				HNEF_LK_CMD_MOVE_COORD, HNEF_TRUE);
		}
		pos	= (unsigned short)(y * rules->bwidth + x);
		assert	(pos < rules->opt_blen);

		hnef_uix_marker_clear_movelist(ui);

		assert	(ui->opt_movelist->elemc < (size_t)1);
		fr	= hnef_game_moves_get_pos(ui->game,
				ui->opt_movelist, pos);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}

		for	(i = 0; i < ui->opt_movelist->elemc; ++i)
		{
			struct hnef_move * const HNEF_RSTR move =
					& ui->opt_movelist->elems[i];
			ui->uix->paintmap[move->pos] =
				HNEF_BIT_U8_REPAINT;
			ui->uix->paintmap[move->dest] =
				HNEF_BIT_U8_REPAINT;
		}

		return	hnef_uix_paint_board(ui, frx, HNEF_FALSE);
	}
	else
	{
		return	hnef_uix_parseline_movemarker(ui, frx);
	}
}

/*
 * `exists` is set to false if `cmd` is unknown.
 */
static
enum HNEF_FR
hnef_uix_print_help_single (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
	const char	cmd,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL	* const HNEF_RSTR exists,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
/*@modifies * ui, * exists, * frx@*/
{
	const char	* HNEF_RSTR key	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != exists);
	assert	(NULL != frx);

	* frx		= HNEF_FRX_SUCCESS;
	* exists	= HNEF_TRUE;

	switch	(cmd)
	{
		case	HNEF_CMD_COPY:
			key	= HNEF_LK_HELP_CLI_COPY;
			break;
		case	HNEF_CMD_ENVVAR:
			key	= HNEF_LK_HELP_CLI_ENVVAR1;
			break;
		case	HNEF_CMD_FORCE:
			key	= HNEF_LK_HELP_XLIB_FORCE;
			break;
		case	HNEF_CMD_GAME_LOAD:
			key	= HNEF_LK_HELP_CLI_GAME_LOAD;
			break;
		case	HNEF_CMD_GAME_NEW:
			key	= HNEF_LK_HELP_CLI_GAME_NEW;
			break;
		case	HNEF_CMD_GAME_SAVE:
			key	= HNEF_LK_HELP_CLI_GAME_SAVE;
			break;
		case	HNEF_CMD_GAME_UNDO:
			key	= HNEF_LK_HELP_CLI_GAME_UNDO1;
			break;
		case	HNEF_CMD_HELP:
			key	= HNEF_LK_HELP_CLI_HELP1;
			break;
		case	HNEF_CMD_LANG:
			key	= HNEF_LK_HELP_CLI_LANG;
			break;
		case	HNEF_CMD_MOVE:
			key	= HNEF_LK_HELP_XLIB_MOVE;
			break;
		case	HNEF_CMD_PATH:
			key	= HNEF_LK_HELP_CLI_PATH1;
			break;
		case	HNEF_CMD_PLAYER:
			key	= HNEF_LK_HELP_CLI_PLAYER2;
			break;
		case	HNEF_CMD_PRINT:
			key	= HNEF_LK_HELP_XLIB_PRINT;
			break;
		case	HNEF_CMD_RULES:
			key	= HNEF_LK_HELP_XLIB_RULES;
			break;
		case	HNEF_CMD_QUIT:
			key	= HNEF_LK_HELP_CLI_QUIT;
			break;
		case	HNEF_CMD_THEME:
			key	= HNEF_LK_HELP_XLIB_THEME;
			break;
		case	HNEF_CMD_VERSION:
			key	= HNEF_LK_HELP_CLI_VERSION;
			break;
		case	HNEF_CMD_XLIB:
			key	= HNEF_LK_HELP_XLIB_XLIB;
			break;
		default:
			* exists	= HNEF_FALSE;
			return		HNEF_FR_SUCCESS;
	}
	assert	(NULL != key);
	return	hnef_uix_print_langkey(ui, frx, key, HNEF_FALSE);
}

static
enum HNEF_FR
hnef_uix_print_help_all (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
/*@modifies * ui, * frx@*/
{
	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	(void)hnef_uix_console_out_clear(ui);

/*@-boolops@*/
	if	(!gleip_line_add_str_back_b(& ui->uix->console->output,
					"h <c>, <c> ∈ ")
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_PRINT)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_PATH)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_GAME_LOAD)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_FORCE)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_GAME_NEW)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_HELP)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_COPY)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_LANG)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_MOVE)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_ENVVAR)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_PLAYER)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_QUIT)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_RULES)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_THEME)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_GAME_UNDO)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_VERSION)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_GAME_SAVE)
	||	!gleip_line_add_ch_back_b(& ui->uix->console->output,
				HNEF_CMD_XLIB))
	{
		return	HNEF_FR_FAIL_ALLOC;
	}
/*@=boolops@*/

	return	hnef_uix_paint_console_out(ui, frx, HNEF_FALSE);
}

static
enum HNEF_FR
hnef_uix_parseline_help (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@modifies * ui, * frx@*/
{
	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != line);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	if	(gleip_lines_size(line) > (size_t)1
	&&	!hnef_line_empty(gleip_lines_get_ro(line, (size_t)1)))
	{
		HNEF_BOOL	exists;
		const enum HNEF_FR fr = hnef_uix_print_help_single
				(ui, gleip_lines_get_ro
				(line, (size_t)1)[0], & exists, frx);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}

		return	exists
			? HNEF_FR_SUCCESS
			: hnef_uix_print_help_all(ui, frx);
	}
	else
	{
		return	hnef_uix_print_help_all(ui, frx);
	}
}

static
enum HNEF_FR
hnef_uix_command_parseline (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
#ifdef	HNEFATAFL_UI_AIM
	,
	const HNEF_BOOL	ai_callback
#endif	/* HNEFATAFL_UI_AIM */
	)
/*@globals errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies * ui, * frx@*/
{
	struct gleip_lines	* GLEIP_RSTR line_tkns	= NULL;
	const char		* lineptr		= NULL;
	int			retval			= 1;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->console);
	assert	(!hnef_line_empty(ui->uix->console->input));
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	* frx		= HNEF_FRX_SUCCESS;
	lineptr		= ui->uix->console->input;
	line_tkns	= ui->uix->console->input_tokens;

	gleip_lines_rm(line_tkns);
	retval	= gleip_lineread_interp_cstr_def
		(line_tkns, & lineptr, 0);
/*@i1@*/\
	if	(!retval)
	{
		return	HNEF_FR_FAIL_ALLOC;
	}

	if	(gleip_lines_size(line_tkns) > 0
	&&	!hnef_line_empty(gleip_lines_get_ro(line_tkns, 0)))
	{
		switch	(gleip_lines_get_ro(line_tkns, 0)[0])
		{
			case	HNEF_CMD_COPY:
				return	hnef_uix_console_out_print
					(ui, frx, HNEF_UIX_COPY_BRIEF,
					HNEF_FALSE);
			case	HNEF_CMD_GAME_UNDO:
#ifdef	HNEFATAFL_UI_AIM
				if	(ai_callback)
				{
					(void)hnef_beep();
					return	HNEF_FR_SUCCESS;
				}
#endif	/* HNEFATAFL_UI_AIM */
				return	hnef_uix_parseline_game_undo
					(ui, line_tkns, frx);
			case	HNEF_CMD_EE_HINT:
				return	hnef_uix_console_out_print
					(ui, frx, hnef_nine_hint(),
					HNEF_FALSE);
			case	HNEF_CMD_EE_NINE:
				return	hnef_uix_console_out_print
					(ui, frx, hnef_nine_brief
				(gleip_lines_size(line_tkns) > (size_t)1
						? gleip_lines_get_ro
						(line_tkns, (size_t)1)
						: NULL),
					HNEF_FALSE);
			case	HNEF_CMD_ENVVAR:
				return	hnef_uix_parseline_envvar
					(ui, line_tkns, frx);
			case	HNEF_CMD_GAME_LOAD:
#ifdef	HNEFATAFL_UI_AIM
				if	(ai_callback)
				{
					(void)hnef_beep();
					return	HNEF_FR_SUCCESS;
				}
#endif	/* HNEFATAFL_UI_AIM */
				return	hnef_uix_parseline_game_load
					(ui, line_tkns, frx);
			case	HNEF_CMD_GAME_SAVE:
#ifdef	HNEFATAFL_UI_AIM
				if	(ai_callback)
				{
					(void)hnef_beep();
					return	HNEF_FR_SUCCESS;
				}
#endif	/* HNEFATAFL_UI_AIM */
				return	hnef_uix_parseline_game_save
					(ui, line_tkns, frx);
			case	HNEF_CMD_GAME_NEW:
#ifdef	HNEFATAFL_UI_AIM
				if	(ai_callback)
				{
					(void)hnef_beep();
					return	HNEF_FR_SUCCESS;
				}
#endif	/* HNEFATAFL_UI_AIM */
				return	hnef_uix_parseline_game_new
					(ui, frx);
			case	HNEF_CMD_HELP:
				return	hnef_uix_parseline_help
					(ui, line_tkns, frx);
			case	HNEF_CMD_LANG:
				return	hnef_uix_parseline_lang
					(ui, line_tkns, frx);
			case	HNEF_CMD_MOVE:
#ifdef	HNEFATAFL_UI_AIM
				if	(ai_callback)
				{
					if (gleip_lines_size(line_tkns)
						>= (size_t)3)
					{
						(void)hnef_beep();
						return	HNEF_FR_SUCCESS;
					}
					else
					{
						return
					hnef_uix_parseline_movemarker
							(ui, frx);
					}
				}
#endif	/* HNEFATAFL_UI_AIM */
				return	hnef_uix_parseline_move
					(ui, line_tkns, frx);
			case	HNEF_CMD_PATH:
				return	hnef_uix_parseline_path
					(ui, line_tkns, frx);
			case	HNEF_CMD_PLAYER:
#ifdef	HNEFATAFL_UI_AIM
				if	(ai_callback)
				{
					(void)hnef_beep();
					return	HNEF_FR_SUCCESS;
				}
#endif	/* HNEFATAFL_UI_AIM */
				return	hnef_uix_parseline_player
					(ui, line_tkns, frx);
			case	HNEF_CMD_PRINT:
				return	hnef_uix_paint_board
					(ui, frx, HNEF_TRUE);
			case	HNEF_CMD_QUIT:
				ui->quit		= HNEF_TRUE;
				return	HNEF_FR_SUCCESS;
			case	HNEF_CMD_RULES:
#ifdef	HNEFATAFL_UI_AIM
				if	(ai_callback)
				{
					(void)hnef_beep();
					return	HNEF_FR_SUCCESS;
				}
#endif	/* HNEFATAFL_UI_AIM */
				return	hnef_uix_parseline_rules
					(ui, line_tkns, frx);
			case	HNEF_CMD_THEME:
				return	hnef_uix_parseline_theme
					(ui, line_tkns, frx);
			case	HNEF_CMD_VERSION:
				return	hnef_uix_parseline_version
					(ui, frx);
			case	HNEF_CMD_XLIB:
				ui->uix->quit_xlib	= HNEF_TRUE;
				return	HNEF_FR_SUCCESS;
#ifdef	HNEFATAFL_UI_AIM
			case	HNEF_CMD_FORCE:
				if	(ai_callback)
				{
					/*
					 * `ai_callback` implies ongoing
					 * game.
					 */
					ui->uix->ai_force = HNEF_TRUE;
				}
				else
				{
					(void)hnef_beep();
				}
				return	HNEF_FR_SUCCESS;
#endif	/* HNEFATAFL_UI_AIM */
			default:
				(void)hnef_beep();
				return	hnef_uix_print_langkey(ui, frx,
					HNEF_LK_CMD_UNK, HNEF_TRUE);
		}
	}
	return	HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_uix_console_key_bkspace (
	struct hnef_ui	* const HNEF_RSTR ui,
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
{
	size_t	len,
		len_del	= 0;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->console);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	len	= strlen(ui->uix->console->input);

	if	(len < (size_t)1)
	{
		return	HNEF_FR_SUCCESS;
	}

	if	(ui->uix->font->use_fontset)
	{
		/*
		 * Multibyte string (probably UTF-8). We may have to
		 * delete several `char`s (bytes) because one multibyte
		 * symbol may consist of several `char`s.
		 *
		 * NOTE:	We assume that all `char`s in multi-byte
		 *		symbols are `!isprint()`.
		 */
		size_t		i;
		HNEF_BOOL	print_prev	= HNEF_TRUE,
				print_cur	= HNEF_TRUE;
		for	(i = len - 1; i < len && SIZET_MAX != i; --i)
		{
			const char ch	= ui->uix->console->input[i];
/*@i1@*/\
			const char * str = ui->uix->console->input + i;
			print_cur	= isprint((int)ch);

			if	(print_cur)
			{
				/*
				 * ASCII should always delimit deletion.
				 */
				++len_del;
				break;
			}
			else if	(len_del < (size_t)1)
			{
				/*
				 * `len_del == 0` is never acceptable,
				 * keep looking.
				 */
				++len_del;
				print_prev	= isprint((int)ch);
			}
			else if	(!print_prev
			&&	!print_cur
			&&	mblen(str, strlen(str)) > 0)
			{
				/*
				 * `isprint()` is always false for UTF-8
				 * characters (for leading and
				 * continuation bytes), so if the
				 * current and previous chars were
				 * non-printable, then they may be UTF-8
				 * bytes. They are definitely UTF-8
				 * bytes if `mblen()` returns a > 0
				 * length. So this delimits deletion.
				 */
				++len_del;
				break;
			}
			else
			{
				/*
				 * Continuation byte, keep looking.
				 */
				++len_del;
				print_prev	= isprint((int)ch);
			}
		}
	}
	else
	{
		/*
		 * Always delete 1 char with Latin-1 fallback because
		 * it's not a multi-byte encoding.
		 */
		len_del	= (size_t)1;
	}

	assert	(len_del > 0);
	assert	(len_del <= len);

	gleip_line_rm_from(ui->uix->console->input, len - len_del);
	return	hnef_uix_paint_console_in(ui, frx, HNEF_FALSE);
}

enum HNEF_FR
hnef_uix_console_key_enter (
	struct hnef_ui	* const HNEF_RSTR ui,
	enum HNEF_FRX	* const HNEF_RSTR frx
#ifdef	HNEFATAFL_UI_AIM
	,
	const HNEF_BOOL	ai_callback
#endif	/* HNEFATAFL_UI_AIM */
	)
{
	size_t		len;
	HNEF_BOOL	repaint_out;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->console);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	repaint_out	= hnef_uix_console_out_clear(ui);

	len	= strlen(ui->uix->console->input);
	if	(len > 0)
	{
		enum HNEF_FR	fr	= HNEF_FR_SUCCESS;
#ifndef	NDEBUG
		HNEF_BOOL	repaint_in;
#endif	/* NDEBUG */

		fr	= hnef_uix_command_parseline(ui, frx
#ifdef	HNEFATAFL_UI_AIM
			, ai_callback
#endif	/* HNEFATAFL_UI_AIM */
			);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}

#ifndef	NDEBUG
		repaint_in =
#else
		(void)
#endif	/* NDEBUG */
			hnef_uix_console_in_clear(ui);
#ifndef	NDEBUG
		assert	(repaint_in);
#endif	/* NDEBUG */
		fr = hnef_uix_paint_console_in(ui, frx, HNEF_FALSE);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}
	}

	return	repaint_out
		? hnef_uix_paint_console_out(ui, frx, HNEF_FALSE)
		: HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_uix_console_key_escape (
	struct hnef_ui	* const HNEF_RSTR ui,
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
{
	size_t		len;
	HNEF_BOOL	repaint_out;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->console);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	repaint_out	= hnef_uix_console_out_clear(ui);

	len	= strlen(ui->uix->console->input);
	if	(len > 0)
	{
		enum HNEF_FR	fr = HNEF_FR_SUCCESS;
#ifndef	NDEBUG
		HNEF_BOOL	repaint_in =
#else
		(void)
#endif	/* NDEBUG */
				hnef_uix_console_in_clear(ui);
#ifndef	NDEBUG
		assert	(repaint_in);
#endif	/* NDEBUG */

		fr = hnef_uix_paint_console_in(ui, frx, HNEF_FALSE);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}
	}

	return	repaint_out
		? hnef_uix_paint_console_out(ui, frx, HNEF_FALSE)
		: HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_uix_console_append (
	struct hnef_ui	* const HNEF_RSTR ui,
	enum HNEF_FRX	* const HNEF_RSTR frx,
	const char	* const HNEF_RSTR str
	)
{
	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->console);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);
	assert	(NULL != str);

	* frx	= HNEF_FRX_SUCCESS;

/*@i1@*/\
	if (!gleip_line_add_str_back_b(& ui->uix->console->input, str))
	{
		return	HNEF_FR_FAIL_ALLOC;
	}

	return	hnef_uix_paint_console_in(ui, frx, HNEF_FALSE);
}

struct hnef_uix_console *
hnef_alloc_uix_console (void)
{
	struct hnef_uix_console	* const HNEF_RSTR console =
			malloc(sizeof(* console));
	if	(NULL == console)
	{
		return	NULL;
	}

	console->output_error			= HNEF_FALSE;
	console->output_error_last		= HNEF_FALSE;
	console->input_computer_turn_last	= HNEF_FALSE;

	console->input		= gleip_line_alloc();
	if	(NULL == console->input)
	{
		free	(console);
		return	NULL;
	}

	console->input_echo	= gleip_line_alloc();
	if	(NULL == console->input_echo)
	{
		free	(console->input);
		free	(console);
		return	NULL;
	}

	console->output		= gleip_line_alloc();
	if	(NULL == console->output)
	{
		free	(console->input_echo);
		free	(console->input);
		free	(console);
		return	NULL;
	}

	console->input_last	= gleip_line_alloc();
	if	(NULL == console->input_last)
	{
		free	(console->output);
		free	(console->input_echo);
		free	(console->input);
		free	(console);
		return	NULL;
	}

	console->output_last	= gleip_line_alloc();
	if	(NULL == console->output_last)
	{
		free	(console->input_last);
		free	(console->output);
		free	(console->input_echo);
		free	(console->input);
		free	(console);
		return	NULL;
	}

	console->input_tokens	= gleip_lines_alloc();
	if	(NULL == console->input_tokens)
	{
		free	(console->output_last);
		free	(console->input_last);
		free	(console->output);
		free	(console->input_echo);
		free	(console->input);
		free	(console);
		return	NULL;
	}

	return	console;
}

void
hnef_free_uix_console (
	struct hnef_uix_console	* const HNEF_RSTR console
	)
{
	assert	(NULL != console);
	assert	(NULL != console->input);
	assert	(NULL != console->input_echo);
	assert	(NULL != console->output);
	assert	(NULL != console->input_last);
	assert	(NULL != console->output_last);

	gleip_lines_free(console->input_tokens);
	free		(console->output_last);
	free		(console->input_last);
	free		(console->output);
	free		(console->input_echo);
	free		(console->input);
	free		(console);
}

#endif	/* HNEFATAFL_UI_XLIB */

