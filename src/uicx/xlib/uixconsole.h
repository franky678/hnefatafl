/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#ifndef HNEF_UI_UIXCONSOLE_H
#define HNEF_UI_UIXCONSOLE_H

#include "funcxt.h"		/* HNEF_FRX */
#include "uit.h"		/* hnef_ui */
#include "uixconsolet.h"	/* hnef_uix_console */

/*@-protoparamname@*/
extern
HNEF_BOOL
hnef_uix_console_clear (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui
	)
/*@modifies * ui@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_uix_print_langkey (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const ui,
/*@in@*/
/*@notnull@*/
	enum HNEF_FRX	* const frx,
/*@in@*/
/*@notnull@*/
	const char	* const key,
	const HNEF_BOOL	error
	)
/*@modifies * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_uix_print_fail_frx (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	enum HNEF_FRX		* const HNEF_RSTR frx,
	const enum HNEF_FRX	frxcode
	)
/*@globals errno@*/
/*@modifies errno, * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_uix_console_key_bkspace (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
/*@modifies * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_uix_console_key_enter (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
#ifdef	HNEFATAFL_UI_AIM
	,
	const HNEF_BOOL	ai_callback
#endif	/* HNEFATAFL_UI_AIM */
	)
/*@globals errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies * ui, ui->quit, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_uix_console_key_escape (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
/*@modifies * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_uix_console_append (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx,
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR str
	)
/*@modifies * ui, * frx@*/
;
/*@=protoparamname@*/

/*@in@*/
/*@null@*/
/*@only@*/
extern
struct hnef_uix_console *
hnef_alloc_uix_console (void)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
extern
void
hnef_free_uix_console (
/*@in@*/
/*@notnull@*/
/*@owned@*/
/*@special@*/
	struct hnef_uix_console	* const HNEF_RSTR console
	)
/*@modifies console@*/
/*@releases console@*/
;
/*@=protoparamname@*/

#endif

#endif	/* HNEFATAFL_UI_XLIB */

