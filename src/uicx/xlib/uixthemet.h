/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#ifndef HNEF_UI_UIXTHEMET_H
#define HNEF_UI_UIXTHEMET_H

#include "X11/Xlib.h"	/* XImage, clipmask */

#include "hnefatafl.h"

/*@unchecked@*/
extern
const int	HNEF_THEME_SIZE_INVALID;

/*
 * Both width and height of `hnef_uix_img->image` must be > 0.
 *
 * If `square`, all `width`s must equal in the same theme, and the same
 * goes for all `height`s. (However, width does not have to equal
 * height.)
 *
 * If `piece`, then no `width` can exceed the square width in any theme,
 * and the same goes for `height`.
 *
 * These structs are immutable once allocated/initialized (which is
 * done by `hnef_alloc_uix_img_init(...)`).
 */
struct hnef_uix_img
{

	/*
	 * Can't be `HNEF_BIT_U8_EMPTY`. This either a bit or, if
	 * `ui_bit` is defined, an `ui_bit`.
	 */
	HNEF_BIT_U8	ui_bit;

	/*
	 * `square` is `true` if the image depicts a `hnef_type_square`,
	 * false if it depicts a `hnef_type_piece`.
	 *
	 * `transparent` is not allowed to be true if `square`. It may
	 * or may not be true if `!square`. The clipmask is defined only
	 * if `transparent` -- else the clipmask variable may not be
	 * accessed.
	 */
	HNEF_BOOL	square,
			transparent;

/*@notnull@*/
/*@owned@*/
	XImage		* image;

	Pixmap		clipmask;

};

/*
 * The `hnef_uix_img`s in each theme are allocated and initialized
 * lazily when a ruleset that uses them is in use when `hnef_run_xlib()`
 * is called.
 */
struct hnef_uix_theme
{

	/*
	 * Pointers < `piecec` are always `!NULL`. Pointers >= `piecec`
	 * may or may not be `NULL`.
	 *
	 * Once images are added here, they are never removed until the
	 * whole `hnef_uix_theme` struct is freed (for the sake of
	 * simplicity, although it means we may needlessly be iterating
	 * through images that will never be used if we load 1 ruleset
	 * which requires many images and then load another one which
	 * requires fewer images).
	 *
	 * The image structs are allocated lazily as needed by every
	 * individual ruleset (so only the XPM files that the ruleset
	 * needs will be loaded, and the other ones may be loaded at a
	 * later point).
	 */
/*@in@*/
/*@notnull@*/
/*@owned@*/
	struct hnef_uix_img	* * piecev,
				* * squarev;

	size_t			piecec,
				piecec_mem,
				squarec,
				squarec_mem;

	/*
	 * `ui_theme` is the name of the theme, as in the ruleset file.
	 *
	 * `datadir` is the directory in which the theme order file is.
	 * It does not end with a path separator. If the theme order
	 * file is `themes`, and the absolute path is
	 * `/usr/share/hnefatafl/xlib/themes`, then `datadir` is
	 * `/usr/share/hnefatafl/xlib/` (note that it contains the path
	 * separator, not that it matters on POSIX since several path
	 * separators ("/////") count as one ("/") unless they are in
	 * the beginning of the path).
	 */
/*@in@*/
/*@notnull@*/
/*@owned@*/
	char			* ui_theme,
				* datadir;

	/*
	 * The width and height of a square in this theme.
	 *
	 * The width is the largest width of any `hnef_uix_img->image`
	 * in `squarev`. The height is the largest height of any
	 * `hnef_uix_img->image` in the same array. This is set when
	 * the ruleset is loaded (by `hnef_uix_themes_load_rules()`).
	 *
	 * These are a negative number if just initialized, unset or
	 * invalid.
	 */
	int	square_width,
		square_height;

};

struct hnef_uix_themes
{

	/*
	 * Pointers < `themec` are always `!NULL`. Pointers >= `themec`
	 * may or may not be `NULL`.
	 */
/*@in@*/
/*@notnull@*/
/*@owned@*/
	struct hnef_uix_theme	* * themev;

	/*
	 * `themei_cur` is the currently loaded theme, or `SIZET_MAX` if
	 * none is loaded or if `themec < 1`. This is set to 0 as the
	 * default value if X starts without a theme argument.
	 */
	size_t			themec,
				themec_mem,
				themei_cur;

};

#endif

#endif	/* HNEFATAFL_UI_XLIB */

