/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CLIIOT_H
#define HNEF_CLIIOT_H

#define	HNEF_CMD_ARG_DEF		'-'
#define	HNEF_CMD_COPY			'i'
#define	HNEF_CMD_EE_NINE		'9'
#define	HNEF_CMD_EE_HINT		'?'
#define	HNEF_CMD_ENVVAR			'n'
#define	HNEF_CMD_FORCE			'f'
#define	HNEF_CMD_GAME_LOAD		'e'
#define	HNEF_CMD_GAME_NEW		'g'
#define	HNEF_CMD_GAME_SAVE		'w'
#define	HNEF_CMD_GAME_UNDO		'u'
#define	HNEF_CMD_HELP			'h'
#define	HNEF_CMD_HELP_CLI		'c'
#define	HNEF_CMD_HELP_XLIB		'x'
#define	HNEF_CMD_LANG			'l'
#define	HNEF_CMD_LANG_PRINT		'L'
#define	HNEF_CMD_LOCALE_SUPPRESS	's'
#define	HNEF_CMD_MOVE			'm'
#define	HNEF_CMD_PATH			'd'
#define	HNEF_CMD_PLAYER			'p'
#define	HNEF_CMD_PLAYER_AIM		'm'
#define	HNEF_CMD_PLAYER_HUMAN		'h'
#define	HNEF_CMD_PRINT			'b'
#define	HNEF_CMD_THEME			't'
#define	HNEF_CMD_QUIT			'q'
#define	HNEF_CMD_RC			'c'
#define	HNEF_CMD_RC_SUPPRESS		'C'
#define	HNEF_CMD_RULES			'r'
#define	HNEF_CMD_VERSION		'v'
#define	HNEF_CMD_XLIB			'x'

/*@observer@*/
/*@unchecked@*/
/*@unused@*/	/* HNEF_UI_ERROR_FRX is unused without X */
extern
const char	* const	HNEF_UI_PROMPT,
		* const HNEF_UI_VAR_UNSET,
		* const HNEF_UI_ERROR_FR,
		* const HNEF_UI_ERROR_RREAD,
		* const HNEF_UI_ERROR_RVALID,
		* const HNEF_UI_ERROR_FRX;

#endif

