/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_AIM_MINIMAXT_H
#define HNEF_AIM_MINIMAXT_H

#include "hnefatafl.h"

#include "config.h"	/* HNEFATAFL_AIM_ZHASH */

/*
 * Maximum value for aiminimax->depth_max.
 *
 * NOTE:	This must be a (compile-time) constant (because
 *		HNEF_AIM_VALUE_WIN in aimalgo.c needs it), hence define
 *		rather than const.
 */
#define	HNEF_AIM_DEPTHMAX_MAX	(unsigned short)1000

/*@unchecked@*/
/*@unused@*/
extern
const unsigned short	HNEF_AIM_DEPTHMAX_MIN,
			HNEF_AIM_DEPTHMAX_UNINIT;

/*@exposed@*/
struct hnef_aiminimax
{

	/*
	 * Owning player's index.
	 */
	unsigned short		p_index;

	/*
	 * Maximum search depth.
	 */
	unsigned short		depth_max;

#ifdef	HNEFATAFL_AIM_ZHASH
	/*
	 * Transposition table.
	 */
/*@dependent@*/
/*@in@*/
/*@notnull@*/
	struct hnef_zhashtable	* tp_tab;
#endif	/* HNEFATAFL_AIM_ZHASH */

	/*
	 * Array that has the same length as ruleset->opt_blen, which
	 * contains values for squares depending on their proximity to
	 * escape squares.
	 */
/*@in@*/
/*@notnull@*/
/*@owned@*/
	int			* board_escval;

	/*
	 * Optimization.
	 *
	 * This is a list of moves that is derived from the original
	 * board passed to aiminimax_command, so we don't have to
	 * re-allocate it all the time.
	 */
/*@in@*/
/*@owned@*/
/*@notnull@*/
	struct hnef_listm	* opt_moves;

	/*
	 * Optimization.
	 *
	 * Move history.
	 */
/*@in@*/
/*@owned@*/
/*@notnull@*/
	struct hnef_listmh	* opt_movehist;

	/*
	 * Optimization.
	 *
	 * Pointer to a list of listm structs. The amount of listm
	 * structs is depth_max + 1.
	 *
	 * In the aiminimax_min() and aiminimax_max() functions, these
	 * lists are used (specifically: opt_buf_moves[depth_cur]) so
	 * they don't have to be re-allocated all the time.
	 * opt_buf_moves[0] is used in the initial call to
	 * aiminimax_min() in aiminimax_command().
	 */
/*@in@*/
/*@owned@*/
/*@notnull@*/
	struct hnef_listm	* * opt_buf_moves;

	/*
	 * Optimization.
	 *
	 * Pointer to a list of board structs. The amount of board
	 * structs is depth_max + 1.
	 *
	 * In the aiminimax_min() and aiminimax_max() functions, these
	 * boards are used (specifically: opt_buf_board[depth_cur]) so
	 * they don't have to be re-allocated all the time.
	 * opt_buf_board[0] is used in the initial call to
	 * aiminimax_min() in aiminimax_command().
	 */
/*@in@*/
/*@owned@*/
/*@notnull@*/
	struct hnef_board	* * opt_buf_board;

	/*
	 * True if the ruleset involves escaping.
	 */
	HNEF_BOOL		escape_ruleset;

	/*
	 * Length of opt_buf_moves and opt_buf_board.
	 */
	unsigned short		opt_buf_len;

};

#endif

