/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <limits.h>	/* INT_* */

#include "algorithm.h"

/*
 * Maximum and minimum value of a move.
 */
/*@unchecked@*/
static
const int	HNEF_AIM_VALUE_INFMAX	= INT_MAX,
		HNEF_AIM_VALUE_INFMIN	= INT_MIN;

/*
 * Victory threshhold.
 *
 * This value exists because the value of a move decreases by -1 if it's
 * deeper down the search tree (because we want the computer to prefer
 * moves that are instantly winning over winning moves that are several
 * moves ahead). But we still want the computer player to recognize that
 * the value is a winning move so he can do it immediately without
 * checking the rest of the move list. Therefore the computer only needs
 * to check for this value when determining (in search()) if a move is a
 * win.
 */
/*@unchecked@*/
static
const int	HNEF_AIM_VALUE_WIN	= INT_MAX
					- (HNEF_AIM_DEPTHMAX_MAX + 1);

/*
 * `*_VALUE_PIECE`: Value of a piece on the board.
 *
 * `*_VALUE_MOVE_KING`: Value of a possible move by the king (piece with
 * type_piece.escapes).
 *
 * `*_VALUE_MOVE`: Value of a possible move by any piece.
 *
 * `*_ESCVAL_KING`: When a piece with escape is near an escape square,
 * the value in aiminimax->board_escval for that position is multiplied
 * by this value.
 *
 * `*_ESCVAL_NORM`: When a piece is near an escape square, the value in
 * aiminimax->board_escval for that position is multiplied by this
 * value.
 *
 * NOTE:	You can tune these values.
 */
/*@unchecked@*/
static
const int	HNEF_AIM_VALUE_PIECE		= 1000,
		HNEF_AIM_VALUE_MOVE_KING	= 200,
		HNEF_AIM_VALUE_MOVE		= 10,
		HNEF_AIM_ESCVAL_KING		= 10,
		HNEF_AIM_ESCVAL_NORM		= 1;

/*
 * Evaluates the board position for aim->p_index.
 *
 * For every player:
 *
 * *	A piece on the board is worth HNEF_AIM_VALUE_PIECE.
 *
 * *	A possible move by the king is worth HNEF_AIM_VALUE_MOVE_KING.
 */
static
int
aiminimax_evaluate (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR g,
/*@in@*/
/*@notnull@*/
	struct hnef_board		* const HNEF_RSTR b,
/*@in@*/
/*@notnull@*/
	const struct hnef_aiminimax	* const HNEF_RSTR aim
	)
/*@modifies * b@*/
{
	int		value	= 0;
	unsigned short	i;
	HNEF_BIT_U8	pbit;
	HNEF_BIT_U8	p_owned = g->players[aim->p_index]->opt_owned;

	/*
	 * Material value.
	 *
	 * A piece on the board is worth HNEF_AIM_VALUE_PIECE.
	 *
	 * In addition, it's worth whatever the value of board_escval is
	 * for that position. For "king" pieces (with escape), it's
	 * multiplied by HNEF_AIM_ESCVAL_KING, and for ordinary pieces
	 * by HNEF_AIM_ESCVAL_NORM. This makes the computer player a
	 * little more eager to defend escape squares as well as move
	 * the king to them.
	 */
	for	(i = 0; i < g->rules->opt_blen; ++i)
	{
		pbit	= b->pieces[i];
		if	(HNEF_BIT_U8_EMPTY != pbit)
		{
			const HNEF_BOOL owned = ((unsigned int)pbit
				& (unsigned int) p_owned)
				== (unsigned int)pbit;
			const HNEF_BOOL king = ((unsigned int)pbit
				& (unsigned int)g->rules->opt_tp_escape)
				== (unsigned int)pbit;
			value += ((HNEF_AIM_VALUE_PIECE +
				aim->board_escval[i] * (king ?
					HNEF_AIM_ESCVAL_KING :
					HNEF_AIM_ESCVAL_NORM)))
				* (owned ? 1 : -1);
		}
	}

	/*
	 * Tactical value.
	 *
	 * It takes a very long time to call hnef_board_movec_get, so
	 * only do it for the player with the king piece. Don't count
	 * move value for all pieces.
	 */
	for	(i = 0; i < g->playerc; i++)
	{
		const struct hnef_player	* const HNEF_RSTR p =
						g->players[i];
		if	(HNEF_BIT_U8_EMPTY != p->opt_owned_esc)
		{
			value += ((hnef_board_movec_get(g, b,
			aim->opt_movehist, p->opt_owned_esc, HNEF_FALSE)
				* HNEF_AIM_VALUE_MOVE_KING)
				* ((aim->p_index == i) ? 1 : -1));
		}
		if	(!aim->escape_ruleset
		&&	HNEF_BIT_U8_EMPTY != p->opt_owned)
		{
			/*
			 * If no piece can escape, then the evaluation
			 * will be very poor. So in that case we use the
			 * slower method of increasing the position
			 * value by the available moves of every piece
			 * (`opt_owned` includes the escape pieces, so
			 * they would be counted twice; but if there is
			 * no escaping, then `opt_owned_esc` is empty,
			 * so they are actually not counted twice).
			 */
			value += ((hnef_board_movec_get(g, b,
			aim->opt_movehist, p->opt_owned, HNEF_FALSE)
				* HNEF_AIM_VALUE_MOVE)
				* ((aim->p_index == i) ? 1 : -1));
		}
	}

	return	value;
}

/*@-protoparamname@*/
static
int
aiminimax_min (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR g,
/*@in@*/
/*@notnull@*/
	struct hnef_board	* const HNEF_RSTR b_old,
/*@in@*/
/*@notnull@*/
	struct hnef_aiminimax	* const aim,
	const int		alpha,
	int			beta,
	const unsigned short	depth,
	const unsigned short	depthmax
#ifdef	HNEFATAFL_AIM_ZHASH
	,
	const unsigned int	hashkey,
	const unsigned int	hashlock
#endif	/* HNEFATAFL_AIM_ZHASH */
	,
/*@in@*/
/*@notnull@*/
	enum HNEF_FR		* const fr
	)
/*@modifies * b_old, * aim, * fr@*/
;
/*@=protoparamname@*/

/*
 * Like aiminimax_min, but maximizes the lowest possible value (alpha
 * cutoff) for the computer player.
 *
 * The current best value is worth at least alpha.
 */
static
int
aiminimax_max (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR g,
/*@in@*/
/*@notnull@*/
	struct hnef_board	* const HNEF_RSTR b_old,
/*@in@*/
/*@notnull@*/
	struct hnef_aiminimax	* const aim,
	int			alpha,
	const int		beta,
	const unsigned short	depth,
	const unsigned short	depthmax
#ifdef	HNEFATAFL_AIM_ZHASH
	,
	const unsigned int	hashkey,
	const unsigned int	hashlock
#endif	/* HNEFATAFL_AIM_ZHASH */
	,
/*@in@*/
/*@notnull@*/
	enum HNEF_FR * const			fr
	)
/*@modifies * b_old, * aim, * fr@*/
{
	unsigned short		winner	= HNEF_PLAYER_UNINIT;
#ifdef	HNEFATAFL_AIM_ZHASH
	struct hnef_zhashnode	* const HNEF_RSTR node =
				hnef_zhashtable_get
			(aim->tp_tab, hashkey, hashlock, b_old->turn);
#endif	/* HNEFATAFL_AIM_ZHASH */

	assert	(NULL != g);
	assert	(NULL != b_old);
	assert	(NULL != aim);

#ifdef	HNEFATAFL_AIM_ZHASH
	if	(NULL != node
	&&	node->depthleft >= (unsigned short)(depthmax - depth))
	{
		/*
		 * NOTE:	Assumes that value can't be VACANT.
		 */
		if	(HNEF_AIM_ZHNEF_BETA != node->value_type)
		{
			node->used	= HNEF_TRUE;
			if	(HNEF_AIM_VALUE_INFMAX == node->value)
			{
				return		node->value - depth;
			}
			else
			{
				return		node->value;
			}
		}
	}
#endif	/* HNEFATAFL_AIM_ZHASH */

	if (hnef_board_game_over(g, b_old, aim->opt_movehist, & winner))
	{
		if	(aim->p_index == winner)
		{
#ifdef	HNEFATAFL_AIM_ZHASH
			hnef_zhashtable_put(aim->tp_tab, b_old,
				HNEF_AIM_VALUE_INFMAX,
				HNEF_AIM_ZHNEF_EXACT,
				(unsigned short)(depthmax - depth));
#endif	/* HNEFATAFL_AIM_ZHASH */
			return	HNEF_AIM_VALUE_INFMAX - depth;
		}
		else
		{
#ifdef	HNEFATAFL_AIM_ZHASH
			hnef_zhashtable_put(aim->tp_tab, b_old,
				HNEF_AIM_VALUE_INFMIN,
				HNEF_AIM_ZHNEF_EXACT,
				(unsigned short)(depthmax - depth));
#endif	/* HNEFATAFL_AIM_ZHASH */
			return	HNEF_AIM_VALUE_INFMIN;
		}
	}
	else if	(depth >= depthmax)
	{
		const int value = aiminimax_evaluate(g, b_old, aim);
#ifdef	HNEFATAFL_AIM_ZHASH
		hnef_zhashtable_put(aim->tp_tab, b_old, value,
			HNEF_AIM_ZHNEF_EXACT,
			(unsigned short)(depthmax - depth));
#endif	/* HNEFATAFL_AIM_ZHASH */
		return	value;
	}
	else
	{
		int			tmp	= 0;
		size_t			i;
		struct hnef_listm	* const moves =
					aim->opt_buf_moves[depth];
		struct hnef_move	* HNEF_RSTR mov;
#ifdef	HNEFATAFL_AIM_ZHASH
		unsigned int		keytmp,
					locktmp;
#endif	/* HNEFATAFL_AIM_ZHASH */
		hnef_listm_clear(moves);

		* fr	= hnef_board_moves_get(g, b_old,
				aim->opt_movehist, moves);
		if	(!hnef_fr_good(* fr))
		{
			return	HNEF_AIM_VALUE_INFMIN;
		}

		/*
		 * hnef_board_game_over should have triggered.
		 */
		assert	(moves->elemc > 0);

		for	(i = 0; i < moves->elemc; i++)
		{
			struct hnef_board	* const b_new =
					aim->opt_buf_board[depth];
			hnef_board_copy	(b_old, b_new,
					g->rules->opt_blen);
			mov	= & moves->elems[i];
#ifdef	HNEFATAFL_AIM_ZHASH
			keytmp	= hashkey;
			locktmp	= hashlock;
#endif	/* HNEFATAFL_AIM_ZHASH */

			* fr =	hnef_board_move_unsafe(g, b_new,
				aim->opt_movehist, mov->pos, mov->dest
#ifdef	HNEFATAFL_AIM_ZHASH
				, aim->tp_tab, & keytmp, & locktmp
#else	/* HNEFATAFL_AIM_ZHASH */
#ifdef	HNEFATAFL_ZHASH
				, NULL, NULL, NULL
#endif	/* HNEFATAFL_ZHASH */
#endif	/* HNEFATAFL_AIM_ZHASH */
				);
			if	(!hnef_fr_good(* fr))
			{
				return	HNEF_AIM_VALUE_INFMIN;
			}

			tmp = aiminimax_min(g, b_new, aim, alpha, beta,
				(unsigned short)(depth + 1), depthmax
#ifdef	HNEFATAFL_AIM_ZHASH
				, keytmp, locktmp
#endif	/* HNEFATAFL_AIM_ZHASH */
				, fr);
			if	(!hnef_fr_good(* fr))
			{
				return	HNEF_AIM_VALUE_INFMIN;
			}

			--aim->opt_movehist->elemc;

			if	(tmp > alpha)
			{
				alpha	= tmp;
			}
			if	(alpha >= beta)
			{
#ifdef	HNEFATAFL_AIM_ZHASH
				hnef_zhashtable_put(aim->tp_tab, b_new,
					alpha, HNEF_AIM_ZHNEF_ALPHA,
					(unsigned short)
						(depthmax - depth));
#endif	/* HNEFATAFL_AIM_ZHASH */
				return	alpha;
			}
		}
#ifdef	HNEFATAFL_AIM_ZHASH
		hnef_zhashtable_put(aim->tp_tab, b_old, alpha,
			HNEF_AIM_ZHNEF_ALPHA,
			(unsigned short)(depthmax - depth));
#endif	/* HNEFATAFL_AIM_ZHASH */
		return	alpha;
	}
}

/*
 * Minimizes the highest possible value (beta cutoff) for the opponent.
 *
 * The current best value is worth at most beta.
 *
 * Upon successful return, returns an evaluated value.
 *
 * Upon failed return, returns 0.
 *
 * This function never sets HNEF_FR fr parameter to anything else but a
 * failure code in the event of a failure. It assumes that the fr
 * parameter is HNEF_FR_SUCCESS when passed to this function, and will
 * not change it upon successful execution.
 *
 * Stops searching when depth >= depth_max.
 */
static
int
aiminimax_min (
	const struct hnef_game	* const HNEF_RSTR g,
	struct hnef_board	* const HNEF_RSTR b_old,
	struct hnef_aiminimax	* const aim,
	const int		alpha,
	int			beta,
	const unsigned short	depth,
	const unsigned short	depthmax
#ifdef	HNEFATAFL_AIM_ZHASH
	, const unsigned int	hashkey,
	const unsigned int	hashlock
#endif	/* HNEFATAFL_AIM_ZHASH */
	,
	enum HNEF_FR		* const fr
	)
{
	unsigned short		winner	= HNEF_PLAYER_UNINIT;

#ifdef	HNEFATAFL_AIM_ZHASH
	struct hnef_zhashnode	* const HNEF_RSTR node =
				hnef_zhashtable_get
			(aim->tp_tab, hashkey, hashlock, b_old->turn);
#endif	/* HNEFATAFL_AIM_ZHASH */

	assert	(NULL != g);
	assert	(NULL != b_old);
	assert	(NULL != aim);

#ifdef	HNEFATAFL_AIM_ZHASH
	if	(NULL != node
	&&	node->depthleft >= (unsigned short)(depthmax - depth))
	{
		/*
		 * Position already evaluated.
		 */
		if	(HNEF_AIM_ZHNEF_ALPHA != node->value_type)
		{
			node->used	= HNEF_TRUE;
			if	(HNEF_AIM_VALUE_INFMAX == node->value)
			{
				return		node->value - depth;
			}
			else
			{
				return		node->value;
			}
		}
	}
#endif	/* HNEFATAFL_AIM_ZHASH */

	if (hnef_board_game_over(g, b_old, aim->opt_movehist, & winner))
	{
		if	(aim->p_index == winner)
		{
#ifdef	HNEFATAFL_AIM_ZHASH
			hnef_zhashtable_put(aim->tp_tab, b_old,
				HNEF_AIM_VALUE_INFMAX,
				HNEF_AIM_ZHNEF_EXACT,
				(unsigned short)(depthmax - depth));
#endif	/* HNEFATAFL_AIM_ZHASH */
			return	HNEF_AIM_VALUE_INFMAX - depth;
		}
		else
		{
#ifdef	HNEFATAFL_AIM_ZHASH
			hnef_zhashtable_put(aim->tp_tab, b_old,
				HNEF_AIM_VALUE_INFMIN,
				HNEF_AIM_ZHNEF_EXACT,
				(unsigned short)(depthmax - depth));
#endif	/* HNEFATAFL_AIM_ZHASH */
			return	HNEF_AIM_VALUE_INFMIN;
		}
	}
	else if	(depth >= depthmax)
	{
		const int value = aiminimax_evaluate(g, b_old, aim);
#ifdef	HNEFATAFL_AIM_ZHASH
		hnef_zhashtable_put(aim->tp_tab, b_old, value,
			HNEF_AIM_ZHNEF_EXACT,
			(unsigned short)(depthmax - depth));
#endif	/* HNEFATAFL_AIM_ZHASH */
		return	value;
	}
	else
	{
		int			tmp	= 0;
		size_t			i;
		struct hnef_listm	* const moves =
					aim->opt_buf_moves[depth];
		struct hnef_move	* HNEF_RSTR mov;
#ifdef	HNEFATAFL_AIM_ZHASH
		unsigned int		keytmp,
					locktmp;
#endif	/* HNEFATAFL_AIM_ZHASH */
		hnef_listm_clear(moves);

		* fr	= hnef_board_moves_get(g, b_old,
				aim->opt_movehist, moves);
		if	(!hnef_fr_good(* fr))
		{
			return	HNEF_AIM_VALUE_INFMIN;
		}

		/*
		 * hnef_board_game_over should have triggered.
		 */
		assert	(moves->elemc > 0);

		for	(i = 0; i < moves->elemc; i++)
		{
			struct hnef_board	* const b_new =
					aim->opt_buf_board[depth];
			hnef_board_copy	(b_old, b_new,
					g->rules->opt_blen);
			mov	= & moves->elems[i];
#ifdef	HNEFATAFL_AIM_ZHASH
			keytmp	= hashkey;
			locktmp	= hashlock;
#endif	/* HNEFATAFL_AIM_ZHASH */

			* fr =	hnef_board_move_unsafe(g, b_new,
				aim->opt_movehist, mov->pos, mov->dest
#ifdef	HNEFATAFL_AIM_ZHASH
				, aim->tp_tab, & keytmp, & locktmp
#else	/* HNEFATAFL_AIM_ZHASH */
#ifdef	HNEFATAFL_ZHASH
				, NULL, NULL, NULL
#endif	/* HNEFATAFL_ZHASH */
#endif	/* HNEFATAFL_AIM_ZHASH */
				);
			if	(!hnef_fr_good(* fr))
			{
				return	HNEF_AIM_VALUE_INFMIN;
			}

			tmp = aiminimax_max(g, b_new, aim, alpha, beta,
				(unsigned short)(depth + 1), depthmax
#ifdef	HNEFATAFL_AIM_ZHASH
				, keytmp, locktmp
#endif	/* HNEFATAFL_AIM_ZHASH */
				, fr);
			if	(!hnef_fr_good(* fr))
			{
				return	HNEF_AIM_VALUE_INFMIN;
			}

			--aim->opt_movehist->elemc;

			if	(tmp < beta)
			{
				beta	= tmp;
			}
			if	(alpha >= beta)
			{
#ifdef	HNEFATAFL_AIM_ZHASH
				hnef_zhashtable_put(aim->tp_tab, b_new,
					beta, HNEF_AIM_ZHNEF_BETA,
					(unsigned short)
						(depthmax - depth));
#endif	/* HNEFATAFL_AIM_ZHASH */
				return	beta;
			}
		}
#ifdef	HNEFATAFL_AIM_ZHASH
		hnef_zhashtable_put(aim->tp_tab, b_old, beta,
			HNEF_AIM_ZHNEF_BETA,
			(unsigned short)(depthmax - depth));
#endif	/* HNEFATAFL_AIM_ZHASH */
		return	beta;
	}
}

/*
 * Searches b_orig for moves in a minimax search down to a maximum depth
 * of depth_cur.
 *
 * The best move is sorted first in aim->opt_moves (aim->opt_moves[0] is
 * always the best move that this function could find).
 *
 * If this function sets win_found to true, then aim->opt_moves[0] is
 * not only the best move that it could find, but also a certain win
 * that should be carried out immediately without calling this function
 * again.
 *
 * This function may also set `force` (indicating to select any move
 * immediately) or `stop` (indicating to not select a move at all). If
 * `force`, then the best move is the first one in the list like if a
 * winning move has been found.
 */
static
enum HNEF_FR
aiminimax_search (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR g,
/*@in@*/
/*@notnull@*/
	struct hnef_aiminimax	* const aim,
/*@in@*/
/*@notnull@*/
	const struct hnef_board	* const HNEF_RSTR b_orig,
	const unsigned short	depth_cur,
/*@in@*/
/*@notnull@*/
	HNEF_BOOL		* const HNEF_RSTR win_found,
/*@in@*/
/*@notnull@*/
	HNEF_BOOL		* const HNEF_RSTR force,
/*@in@*/
/*@notnull@*/
	HNEF_BOOL		* const HNEF_RSTR stop,
/*@null@*/
	void			* data,
/*@null@*/
	enum HNEF_FR (* func_interrupt)
				(void *, HNEF_BOOL *, HNEF_BOOL *),
/*@null@*/
	enum HNEF_FR (* func_progress) (void *, int, int)
	)
/*@modifies * aim, * win_found, * force, * stop, * data@*/
{
	int			alpha		= HNEF_AIM_VALUE_INFMIN;
	const int		beta		= HNEF_AIM_VALUE_WIN;
	enum HNEF_FR		fr		= HNEF_FR_SUCCESS;
	int			i;
	struct hnef_board	* b_new		= NULL;
	int			value;
	struct hnef_move	* HNEF_RSTR mtmp;
	struct hnef_listm	* moves		= NULL;
	struct hnef_move	mcache;

#ifdef	HNEFATAFL_AIM_ZHASH
	unsigned int		hashkey,
				hashlock;
#endif	/* HNEFATAFL_AIM_ZHASH */
	/*
	 * aim->opt_moves is the list of moves that we are using in the
	 * search.
	 *
	 * mcache is the previous best move. Since the algorithm will
	 * check the best move from the previous search first, it will
	 * also discard that move first if all moves are bad. That's why
	 * we remember that move in case the computer decides that all
	 * moves are bad. If so, the previously apparently least bad
	 * move will be returned. This is important if the computer
	 * thinks further than the human, because the human may not have
	 * realized that the move is bad. Otherwise the computer will
	 * make random moves when it realizes that defeat is certain.
	 * By doing this, it won't appear to give up as fast.
	 */

	assert	(NULL != aim);
	assert	(NULL != b_orig);

#ifdef	HNEFATAFL_AIM_ZHASH
	hashkey		= hnef_zhashkey	(aim->tp_tab, b_orig);
	hashlock	= hnef_zhashlock(aim->tp_tab, b_orig);
#endif	/* HNEFATAFL_AIM_ZHASH */


	moves		= aim->opt_moves;
	mcache.pos	= moves->elems[0].pos;
	mcache.dest	= moves->elems[0].dest;

	if	(NULL != func_progress)
	{
		fr	= func_progress(data, 0, 100);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}
	}

	for	(i = 0; i < (int)moves->elemc; i++)
	{
		if	(NULL != func_interrupt)
		{
			fr	= func_interrupt(data, force, stop);
			if	(!hnef_fr_good(fr))
			{
				return	fr;
			}
			if	(* force
			||	* stop)
			{
				break;
			}
		}

		b_new	= aim->opt_buf_board[0];
		mtmp	= & moves->elems[i];

		hnef_board_copy(b_orig, b_new, g->rules->opt_blen);

		fr	= hnef_board_move_unsafe(g, b_new,
			aim->opt_movehist, mtmp->pos, mtmp->dest
#ifdef	HNEFATAFL_AIM_ZHASH
			, aim->tp_tab , & hashkey, & hashlock
#else	/* HNEFATAFL_AIM_ZHASH */
#ifdef	HNEFATAFL_ZHASH
			, NULL, NULL, NULL
#endif	/* HNEFATAFL_ZHASH */
#endif	/* HNEFATAFL_AIM_ZHASH */
			);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}

		value = aiminimax_min(g, b_new, aim, alpha, beta,
			(unsigned short)1, depth_cur
#ifdef	HNEFATAFL_AIM_ZHASH
			, hashkey, hashlock
#endif	/* HNEFATAFL_AIM_ZHASH */
			, & fr);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}

		/*
		 * A move was added by hnef_board_move_unsafe(). Remove
		 * it:
		 */
		aim->opt_movehist->elemc--;

		if	(value > alpha)
		{
			/*
			 * New best move found.
			 */
			fr	= hnef_listm_swaptofr(moves, (size_t)i);
			if	(!hnef_fr_good(fr))
			{
				return	fr;
			}
			alpha	= value;
		}

		if	(alpha >= beta)
		{
			/*
			 * Winning move found. Stop searching.
			 */
			* win_found	= HNEF_TRUE;
			break;
		}

		if	(value <= HNEF_AIM_VALUE_INFMIN)
		{
			if	(moves->elemc < (size_t)2)
			{
				/*
				 * The computer wants to remove all
				 * moves. Therefore use the previously
				 * cached move.
				 */
				moves->elems[0].pos	= mcache.pos;
				moves->elems[0].dest	= mcache.dest;
				break;
			}
			fr	= hnef_listm_remove(moves, (size_t)i);
			if	(!hnef_fr_good(fr))
			{
				return	fr;
			}

			/*
			 * hnef_listm_remove changes elemc, and i now
			 * points to the next move in the list.
			 *
			 * Note that we can not break here if
			 * moves->elemc <= 1, because then we won't know
			 * if the only remaining move is worse than the
			 * previously best move (mcache). We have to
			 * "unnecessarily" check the last move too.
			 */
			i--;
		}

		/*
		 * i may be < 0 if we removed a move by
		 * hnef_listm_remove.
		 */

		if	(NULL != func_progress)
		{
			fr	= func_progress(data,
				((int)(i < 0 ? 0 : i)
				* 100 / (int)moves->elemc), 100);
			if	(!hnef_fr_good(fr))
			{
				return	fr;
			}
		}
	}

	if	(NULL != func_progress)
	{
		fr	= func_progress(data, 100, 100);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}
	}

/*
 * SPLint doesn't know that `force`, `stop` and `data` may be modified
 * through the function pointers.
 */
	return HNEF_FR_SUCCESS;
/*@i3@*/\
}

/*
 * Performs a minimax search with alpha-beta pruning and iterative
 * deepening.
 *
 * This function sets `pos` and `dest`:
 *
 * *	Both are set to `HNEF_BOARDPOS_NONE` if no move is possible or
 *	if the command to "stop" was given. (This returns
 *	`HNEF_FR_SUCCESS` since it's always non-fatal.)
 * *	Both are set to valid coordinates (indicating a valid move) if
 *	one exists, and the "stop" command was not given.
 *
 * Note that if the game is not over, then this algorithm must always
 * find a move, unless the "stop" command is given. So if the computer
 * player doesn't find a move when the game is not over, and the "stop"
 * command was never given, then there's a bug in the program.
 *
 * The function pointers can both be `NULL`.
 *
 * -	`func_interrupt` takes `HNEF_BOOL * force, HNEF_BOOL * stop`
 *	parameters. Setting the first one (force) makes the computer
 *	force a move prematurely. Setting the second one (stop) makes
 *	the computer stop thinking without returning a move (it returns
 *	`HNEF_BOARDPOS_NONE` for `pos` and `dest`).
 * -	`func_progress` takes `int progress, int progress_max` as
 *	parameters, where both integers must be >= and
 *	`progress <= progress_max`. This should display the progress as
 *	a percentage in the interface (or wherever).
 *
 * Note that the progress function will be called with "0% to 100%" for
 * every iteration (in the iterative deepening). So if the search depth
 * is 3, it will be called like this:
 *
 * 1.	0% to 100% progress on depth 1
 * 2.	0% to 100% progress on depth 2
 * 3.	0% to 100% progress on depth 3
 *
 * `data` is passed to both `func_*` pointers as the first parameter. It
 * can be `NULL` or anything. The purpose is if you have, for example,
 * an `ui` struct which you need to pass to `func_*`.
 *
 * The game must be valid according to `hnef_game_valid()` for this
 * function to be called.
 */
enum HNEF_FR
aiminimax_command (
	const struct hnef_game	* const HNEF_RSTR g,
	struct hnef_aiminimax	* const aim,
	unsigned short		* pos,
	unsigned short		* dest,
	void			* data,
	enum HNEF_FR (* func_interrupt)
				(void *, HNEF_BOOL *, HNEF_BOOL *),
	enum HNEF_FR (* func_progress) (void *, int, int)
	)
{
	enum HNEF_FR		fr		= HNEF_FR_SUCCESS;
	const struct hnef_board	* b_orig	= NULL;
	unsigned short		depth_cur;
	HNEF_BOOL		win_found	= HNEF_FALSE,
				force		= HNEF_FALSE,
				stop		= HNEF_FALSE;

	assert	(NULL != g);
	assert	(NULL != g->board);
	assert	(NULL != aim);
	assert	(NULL != aim->opt_moves);
	assert	(NULL != aim->opt_buf_moves);
	assert	(NULL != aim->opt_buf_board);
	assert	(NULL != pos);
	assert	(NULL != dest);

	/*
	 * Set pos / dest to invalid (indicating no move).
	 */
	* pos	= * dest	= HNEF_BOARDPOS_NONE;

	b_orig	= g->board;

	/*
	 * Move count is aim->opt_moves.elemc.
	 */
	hnef_listm_clear(aim->opt_moves);

	fr	= hnef_listmh_copy(g->movehist, aim->opt_movehist);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	fr	= hnef_board_moves_get(g, b_orig, aim->opt_movehist,
		aim->opt_moves);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	if	(aim->opt_moves->elemc < (size_t)1)
	{
		return	HNEF_FR_SUCCESS;
	}

#ifdef	HNEFATAFL_AIM_ZHASH
	hnef_zhashtable_clearunused(aim->tp_tab);
#endif	/* HNEFATAFL_AIM_ZHASH */
	/*
	 * NOTE:	If aim->tp_tab->cols->elemc >=
	 *		aim->tp_tab->cols->capc, then we have depleted
	 *		all available memory for collisions, and
	 *		increasing g->ai_minimax_hash_mem_col would
	 *		help.
	 */

	/*
	 * aim->opt_moves->elems[0] will always be the best move.
	 */
	for	(depth_cur = (unsigned short)1;
		depth_cur <= aim->depth_max; depth_cur++)
	{
		if	(aim->opt_moves->elemc < (size_t)2)
		{
			/*
			 * There's only one move (left). The algorithm
			 * is forced to do it, so stop searching.
			 */
			break;
		}

		fr	= aiminimax_search(g, aim, b_orig, depth_cur,
			& win_found, & force, & stop,
			data, func_interrupt, func_progress);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}

		if	(stop)
		{
			/*
			 * Don't select a move at all.
			 */
			return	HNEF_FR_SUCCESS;
		}
/*@i1@*/\
		else if	(force
		||	win_found)
		{
			/*
			 * Select a move immediately.
			 */
			break;
		}
	}

	assert	(aim->opt_moves->elemc > 0);

	if	(HNEF_BOARDPOS_NONE == aim->opt_moves->elems[0].pos
	||	HNEF_BOARDPOS_NONE == aim->opt_moves->elems[0].dest)
	{
		return	HNEF_FR_SUCCESS;
	}
	else
	{
		* pos	= aim->opt_moves->elems[0].pos;
		* dest	= aim->opt_moves->elems[0].dest;
		return	HNEF_FR_SUCCESS;
	}
}

